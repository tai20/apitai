package com.sharework.health.helper;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("")
public class Test {

    @GetMapping("")
    public String test(){
        return "Deploy successfully!!! Use Postman to test API";
    }
}
