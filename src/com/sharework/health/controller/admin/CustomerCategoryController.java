package com.sharework.health.controller.admin;

import com.sharework.health.dto.CustomerCategoryDto;
import com.sharework.health.service.CustomerCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/customercategories")
@AllArgsConstructor
public class CustomerCategoryController {

    private CustomerCategoryService customerCategoryService;

    @GetMapping("")
    public Object getAllCustomerCategory(){
        List<CustomerCategoryDto> customerCategoryDtos = customerCategoryService.findAll();
        if (customerCategoryDtos.isEmpty() || customerCategoryDtos == null){
            return new ResponseEntity<>("Không có loại khách hàng", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(customerCategoryDtos, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addCustomerCategory(@RequestBody CustomerCategoryDto dto){
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = customerCategoryService.insert(dto);
        if (!result){
            return new ResponseEntity<>("Thêm loại khách hàng không thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm loại khách hàng thành công", HttpStatus.OK);
    }

    @PutMapping("{customercategory_id}")
    public Object updateCustomerCategory(@PathVariable("customercategory_id") Integer customerCategoryId, @RequestBody CustomerCategoryDto dto){
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = customerCategoryService.update(customerCategoryId, dto);
        if (!result){
            return new ResponseEntity<>("Cập nhật loại khách hàng không thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật loại khách hàng thành công", HttpStatus.OK);
    }
}
