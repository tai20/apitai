package com.sharework.health.controller.admin;

import com.sharework.health.dto.CustomerDto;
import com.sharework.health.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/customers")
@AllArgsConstructor
public class AdminCustomerController {

    private CustomerService customerService;

    @GetMapping("")
    public Object getAllUser() {
        List<CustomerDto> customerDtos = customerService.findAll();
        if (customerDtos.isEmpty()) {
            return new ResponseEntity<>("Không có dữ liệu",HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(customerDtos,HttpStatus.OK);
    }

    @PostMapping("")
    public Object addCustomer(@RequestBody CustomerDto dto,
                                           BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin",HttpStatus.BAD_REQUEST);
        }
        boolean result = customerService.insert(dto);
        if (!result){
            return new ResponseEntity<>("Thêm khách hàng thất bại",HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm khách hàng thành công",HttpStatus.CREATED);
    }

    @PutMapping("/{customer_id}")
    public Object updateCustomer(@PathVariable("customer_id") Integer id , @RequestBody CustomerDto dto,
                                       BindingResult error) {
        if (dto.getGender() == "" || dto.getCmnd() == "" || dto.getEmail() == "" || dto.getPhoneNumber() == ""){
            return new ResponseEntity<>("Thiếu thông tin quan trọng (gender, cmnd, email, phoneNumber)",HttpStatus.BAD_REQUEST);
        }
        if (error.hasErrors()) {
            return new ResponseEntity<>("Cập nhật khách hàng thất bại",HttpStatus.BAD_REQUEST);
        }
        boolean result = customerService.update(id, dto);
        if (!result){
            return new ResponseEntity<>("Cập nhật khách hàng thất bại",HttpStatus.OK);
        }
        return new ResponseEntity<>("Cập nhật khách hàng thành công",HttpStatus.OK);
    }

    @DeleteMapping("/{customer_id}")
    public ResponseEntity<CustomerDto> deleteCustomer(@PathVariable("customer_id") Integer id) {
        customerService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{phoneNumber}")
    public Object findCustomerByPhoneNumber(@PathVariable("phoneNumber") String phoneNumber) {
        CustomerDto dto = customerService.findByPhoneNumber(phoneNumber);
        if (dto == null){
            return new ResponseEntity<>("Không tìm thấy khách hàng cần tìm", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @GetMapping("getCustomerById/{customer_id}")
    public Object getCustomerById(@PathVariable("customer_id") Integer customerId) {
        CustomerDto dto = customerService.findById(customerId);
        if (dto == null){
            return new ResponseEntity<>("Không tìm thấy khách hàng cần tìm", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @GetMapping("findAllByCustomerCategory/{customercategory_id}")
    public Object findAllByCustomerCategory(@PathVariable("customercategory_id") Integer customerCategoryId) {
        List<CustomerDto> customerDtos = customerService.findAllByCustomerCategory(customerCategoryId);
        if (customerDtos == null || customerDtos.isEmpty()){
            return new ResponseEntity<>("Danh mục này không có khách hàng", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(customerDtos,HttpStatus.OK);
    }
}
