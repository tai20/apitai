package com.sharework.health.controller.admin;

import com.sharework.health.dto.MedicalCardAfterInsertDto;
import com.sharework.health.dto.PackageTestAfterInsertDto;
import com.sharework.health.dto.PackageTestDto;
import com.sharework.health.service.PackageTestService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/packagetest")
@AllArgsConstructor
public class PackageTestController {

    private PackageTestService packageTestService;

    @GetMapping("")
    public Object getAllPackageTest() {
        List<PackageTestDto> packageTests = packageTestService.findAll();
        if (packageTests.isEmpty() || packageTests == null) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(packageTests, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addPackageTest(@RequestBody PackageTestDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = packageTestService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm thất bại", HttpStatus.BAD_REQUEST);
        }
        PackageTestAfterInsertDto dto1 = new PackageTestAfterInsertDto()
                .setMessage("Thêm thành công")
                .setPackageTestDto(packageTestService.getPackageTestAfterInsert());
        return new ResponseEntity<>(dto1, HttpStatus.CREATED);
    }

    @PutMapping("{packagetest_id}")
    public Object updatePackageTest(@PathVariable("packagetest_id") Integer advisoryId, @RequestBody PackageTestDto dto) {
        if (dto.getTestServiceDto() == null) {
            return new ResponseEntity<>("Thiếu thông tin gói dịch vụ khám", HttpStatus.BAD_REQUEST);
        }
        boolean result = packageTestService.update(advisoryId, dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.OK);
    }

    @DeleteMapping("{packagetest_id}")
    public Object deletePackageTest(@PathVariable("packagetest_id") Integer advisoryId) {

        boolean result = packageTestService.delete(advisoryId);
        if (!result) {
            return new ResponseEntity<>("Xóa thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Xóa thành công", HttpStatus.OK);
    }
}
