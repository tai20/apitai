package com.sharework.health.controller.admin;

import com.sharework.health.dto.IndicationCardDetailDto;
import com.sharework.health.dto.IndicationCardDto;
import com.sharework.health.dto.SlipUsePackageTestDetailDto;
import com.sharework.health.service.SlipUsePackageTestDetailService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/slipusepackagetestdetail")
@AllArgsConstructor
public class SlipUsePackageTestDetailController {

    private SlipUsePackageTestDetailService slipUsePackageTestDetailService;

    @GetMapping("")
    public Object getAllSlipUsePackageTestDetail() {
        List<SlipUsePackageTestDetailDto> slipUsePackageTestDetailDtos = slipUsePackageTestDetailService.findAll();
        if (slipUsePackageTestDetailDtos.isEmpty() || slipUsePackageTestDetailDtos == null) {
            return new ResponseEntity<>("Không có chi tiết tư vấn nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(slipUsePackageTestDetailDtos, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addSlipUsePackageTestDetail(@RequestBody SlipUsePackageTestDetailDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = slipUsePackageTestDetailService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm thành công", HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public Object updateSlipUsePackageTestDetail(@PathVariable("id") Integer advisoryId, @RequestBody SlipUsePackageTestDetailDto dto) {
        if (dto.getSlipusePackageTestDto() == null || dto.getCategoryIndicationCardDto() == null) {
            return new ResponseEntity<>("Thiếu thông tin gói sử dụng dịch vụ hoặc chỉ định", HttpStatus.BAD_REQUEST);
        }
        boolean result = slipUsePackageTestDetailService.update(advisoryId, dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public Object deleteSlipUsePackageTestDetail(@PathVariable("id") Integer advisoryId) {

        boolean result = slipUsePackageTestDetailService.delete(advisoryId);
        if (!result) {
            return new ResponseEntity<>("Xóa thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Xóa thành công", HttpStatus.OK);
    }


    @GetMapping("getSlipUsePackageTestDetailBySlipUsePackageId/{slipUsePackageTestId}")
    public Object getSlipUsePackageTestDetailBySlipUsePackageId(@PathVariable("slipUsePackageTestId") Integer slipUsePackageTestId) {
        List<SlipUsePackageTestDetailDto> dtos = slipUsePackageTestDetailService.getSlipUsePackageTestDetailBySlipUsePackageId(slipUsePackageTestId);
        if (dtos.isEmpty() || dtos == null) {
            return new ResponseEntity<>("Không có chi tiết chỉ định", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
}
