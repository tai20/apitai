package com.sharework.health.controller.admin;

import com.sharework.health.dto.FreeTrialDto;
import com.sharework.health.service.FreeTrialService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/freetrials")
@AllArgsConstructor
public class FreeTrialController {

    private FreeTrialService freeTrialService;

    @GetMapping("")
    public Object getAllFreeTrial(){
        List<FreeTrialDto> freeTrialDtos = freeTrialService.findAll();
        if (freeTrialDtos.isEmpty()) {
            return new ResponseEntity<>("Không có dữ liệu dùng thử", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(freeTrialDtos,HttpStatus.OK);
    }

    @PostMapping("")
    public Object addFreeTrial(@RequestBody FreeTrialDto dto){
        if (dto == null){
            return new ResponseEntity<>("Thiếu dữ liệu", HttpStatus.BAD_REQUEST);
        }

        boolean result = freeTrialService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm dịch vụ dùng thử thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm dịch vụ dùng thử thành công",HttpStatus.OK);
    }
}
