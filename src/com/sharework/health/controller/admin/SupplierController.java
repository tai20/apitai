package com.sharework.health.controller.admin;

import com.sharework.health.dto.SupplierDto;
import com.sharework.health.service.SupplierService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/suppliers")
@AllArgsConstructor
public class SupplierController {

    private SupplierService supplierService;

    @GetMapping("")
    public ResponseEntity<List<SupplierDto>> getAllSupplier() {
        List<SupplierDto> supplierDtos = supplierService.findAll();
        if (supplierDtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(supplierDtos,HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<SupplierDto> addSupplier(@RequestBody SupplierDto dto,
                                           BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        supplierService.insert(dto);
        return new ResponseEntity<>(dto,HttpStatus.CREATED);
    }

    @GetMapping("findAllByStatusLikeSupplier")
    public Object findAllByStatusLikeSupplier() {
        List<SupplierDto> supplierDtos = supplierService.findAllByStatusLikeSupplier();
        if (supplierDtos.isEmpty()) {
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(supplierDtos,HttpStatus.OK);
    }

    @GetMapping("findAllByStatusLikeClinic")
    public Object findAllByStatusLikeClinic() {
        List<SupplierDto> supplierDtos = supplierService.findAllByStatusLikeClinic();
        if (supplierDtos.isEmpty()) {
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(supplierDtos,HttpStatus.OK);
    }
}
