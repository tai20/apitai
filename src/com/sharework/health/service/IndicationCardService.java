package com.sharework.health.service;

import com.sharework.health.dto.IndicationCardDetailDto;
import com.sharework.health.dto.IndicationCardDto;
import com.sharework.health.dto.TotalCostDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface IndicationCardService extends BaseService<IndicationCardDto, Integer> {

    List<IndicationCardDto> getIndicationCardByCustomer(Integer customerId);

    List<IndicationCardDto> getIndicationCardByUser(Integer userId);

    boolean addIndicationCardDetail(IndicationCardDetailDto dto, Integer medicalCardId);

    boolean updateIndicationCardDetailABoutUtralsoud(Integer indicationCardId, Integer categoryIndicationCardId,String liverandkidney,String bilebuctsAndgallbladderandbladder,String abdominalfluid,
                                                     String uterus,String dkts,String density,String ovary,String endometrium,String cervical,
                                                     String fornixfluid,String smallfetusultrasoundinformation1,String smallfetusultrasoundinformation2,String blackandwhitefetalultrasoundinformation,
                                                     String colordoppleroffetalbloodvesselsandnuchaltranslucency,  String pidmuterus,String unbt,
                                                     String interstitial,String tntc,String colordopplerofFetalbloodvessels,String pregnancyultrasound4d,
                                                     String fetalanatomyheadfaceneck,String fetalanatomychest,String fetalanatomystomach,String fetalanatomythefourlimbs,
                                                     String mlt,String bigblackandwhitefetalultrasoundinformation,String conclude,String ctc_dk,
                                                     String bldb,String blda,String thewomb,MultipartFile[] listImages) throws IOException;

    boolean importFileIndicationCard(Integer indicationCardId, Integer categoryIndicationId, MultipartFile file) throws IOException;

    IndicationCardDto getIndicationCardAfterInsert();

    List<IndicationCardDetailDto> getIndicationCardDetailByIndicationCard(Integer indicationCardId);

    IndicationCardDto getIndicationCardLatelyByCustomer(Integer customerId);

    IndicationCardDto getIndicationCardByMedicalCardId(Integer medicalCardId);

    List<IndicationCardDetailDto> getIndicationCardDetailByMedicalCardId(Integer medicalCardId);

    boolean updateIndicationCardInMedicalCard(Integer medicalCardId, IndicationCardDto dto);

    boolean updateStatusIndicationCardDetail(Integer medicalCardId, Integer indicationCardId,
                                             IndicationCardDetailDto dto);

    IndicationCardDetailDto getIndicationCardDetailByAfterUpdate(Integer indicationCardId, Integer categoryIndicationCardId);
}
