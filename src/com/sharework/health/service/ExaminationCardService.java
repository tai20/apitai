package com.sharework.health.service;

import com.sharework.health.dto.*;

import java.util.List;

public interface ExaminationCardService extends BaseService<ExaminationCardDto, Integer> {

    //boolean activeExaminationCard(Integer examinationCardId, MultipartFile signature) throws IOException;

    boolean activeExaminationCard(Integer examinationCardId);

    boolean addExaminationCardDetail(ExaminationCardDetailDto dto);

    boolean confirmExamination(Integer examinationCardId, Integer serviceId, Integer userId);

    List<ExaminationCardDetailQueryDto> findAllByUser(Integer userId);

    List<ExaminationCardDto> findAllBySlipUse(Integer slipUseId);

    List<ExaminationCardDetailDto> findAllByExaminationCard(Integer examinationCardId);

    ExaminationCardDto findExaminationCardAfterInsert();

    //List<ExaminationCardQueryDto> findAllExaminationCardByUserAndCustomer();

    List<ExaminationCardCustomerQueryDto> getExaminationCardWithCustomer();
}
