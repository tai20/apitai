package com.sharework.health.service.impl;

import com.sharework.health.convert.CustomerConvert;
import com.sharework.health.dto.CustomerDto;
import com.sharework.health.dto.PasswordDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.CustomerCategoryRepository;
import com.sharework.health.repository.CustomerRepository;
import com.sharework.health.repository.RoleRepository;
import com.sharework.health.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;

    private CustomerConvert customerConvert;

    private CustomerCategoryRepository customerCategoryRepository;

    private RoleRepository roleRepository;

    @Override
    public List<CustomerDto> findAll() {
        List<Customer> customers = customerRepository.findAll();
        List<CustomerDto> customerDtos = new ArrayList<>();
        for (Customer entity: customers
             ) {
                CustomerDto dto = customerConvert.entityToDto(entity);
                customerDtos.add(dto);
        }
        return customerDtos;
    }

    @Override
    public CustomerDto findById(Integer id) {
        Customer entity = customerRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return customerConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(CustomerDto dto) {
        if (dto == null) {
            return false;
        }
        if (customerRepository.findByPhoneNumber(dto.getPhoneNumber()) == null &&
                customerRepository.findByCmnd(dto.getCmnd()) == null && customerRepository.findByEmail(dto.getEmail()) == null) {
            Customer entity = customerConvert.dtoToEntity(dto);
            entity.setId(null);
            entity.setCreatedDate(LocalDateTime.now());
            entity.setCustomerCategory(customerCategoryRepository.findById(1).get());
            customerRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public boolean update(Integer id, CustomerDto dto) {
        Customer entity = customerRepository.findById(id).get();
        if (dto == null) {
            return false;
        }

        entity.setEmail(dto.getEmail());
        if (dto.getGender().equalsIgnoreCase("Nam")){
            entity.setGender(Gender.Nam);
        }
        if (dto.getGender().equalsIgnoreCase("Nữ")){
            entity.setGender(Gender.Nữ);
        }
        entity.setName(dto.getName());
        entity.setBirthDate(dto.getBirthDate());
        entity.setAddress(dto.getAddress());
        entity.setCmnd(dto.getCmnd());
        entity.setSkinStatus(dto.getSkinStatus());
        entity.setPhoneNumber(dto.getPhoneNumber());
        customerRepository.save(entity);
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        Customer entity = customerRepository.findById(id).get();
        try {
            customerRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public CustomerDto findByPhoneNumber(String phoneNumber) {
        Customer entity = customerRepository.findByPhoneNumber(phoneNumber);
        if (entity == null) {
            return null;
        }
        return customerConvert.entityToDto(entity);
    }

    @Override
    public CustomerDto getProfile() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String email = ((UserDetails) principal).getUsername();

        Customer customer = customerRepository.findByEmail(email);
        return customerConvert.entityToDto(customer);
    }

    @Override
    public String changePassword(PasswordDto dto) {
        return null;
    }

    @Override
    public List<CustomerDto> findAllByCustomerCategory(Integer customerCategoryId) {
        List<Customer> customers = customerRepository.findAllByCustomerCategory(customerCategoryId);
        List<CustomerDto> customerDtos = new ArrayList<>();
        for (Customer entity: customers
        ) {
            CustomerDto dto = customerConvert.entityToDto(entity);
            customerDtos.add(dto);
        }
        return customerDtos;
    }
}
