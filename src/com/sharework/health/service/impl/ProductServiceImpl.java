package com.sharework.health.service.impl;

import com.sharework.health.convert.ProductConvert;
import com.sharework.health.dto.ProductDto;
import com.sharework.health.entity.Clinic;
import com.sharework.health.entity.Product;
import com.sharework.health.entity.ProductCategory;
import com.sharework.health.entity.Status;
import com.sharework.health.repository.ClinicRepository;
import com.sharework.health.repository.ProductCategoryRepository;
import com.sharework.health.repository.ProductRepository;
import com.sharework.health.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    private ProductConvert productConvert;

    private ProductCategoryRepository productCategoryRepository;

    private ClinicRepository clinicRepository;

    @Override
    public List<ProductDto> findAll() {
        List<Product> products = productRepository.findAll();
        List<ProductDto> productDtos = new ArrayList<>();
        for (Product entity: products
             ) {
            if (entity.getStatus() != Status.DEACTIVE){
                ProductDto dto = productConvert.entityToDto(entity);
                productDtos.add(dto);
            }
        }
        return productDtos;
    }

    @Override
    public ProductDto findById(Integer id) {
        Product entity = productRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return productConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(ProductDto dto) {
        if (dto == null) {
            return false;
        }
        ProductCategory productCategory = productCategoryRepository.findById(dto.getProductCategoryDto().getId()).get();
        Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
        if (productCategory == null || clinic == null){
            return false;
        }
            Product product = productConvert.dtoToEntity(dto);
            if (dto.getCapacity() <=0 ){
                int totalCapacity = dto.getQuantity() * dto.getCapacity();
                product.setTotalCapacity(totalCapacity);
            }
            product.setId(null);
            product.setProductCategory(productCategory);
            product.setClinic(clinic);
            productRepository.save(product);
            return true;

    }

    @Override
    public boolean update(Integer id, ProductDto dto) {
        Product entity = productRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        ProductCategory productCategory = productCategoryRepository.findById(dto.getProductCategoryDto().getId()).get();
        Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
        if (productCategory == null || clinic == null){
            return false;
        }
        entity.setName(dto.getName());
        entity.setStatus(Status.ACTIVE);
        entity.setQuantity(dto.getQuantity());
        entity.setWholesalePrice(dto.getWholesalePrice());
        entity.setUnit(dto.getUnit());
        entity.setCapacity(dto.getCapacity());
        entity.setTotalCapacity(dto.getQuantity() * dto.getCapacity());
        entity.setPrice(dto.getPrice());
        entity.setProductCategory(productCategory);
        entity.setClinic(clinic);
        productRepository.save(entity);
        return true;

    }

    @Override
    public boolean delete(Integer id) {
        Product entity = productRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        entity.setStatus(Status.DEACTIVE);
        return true;
    }

    @Override
    public List<ProductDto> searchProductByProductName(String name) {
        List<Product> products = productRepository.findByNameContains(name);
        List<ProductDto> productDtos = new ArrayList<>();
        for (Product entity: products
             ) {
            ProductDto dto = productConvert.entityToDto(entity);
            productDtos.add(dto);
        }
        return productDtos;
    }

    @Override
    public List<ProductDto> findAllByClinic(Integer clinicId) {
        List<Product> products = productRepository.findAllByClinic(clinicId);
        List<ProductDto> productDtos = new ArrayList<>();
        for (Product entity: products
        ) {
            ProductDto dto = productConvert.entityToDto(entity);
            productDtos.add(dto);
        }
        return productDtos;
    }

    @Override
    public List<ProductDto> findAllByClinicName(String clinicName) {
        List<Product> products = productRepository.findAllByClinicName(clinicName);
        List<ProductDto> productDtos = new ArrayList<>();
        for (Product entity: products
        ) {
            ProductDto dto = productConvert.entityToDto(entity);
            productDtos.add(dto);
        }
        return productDtos;
    }
}
