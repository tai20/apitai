package com.sharework.health.service.impl;

import com.sharework.health.convert.ProductServiceDetailConvert;
import com.sharework.health.convert.ServiceConvert;
import com.sharework.health.dto.ProductServiceDetailDto;
import com.sharework.health.dto.ServiceDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.ProductRepository;
import com.sharework.health.repository.ProductServiceDetailRepository;
import com.sharework.health.repository.ServiceCategoryRepository;
import com.sharework.health.repository.ServiceRepository;
import com.sharework.health.service.ServiceService;
import lombok.AllArgsConstructor;


import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class ServiceImpl implements ServiceService {

    private ServiceRepository serviceRepository;

    private ServiceConvert serviceConvert;

    private ProductRepository productRepository;

    private ProductServiceDetailRepository productServiceDetailRepository;

    private ProductServiceDetailConvert productServiceDetailConvert;

    private ServiceCategoryRepository serviceCategoryRepository;

    @Override
    public List<ServiceDto> findAll() {
        List<Service> services = serviceRepository.findALlByStatusLike();
        List<ServiceDto> serviceDtos = new ArrayList<>();
        for (Service entity: services
             ) {
            ServiceDto dto = serviceConvert.entityToDto(entity);
            serviceDtos.add(dto);
        }
        return serviceDtos;
    }

    @Override
    public ServiceDto findById(Integer id) {
        Service entity = serviceRepository.findById(id).get();
        if (entity == null){
            return null;
        }
        return serviceConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(ServiceDto dto) {
        if (dto == null){
            return false;
        }
        Service entity = serviceConvert.dtoToEntity(dto);
        entity.setId(null);
        entity.setStatus(Status.ACTIVE);
        serviceRepository.save(entity);
        return true;
    }

    @Override
    public boolean update(Integer id, ServiceDto dto) {
        Service entity = serviceRepository.findById(id).get();
        if (entity == null){
            return false;
        }
        ServiceCategory serviceCategory = serviceCategoryRepository.findById(dto.getServiceCategoryDto().getId()).get();
        if (serviceCategory == null){
            return false;
        }
        entity.setName(dto.getName());
        entity.setPeriod(dto.getPeriod());
        entity.setPrice(dto.getPrice());
        entity.setServiceCategory(serviceCategory);
        serviceRepository.save(entity);
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        Service entity = serviceRepository.findById(id).get();
        if (entity == null){
            return false;
        }
        entity.setStatus(Status.DEACTIVE);
        serviceRepository.save(entity);
        return true;
    }

    @Override
    public boolean addProductServiceDetail(ProductServiceDetailDto dto) {
        Product product = productRepository.findById(dto.getProductDto().getId()).get();
        Service service = serviceRepository.findById(dto.getServiceDto().getId()).get();
        if (product != null && service!= null){
            ProductServiceDetail entity = new ProductServiceDetail()
                    .setProduct(product)
                    .setService(service)
                    .setQuantity(dto.getQuantity());

            productServiceDetailRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public List<ProductServiceDetailDto> findAllByServiceId(Integer serviceId) {
        List<ProductServiceDetail> productServiceDetails = productServiceDetailRepository.findAllByServiceId(serviceId);
        List<ProductServiceDetailDto> productServiceDetailDtos = new ArrayList<>();

        for (ProductServiceDetail entity: productServiceDetails
             ) {
            ProductServiceDetailDto dto = productServiceDetailConvert.entityToDto(entity);
            productServiceDetailDtos.add(dto);
        }
        return productServiceDetailDtos;
    }

    @Override
    public ServiceDto findServiceAfterInsert() {
        Service entity = serviceRepository.findServiceAfterInsert();
        if (entity == null){
            return null;
        }
        return serviceConvert.entityToDto(entity);
    }
}
