package com.sharework.health.service.impl;

import com.sharework.health.convert.TreatmentPackageConvert;
import com.sharework.health.convert.TreatmentPackageServiceConvert;
import com.sharework.health.dto.TreatmentPackageDto;
import com.sharework.health.dto.TreatmentPackageServiceDto;
import com.sharework.health.entity.Status;
import com.sharework.health.entity.TreatmentPackage;
import com.sharework.health.entity.ServiceCategory;
import com.sharework.health.repository.ServiceCategoryRepository;
import com.sharework.health.repository.ServiceRepository;
import com.sharework.health.repository.TreatmentPackageRepository;
import com.sharework.health.repository.TreatmentPackageServiceRepository;
import com.sharework.health.service.TreatmentPackageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class TreatmentPackageServiceImpl implements TreatmentPackageService {

    private TreatmentPackageRepository treatmentPackageRepository;

    private TreatmentPackageConvert treatmentPackageConvert;

    private ServiceCategoryRepository serviceCategoryRepository;

    private ServiceRepository serviceRepository;

    private TreatmentPackageServiceRepository treatmentPackageServiceRepository;

    private TreatmentPackageServiceConvert treatmentPackageServiceConvert;
    @Override
    public List<TreatmentPackageDto> findAll() {
        List<TreatmentPackage> treatmentPackages = treatmentPackageRepository.findAll();
        List<TreatmentPackageDto> treatmentPackageDtos = new ArrayList<>();
        for (TreatmentPackage entity: treatmentPackages
             ) {
            if (entity.getStatus() != Status.DEACTIVE){
                TreatmentPackageDto dto = treatmentPackageConvert.entityToDto(entity);
                treatmentPackageDtos.add(dto);
            }
        }
        return treatmentPackageDtos;
    }

    @Override
    public TreatmentPackageDto findById(Integer id) {
        TreatmentPackage entity = treatmentPackageRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return treatmentPackageConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(TreatmentPackageDto dto) {
        if (dto == null) {
            return false;
        }
        try {
            ServiceCategory serviceCategory =
                    serviceCategoryRepository.findById(dto.getServiceCategoryDto().getId()).get();
            TreatmentPackage entity = treatmentPackageConvert.dtoToEntity(dto);
            entity.setId(null);
            entity.setStatus(Status.ACTIVE);
            treatmentPackageRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Integer id, TreatmentPackageDto dto) {
        TreatmentPackage entity = treatmentPackageRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        try {
            ServiceCategory serviceCategory =
                    serviceCategoryRepository.findById(dto.getServiceCategoryDto().getId()).get();
            entity.setName(dto.getName());
            treatmentPackageRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        TreatmentPackage entity =
                treatmentPackageRepository.findById(id).get();
        if (entity == null){
            return false;
        }
        entity.setStatus(Status.DEACTIVE);
        return true;
    }

    @Override
    public TreatmentPackageDto searchTreatmentPackageByName(String name) {
        TreatmentPackage entity =
                treatmentPackageRepository.findByName(name);
        if (entity == null){
            return null;
        }
        return treatmentPackageConvert.entityToDto(entity);
    }

    @Override
    public boolean addTreatmentPackageService(TreatmentPackageServiceDto dto) {
        TreatmentPackage treatmentPackage = treatmentPackageRepository.findById(dto.getTreatmentPackageDto().getId()).get();
        if (treatmentPackage == null){
            return false;
        }
        com.sharework.health.entity.Service service = serviceRepository.findById(dto.getServiceDto().getId()).get();
        if (service == null){
            return false;
        }
//        System.out.println(treatmentPackage);
//        System.out.println(service);
        com.sharework.health.entity.TreatmentPackageService treatmentPackageService
                = new com.sharework.health.entity.TreatmentPackageService()
                .setService(service)
                .setTreatmentPackage(treatmentPackage)
                .setNumberOfUse(dto.getNumberOfUse())
                .setPrice(dto.getPrice());
//        treatmentPackage.setTreatmentPackageServices(Arrays.asList(treatmentPackageService));
//        treatmentPackageRepository.save(treatmentPackage);
        treatmentPackageServiceRepository.save(treatmentPackageService);
        return true;
    }

    @Override
    public List<TreatmentPackageServiceDto> findAllByTreatmentPackage(Integer treatmentPackageId) {
        List<com.sharework.health.entity.TreatmentPackageService> treatmentPackageServices = treatmentPackageServiceRepository.findAllByTreatmentPackage(treatmentPackageId);
        List<TreatmentPackageServiceDto> treatmentPackageServiceDtos = new ArrayList<>();
        for (com.sharework.health.entity.TreatmentPackageService entity: treatmentPackageServices
             ) {
            TreatmentPackageServiceDto dto = treatmentPackageServiceConvert.entityToDto(entity);
            treatmentPackageServiceDtos.add(dto);
        }
        return treatmentPackageServiceDtos;
    }

    @Override
    public TreatmentPackageDto findTreatmentPackageAfterInsert() {
        TreatmentPackage entity = treatmentPackageRepository.findTreatmentPackageAfterInsert();
        if (entity == null){
            return null;
        }
        return treatmentPackageConvert.entityToDto(entity);
    }
}
