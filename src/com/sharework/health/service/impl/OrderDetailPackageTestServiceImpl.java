package com.sharework.health.service.impl;

import com.sharework.health.convert.OrderDetailPackageTestConvert;
import com.sharework.health.dto.OrderDetailPackageTestDto;
import com.sharework.health.entity.OrderDetailPackageTest;
import com.sharework.health.repository.OrderDetailPackageTestRepository;
import com.sharework.health.service.OrderDetailPackageTestService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class OrderDetailPackageTestServiceImpl implements OrderDetailPackageTestService {

    private OrderDetailPackageTestRepository orderDetailPackageTestRepository;

    private OrderDetailPackageTestConvert orderDetailPackageTestConvert;

    @Override
    public List<OrderDetailPackageTestDto> findAll() {
        List<OrderDetailPackageTest> orderDetailPackageTests = orderDetailPackageTestRepository.findAll();
        List<OrderDetailPackageTestDto> orderDetailPackageTestDtos = new ArrayList<>();
        for (OrderDetailPackageTest entity : orderDetailPackageTests
        ) {
            OrderDetailPackageTestDto dto = orderDetailPackageTestConvert.entityToDto(entity);
            orderDetailPackageTestDtos.add(dto);
        }
        return orderDetailPackageTestDtos;
    }

    @Override
    public OrderDetailPackageTestDto findById(Integer id) {
        OrderDetailPackageTest entity = orderDetailPackageTestRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return orderDetailPackageTestConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(OrderDetailPackageTestDto dto) {
        if (dto == null) {
            return false;
        }
        OrderDetailPackageTest orderDetailPackageTest = orderDetailPackageTestConvert.dtoToEntity(dto);
        orderDetailPackageTest.setOrdertestdetail_id(null);
        orderDetailPackageTestRepository.save(orderDetailPackageTest);
        return true;
    }

    @Override
    public boolean update(Integer id, OrderDetailPackageTestDto dto) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public List<OrderDetailPackageTestDto> getOrderDetailPackageTestByOrderId(Integer orderId) {
        List<OrderDetailPackageTest> orderDetailPackageTests = orderDetailPackageTestRepository.getOrderDetailPackageTestByOrderId(orderId);
        List<OrderDetailPackageTestDto> orderDetailPackageTestDtos = new ArrayList<>();
        for (OrderDetailPackageTest entity : orderDetailPackageTests
        ) {
            OrderDetailPackageTestDto dto = orderDetailPackageTestConvert.entityToDto(entity);
            orderDetailPackageTestDtos.add(dto);
        }
        return orderDetailPackageTestDtos;
    }
}
