package com.sharework.health.service.impl;

import com.sharework.health.convert.ProductCategoryConvert;
import com.sharework.health.dto.ProductCategoryDto;
import com.sharework.health.entity.ProductCategory;
import com.sharework.health.entity.Status;
import com.sharework.health.repository.ProductCategoryRepository;
import com.sharework.health.service.ProductCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class ProductCategoryServiceImpl implements ProductCategoryService {

    private ProductCategoryRepository productCategoryRepository;

    private ProductCategoryConvert productCategoryConvert;

    @Override
    public List<ProductCategoryDto> findAll() {
        List<ProductCategory> productCategories = productCategoryRepository.findAll();
        List<ProductCategoryDto> productCategoryDtos = new ArrayList<>();
        for (ProductCategory entity: productCategories
             ) {
            if (entity.getStatus() == Status.ACTIVE){
                ProductCategoryDto dto = productCategoryConvert.entityToDto(entity);
                productCategoryDtos.add(dto);
            }
        }
        return productCategoryDtos;
    }

    @Override
    public ProductCategoryDto findById(Integer id) {
        ProductCategory entity = productCategoryRepository.findById(id).get();
        if (entity == null){
            return null;
        }
        return productCategoryConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(ProductCategoryDto dto) {
        if (dto == null) {
            return false;
        }
        try {
            ProductCategory entity = productCategoryConvert.dtoToEntity(dto);
            entity.setId(null);
            entity.setStatus(Status.ACTIVE);
            productCategoryRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Integer id, ProductCategoryDto dto) {
        ProductCategory entity = productCategoryRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        try {
            entity.setName(dto.getName());
            entity.setDescription(dto.getDescription());
            productCategoryRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        try {
            ProductCategory entity = productCategoryRepository.findById(id).get();
            entity.setStatus(Status.DEACTIVE);
            productCategoryRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public ProductCategoryDto searchCategoryByCategoryName(String name) {
        ProductCategory entity = productCategoryRepository.findByName(name);
        if (entity == null){
            return null;
        }
        return productCategoryConvert.entityToDto(entity);
    }
}
