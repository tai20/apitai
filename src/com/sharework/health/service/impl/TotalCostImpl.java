package com.sharework.health.service.impl;

import com.sharework.health.convert.MedicalCardConvert;
import com.sharework.health.convert.TotalCostConvert;
import com.sharework.health.dto.MedicalCardDto;
import com.sharework.health.dto.TotalCostDto;
import com.sharework.health.entity.MedicalCard;
import com.sharework.health.entity.TotalCost;
import com.sharework.health.repository.MedicalCardRepository;
import com.sharework.health.repository.TotalCostRepository;
import com.sharework.health.service.TotalCostService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class TotalCostImpl implements TotalCostService {

    private TotalCostRepository totalCostRepository;

    private TotalCostConvert totalCostConvert;

    private MedicalCardRepository medicalCardRepository;

    @Override
    public List<TotalCostDto> findAll() {
        List<TotalCost> totalCosts = totalCostRepository.findAll();
        List<TotalCostDto> totalCostDtos = new ArrayList<>();
        for (TotalCost entity : totalCosts
        ) {
            TotalCostDto dto = totalCostConvert.entityToDto(entity);
            totalCostDtos.add(dto);
        }
        return totalCostDtos;
    }

    @Override
    public TotalCostDto findById(Integer id) {
        TotalCost entity = totalCostRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return totalCostConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(TotalCostDto dto) {
        if (dto == null) {
            return false;
        }
        TotalCost totalCost = totalCostConvert.dtoToEntity(dto);
        totalCost.setId(null);
        totalCostRepository.save(totalCost);
        return true;
    }

    @Override
    public boolean update(Integer id, TotalCostDto dto) {
        TotalCost entity = totalCostRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        entity.setTotalpayment(dto.getTotalpayment());
        entity.setPaymentmethod(dto.getPaymentmethod());
        totalCostRepository.save(entity);
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public TotalCostDto searchTotalCostById(String totalcostId) {
        return null;
    }


    @Override
    public boolean updateTotalCostInMedicalCard(Integer medicalCardId, TotalCostDto dto){
        MedicalCard medicalCard = medicalCardRepository.findById(medicalCardId).get();
        TotalCost totalCost = totalCostRepository.findById(dto.getId()).get();
        if(medicalCard != null && totalCost != null){
            medicalCard.setTotalCost(totalCost);
            medicalCardRepository.save(medicalCard);
            return true;
        }
        return false;
    }

    @Override
    public TotalCostDto getTotalCostAfterInsert(){
        TotalCost totalCost = totalCostRepository.getTotalCostAfterInsert();
        if(totalCost != null){
            return totalCostConvert.entityToDto(totalCost);
        }
        return null;
    }

}
