package com.sharework.health.service.impl;

import com.sharework.health.convert.ScheduleExaminationConvert;
import com.sharework.health.dto.ScheduleExaminationDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.CustomerRepository;
import com.sharework.health.repository.MedicalCardRepository;
import com.sharework.health.repository.ScheduleExaminationRepository;
import com.sharework.health.repository.UserRepository;
import com.sharework.health.service.ScheduleExaminationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class ScheduleExaminationServiceImpl implements ScheduleExaminationService {

    private static final Path CURRENT_FOLDER = Paths.get(System.getProperty("user.dir"));
    private ScheduleExaminationRepository scheduleExaminationRepository;
    private ScheduleExaminationConvert scheduleExaminationConvert;
    private UserRepository userRepository;
    private MedicalCardRepository medicalCardRepository;
    private CustomerRepository customerRepository;

    @Override
    public List<ScheduleExaminationDto> findAll() {
        List<ScheduleExamination> scheduleExaminations = scheduleExaminationRepository.findAll();
        List<ScheduleExaminationDto> scheduleExaminationDtos = new ArrayList<>();
        for (ScheduleExamination entity : scheduleExaminations
        ) {
            ScheduleExaminationDto dto = scheduleExaminationConvert.entityToDto(entity);
            scheduleExaminationDtos.add(dto);
        }
        return scheduleExaminationDtos;
    }

    @Override
    public ScheduleExaminationDto findById(Integer id) {
        ScheduleExamination scheduleExamination = scheduleExaminationRepository.findById(id).get();
        if (scheduleExamination == null) {
            return null;
        }
        return scheduleExaminationConvert.entityToDto(scheduleExamination);
    }

    @Override
    public boolean insert(ScheduleExaminationDto dto) {

        if (dto == null) {
            return false;
        }

        User user = userRepository.findById(dto.getUserDto().getId()).get();
        Customer customer = customerRepository.findById(dto.getCustomerDto().getId()).get();

        MedicalCard medicalCard = null;
        if (dto.getMedicalCardDto() != null) {
            medicalCard = medicalCardRepository.findById(dto.getMedicalCardDto().getId()).get();
        }

        if (user != null && customer != null) {
            ScheduleExamination entity = scheduleExaminationConvert.dtoToEntity(dto);
            entity.setId(null);
            entity.setSignature(null);
            entity.setStatus(Status.DEACTIVE);
            if (dto.getDateOfExamination() == null) {
                entity.setDateOfExamination(LocalDate.now());
            } else {
                entity.setDateOfExamination(dto.getDateOfExamination());
            }
            entity.setUser(user);
            entity.setCustomer(customer);
            entity.setMedicalCard(medicalCard);
            entity.setReasonForConsultation(dto.getReasonForConsultation());
            scheduleExaminationRepository.save(entity);
            return true;
        }
        return false;

    }

    @Override
    public boolean update(Integer id, ScheduleExaminationDto dto) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public ScheduleExaminationDto findScheduleExaminationByCustomer(Integer customerId) {

        ScheduleExamination scheduleExamination = scheduleExaminationRepository.findScheduleExaminationByCustomer(customerId);
        if (scheduleExamination == null) {
            return null;
        }
        return scheduleExaminationConvert.entityToDto(scheduleExamination);
    }

    @Override
    public List<ScheduleExaminationDto> findAllByUser(Integer userId) {
        List<ScheduleExamination> scheduleExaminations = scheduleExaminationRepository.findAllByUser(userId);
        List<ScheduleExaminationDto> scheduleExaminationDtos = new ArrayList<>();
        for (ScheduleExamination entity : scheduleExaminations
        ) {
            if (entity.getStatus() == Status.ACTIVE) {
                ScheduleExaminationDto dto = scheduleExaminationConvert.entityToDto(entity);
                scheduleExaminationDtos.add(dto);
            }
        }
        return scheduleExaminationDtos;
    }

    @Override
    public ScheduleExaminationDto findScheduleExaminationAfterInsert() {
        ScheduleExamination scheduleExamination = scheduleExaminationRepository.getScheduleExaminationAfterInsert();
        if (scheduleExamination == null) {
            return null;
        }
        return scheduleExaminationConvert.entityToDto(scheduleExamination);
    }

//    @Override
//    public boolean activeScheduleExamination(Integer scheduleExaminationId, MultipartFile signature) throws IOException {
//        ScheduleExamination entity = scheduleExaminationRepository.findById(scheduleExaminationId).get();
//        if (entity == null){
//            return false;
//        }
//        if (signature == null){
//            return false;
//        }
//        Path staticPath = Paths.get("static");
//        Path signaturePath = Paths.get("signatures");
//
//        if (!Files.exists(CURRENT_FOLDER.resolve(staticPath).resolve(signaturePath))){
//            Files.createDirectories(CURRENT_FOLDER.resolve(staticPath).resolve(signaturePath));
//        }
//        if (signature != null && entity.getSignature() == null && entity.getStatus() == Status.DEACTIVE){
//            Path path = Paths.get("static/signatures/");
//            try {
//                InputStream inputStream = signature.getInputStream();
//                Files.copy(inputStream, path.resolve(signature.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
//                entity.setSignature(signature.getOriginalFilename().toLowerCase());
//                entity.setStatus(Status.ACTIVE);
//                scheduleExaminationRepository.save(entity);
//                return true;
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//        }
//        return false;
//    }

    @Override
    public boolean activeScheduleExamination(Integer scheduleExaminationId) {
        ScheduleExamination entity = scheduleExaminationRepository.findById(scheduleExaminationId).get();
        if (entity == null) {
            return false;
        }
        if (entity.getSignature() == null && entity.getStatus() == Status.DEACTIVE) {
            entity.setStatus(Status.ACTIVE);
            scheduleExaminationRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public List<ScheduleExaminationDto> getAllScheduleExaminationNearest() {
        List<ScheduleExamination> scheduleExaminations = scheduleExaminationRepository.getAllScheduleExaminationNearest();
        List<ScheduleExaminationDto> scheduleExaminationDtos = new ArrayList<>();

        for (ScheduleExamination entity : scheduleExaminations
        ) {
            ScheduleExaminationDto dto = scheduleExaminationConvert.entityToDto(entity);
            scheduleExaminationDtos.add(dto);
        }
        return scheduleExaminationDtos;
    }

    @Override
    public boolean confirmEndExamination(Integer scheduleExaminationId) {
        ScheduleExamination entity = scheduleExaminationRepository.findById(scheduleExaminationId).get();
        if (entity == null) {
            return false;
        }
        entity.setStatus(Status.EXAMINED);
        scheduleExaminationRepository.save(entity);
        return true;
    }
}
