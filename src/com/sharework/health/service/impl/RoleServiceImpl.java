package com.sharework.health.service.impl;

import com.sharework.health.convert.RoleConvert;
import com.sharework.health.dto.RoleDto;
import com.sharework.health.entity.Role;
import com.sharework.health.entity.Status;
import com.sharework.health.repository.RoleRepository;
import com.sharework.health.service.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;

    private RoleConvert roleConvert;

    @Override
    public List<RoleDto> findAll() {
        List<Role> roles = roleRepository.findAll();
        //System.out.println(roles);
        List<RoleDto> roleDtos = new ArrayList<>();
        for (Role entity: roles
             ) {
            if (entity.getStatus() != Status.DEACTIVE){
                RoleDto dto = roleConvert.entityToDto(entity);
                roleDtos.add(dto);
            }

        }
        return roleDtos;
    }

    @Override
    public RoleDto findById(Integer id) {
        Role entity = roleRepository.findById(id).get();
        System.out.println(entity);
        if (entity == null) {
            return null;
        }
        return roleConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(RoleDto dto) {
        if (dto == null) {
            return false;
        }
        try {
            Role role = roleConvert.dtoToEntity(dto);
            role.setId(null);
            role.setStatus(Status.ACTIVE);
            roleRepository.save(role);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Integer id, RoleDto dto) {
        Role entity = roleRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        try {
            entity = roleConvert.dtoToEntity(dto);
            roleRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        try {
            roleRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
