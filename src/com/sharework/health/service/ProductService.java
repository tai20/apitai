package com.sharework.health.service;

import com.sharework.health.dto.ProductDto;

import java.util.List;

public interface ProductService extends BaseService<ProductDto, Integer> {
    List<ProductDto> searchProductByProductName(String name);

    List<ProductDto> findAllByClinic(Integer clinicId);

    List<ProductDto> findAllByClinicName(String clinicName);
}
