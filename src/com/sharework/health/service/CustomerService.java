package com.sharework.health.service;

import com.sharework.health.dto.CustomerDto;
import com.sharework.health.dto.PasswordDto;

import java.util.List;

public interface CustomerService extends BaseService<CustomerDto, Integer> {
    CustomerDto findByPhoneNumber(String phoneNumber);

    CustomerDto getProfile();

    String changePassword(PasswordDto dto);

    List<CustomerDto> findAllByCustomerCategory(Integer customerCategoryId);
}
