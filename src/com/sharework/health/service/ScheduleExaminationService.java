package com.sharework.health.service;

import com.sharework.health.dto.ScheduleExaminationDto;

import java.util.List;

public interface ScheduleExaminationService extends BaseService<ScheduleExaminationDto, Integer> {

    ScheduleExaminationDto findScheduleExaminationByCustomer(Integer customerId);

    List<ScheduleExaminationDto> findAllByUser(Integer userId);

    ScheduleExaminationDto findScheduleExaminationAfterInsert();

    //boolean activeScheduleExamination(Integer scheduleExaminationId, MultipartFile signature) throws IOException;

    boolean activeScheduleExamination(Integer scheduleExaminationId);

    List<ScheduleExaminationDto> getAllScheduleExaminationNearest();

    boolean confirmEndExamination(Integer scheduleExaminationId);
}
