package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MedicalCardQueryDto {

    private Integer id;

    private String diagnose;

    private String differentDiagnose;

    private String management;

    private LocalDate nextAppointment;

    private String note;

    private String advice;

    private Integer customerId;

    private String customerName;

    private Integer userId;

    private String userName;

    private Integer scheduleExaminationId;

    private Integer testServiceId;

    private Integer PackageTestId;

    private LocalDate dateOfExamination;

}
