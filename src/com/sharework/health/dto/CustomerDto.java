package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CustomerDto {

    private Integer id;

    private String name;

    private String email;

    private LocalDate birthDate;

    private String gender;

    private String address;

    private String cmnd;

    private String phoneNumber;

    private String skinStatus;

    private LocalDateTime createdDate;

}
