package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sharework.health.entity.MedicalCard;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class IndicationCardDto {


    private Integer id;

    private String reasonIndication;

    private LocalDateTime createdDate;

    private UserDto userDto;

    @JsonIgnore
    private List<IndicationCardDetailDto> indicationCardDetailDtos;

}
