package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SlipReturnProductDto {

    private Integer id;

    private LocalDateTime createdDate;

    private String status;

    private CustomerDto customerDto;
}
