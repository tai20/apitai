package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Set;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class IndicationCardDetailDto {


    private IndicationCardDto indicationCardDto;

    private CategoryIndicationCardDto categoryIndicationCardDto;

    private String fileIndication;

    private Set<String> listImages;

    private String status;

    private String liverandkidney;

    private String bilebuctsAndgallbladderandbladder;

    private String abdominalfluid;

    private String uterus;

    private String dkts;

    private String density;

    private String ovary;

    private String endometrium;

    private String cervical;

    private String fornixfluid;

    private String smallfetusultrasoundinformation1;

    private String smallfetusultrasoundinformation2;

    private String blackandwhitefetalultrasoundinformation;

    private String colordoppleroffetalbloodvesselsandnuchaltranslucency;

    private String fetalAnatomy;

    private String pidmuterus;

    private String unbt;

    private String interstitial;

    private String tntc;

    private String colordopplerofFetalbloodvessels;

    private String pregnancyultrasound4d;

    private String fetalanatomyheadfaceneck;

    private String fetalanatomychest;

    private String fetalanatomystomach;

    private String fetalanatomythefourlimbs;

    private String mlt;

    private String bigblackandwhitefetalultrasoundinformation;

    private String conclude;

    private String ctc_dk;

    private String bldb;

    private String blda;

    private String thewomb;
}
