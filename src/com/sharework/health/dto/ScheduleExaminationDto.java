package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ScheduleExaminationDto {

    private Integer id;

    private String status;

    private LocalDate dateOfExamination;

    private String reasonForConsultation;

    private String signature;

    private UserDto userDto;

    private CustomerDto customerDto;

    private MedicalCardDto medicalCardDto;

}
