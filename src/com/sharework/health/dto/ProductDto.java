package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductDto {

    private Integer id;

    private String name;

    private String status;

    private int quantity;

    private BigDecimal wholesalePrice;

    private String unit;

    private int capacity;

    private int totalCapacity;

    private BigDecimal price;

    private ProductCategoryDto productCategoryDto;

    @JsonIgnore
    private List<WarehouseReceiptDetailDto> warehouseReceiptDetailDtos;

    private ClinicDto clinicDto;
}
