package com.sharework.health.dto;

import com.sharework.health.entity.CategoryIndicationCard;
import com.sharework.health.entity.PackageTest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PackageTestServiceDetailDto {

    private Integer id;

    private PackageTestDto packageTestDto;

    private CategoryIndicationCardDto categoryIndicationCardDto;

    private Integer numberofuse;

}
