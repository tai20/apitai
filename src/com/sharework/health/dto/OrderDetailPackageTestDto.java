package com.sharework.health.dto;

import com.sharework.health.entity.Order;
import com.sharework.health.entity.PackageTest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderDetailPackageTestDto {

    private Integer ordertestdetail_id;

    private OrderDto orderDto;

    private PackageTestDto packageTestDto;

    private String quantity;

}
