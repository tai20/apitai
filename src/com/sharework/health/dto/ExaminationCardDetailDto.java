package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExaminationCardDetailDto {

    private ExaminationCardDto examinationCardDto;

    private ServiceDto serviceDto;

    private UserDto userDto;

    private String status;

    private int timeUse;
}
