package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@Entity
@Table(name = "service")
public class Service implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Enumerated(EnumType.STRING)
    private Status status;

    private int period;

    private BigDecimal price;

    @OneToMany(mappedBy = "service")
    private List<TreatmentPackageService> treatmentPackageServices;

    @ManyToOne
    @JoinColumn(name = "service_category_id")
    private ServiceCategory serviceCategory;

    @OneToMany(mappedBy = "service")
    private List<OrderDetailService> orderDetailServices;

    @OneToMany(mappedBy = "service")
    private List<SlipUseDetail> slipUseDetails;

    @OneToMany(mappedBy = "service")
    private List<ProductServiceDetail> productServiceDetails;

    @OneToMany(mappedBy = "service")
    private List<FreeTrial> freeTrials;
}
