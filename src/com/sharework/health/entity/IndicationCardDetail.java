package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@IdClass(IndicationCardDetail_PK.class)
public class IndicationCardDetail implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "indicationcard_id")
    private IndicationCard indicationCard;

    @Id
    @ManyToOne
    @JoinColumn(name = "categoryindicationcard_id")
    private CategoryIndicationCard categoryIndicationCard;

    private String fileIndication;

    @ElementCollection
    @CollectionTable(name = "ListImage",
            joinColumns = { @JoinColumn(name = "indicationcard_id"), @JoinColumn( name = "categoryindicationcard_id")})
    @LazyCollection(LazyCollectionOption.FALSE)
    @Column(name = "listimage", nullable = false)
    private Set<String> listImages;

    private String status;

    private String liverandkidney;

    private String bilebuctsAndgallbladderandbladder;

    private String abdominalfluid;

    private String uterus;

    private String dkts;

    private String density;

    private String ovary;

    private String endometrium;

    private String uterinemuscle;

    private String cervical;

    private String fornixfluid;

    private String smallfetusultrasoundinformation1;

    private String smallfetusultrasoundinformation2;

    private String blackandwhitefetalultrasoundinformation;

    private String colordoppleroffetalbloodvesselsandnuchaltranslucency;

    private String fetalAnatomy;

    private String pidmuterus;

    private String unbt;

    private String interstitial;

    private String tntc;

    private String colordopplerofFetalbloodvessels;

    private String pregnancyultrasound4d;

    private String fetalanatomyheadfaceneck;

    private String fetalanatomychest;

    private String fetalanatomystomach;

    private String fetalanatomythefourlimbs;

    private String mlt;

    private String bigblackandwhitefetalultrasoundinformation;

    private String conclude;

    private String ctc_dk;

    private String bldb;

    private String blda;

    private String thewomb;

}
