package com.sharework.health.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@IdClass(ExaminationCardDetail_PK.class)
public class ExaminationCardDetail implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "examinationcard_id")
    private ExaminationCard examinationCard;

    @Id
    @ManyToOne
    @JoinColumn(name = "service_id")
    private Service service;

    @Id
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Enumerated(EnumType.STRING)
    private ExaminationStatus status;

    private int timeUse;
}
