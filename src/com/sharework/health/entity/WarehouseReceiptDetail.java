package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "WarehouseReceiptDetail")
@IdClass(WarehouseReceiptDetail_PK.class)
public class WarehouseReceiptDetail implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @Id
    @ManyToOne
    @JoinColumn(name = "receipt_id")
    private WarehouseReceipt warehouseReceipt;

    private int numberProductImport;



}
