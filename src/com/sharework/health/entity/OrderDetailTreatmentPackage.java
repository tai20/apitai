package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "orderdetailtreatmentpackage")
@IdClass(OrderDetailTreatmentPackage_PK.class)
public class OrderDetailTreatmentPackage implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    @Id
    @ManyToOne
    @JoinColumn(name = "treatment_id")
    private TreatmentPackage treatmentPackage;

    private int quantity;

    private BigDecimal price;
}
