package com.sharework.health.repository;

import com.sharework.health.entity.WarehouseReceipt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface WarehouseReceiptRepository extends JpaRepository<WarehouseReceipt, Integer> {

    WarehouseReceipt findByClinicName(String name);

    List<WarehouseReceipt> findAllByReceiptType(String receiptType);
    
    List<WarehouseReceipt> findByDateimport(LocalDate dateimport);
    
    @Query(value = "select w.* from warehousereceipt w join clinic c on w.clinic_id = c.id where c.id= :clinicId",
        nativeQuery = true)
    List<WarehouseReceipt> findByClinic(Integer clinicId);

    @Query(nativeQuery = true, value = "select * from warehousereceipt order by id desc limit 1")
    WarehouseReceipt findWarehouseReceiptAfterInsert();
}
