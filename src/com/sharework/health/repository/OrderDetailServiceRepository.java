package com.sharework.health.repository;

import com.sharework.health.entity.OrderDetailService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailServiceRepository extends JpaRepository<OrderDetailService, Integer> {

    @Query(nativeQuery = true, value = "select od.* from orderdetailservice od join orders o on od.order_id = o.id where o.id= :orderId")
    List<OrderDetailService> findAllByOrder(Integer orderId);
}
