package com.sharework.health.repository;

import com.sharework.health.entity.Advisory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface AdvisoryRepository extends JpaRepository<Advisory, Integer> {

    @Query(nativeQuery = true, value = "select a.* from advisory a join customer c on a.customer_id = c.id where c.id= :customerId")
    List<Advisory> findAllByCustomer(Integer customerId);

    @Query(nativeQuery = true, value = "select * from advisory where createddate between :startDate and :endDate")
    List<Advisory> statisticalAdvisoryByCreatedDateIsBetween(LocalDateTime startDate, LocalDateTime endDate);
}
