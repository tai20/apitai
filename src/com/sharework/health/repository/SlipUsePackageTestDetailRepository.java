package com.sharework.health.repository;

import com.sharework.health.dto.SlipUsePackageTestDetailDto;
import com.sharework.health.entity.SlipUsePackageTestDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SlipUsePackageTestDetailRepository extends JpaRepository<SlipUsePackageTestDetail, Integer> {
    @Query(nativeQuery = true, value = "select id.* from slipusepackagetest i join slipusepackagetestdetail id on i.id = id.slipusepackagetest_id where i.id= :slipUsePackageTestId")
    List<SlipUsePackageTestDetail> getSlipUsePackageTestDetailBySlipUsePackageId(Integer slipUsePackageTestId);

}
