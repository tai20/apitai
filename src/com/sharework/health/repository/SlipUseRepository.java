package com.sharework.health.repository;

import com.sharework.health.entity.Customer;
import com.sharework.health.entity.SlipUse;
import com.sharework.health.entity.TreatmentPackage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SlipUseRepository extends JpaRepository<SlipUse, Integer> {

    List<SlipUse> findAllByCustomer(Customer customer);

    List<SlipUse> findAllByTreatmentPackage(TreatmentPackage treatmentPackage);

    @Query(nativeQuery = true, value = "select * from slipuse where enddate >= now()")
    List<SlipUse> findAll();

}
