package com.sharework.health.repository;

import com.sharework.health.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    List<Product> findByNameContains(String name);

    @Query(nativeQuery = true, value = "select p.* from product p join clinic c on p.clinic_id = c.id where c.id= :clinicId")
    List<Product> findAllByClinic(Integer clinicId);

    @Query(nativeQuery = true, value = "select p.* from product p join clinic c on p.clinic_id = c.id where c.name= :clinicName")
    List<Product> findAllByClinicName(String clinicName);

    @Query(nativeQuery = true, value = "select p.*\n" +
            "from warehousereceiptdetail wr join product p on wr.product_id = p.id join warehousereceipt w on w.id = wr.receipt_id\n" +
            "where dateimport between :from and :to")
    List<Product> findAllProductImportFromTo(LocalDate from, LocalDate to);

    @Query(nativeQuery = true, value = "select p.*\n" +
            "from orderdetailproduct op join product p on op.product_id = p.id join orders o on o.id = op.order_id\n" +
            "where date(o.orderdate) between :from and :to")
    List<Product> findAllProductExportFromTo(LocalDate from, LocalDate to);
}
