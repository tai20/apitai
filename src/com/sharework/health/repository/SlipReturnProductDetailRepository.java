package com.sharework.health.repository;

import com.sharework.health.entity.SlipReturnProductDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SlipReturnProductDetailRepository extends JpaRepository<SlipReturnProductDetail, Integer> {

    @Query(nativeQuery = true, value = "select * from slipreturnproductdetail where slipreturnproduct_id= :slipReturnProductId")
    List<SlipReturnProductDetail> findAllBySlipReturnProductId(Integer slipReturnProductId);
}
