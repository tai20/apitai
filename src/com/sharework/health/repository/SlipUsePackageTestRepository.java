package com.sharework.health.repository;

import com.sharework.health.entity.IndicationCard;
import com.sharework.health.entity.IndicationCardDetail;
import com.sharework.health.entity.SlipUsePackageTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SlipUsePackageTestRepository extends JpaRepository<SlipUsePackageTest, Integer> {
    @Query(nativeQuery = true, value = "select a.* from slipusepackagetest a join customer c on a.customer_id = c.id where c.id= :customerId")
    List<SlipUsePackageTest> findAllByCustomer(Integer customerId);

    @Query(nativeQuery = true, value = "select i.* from slipusepackagetest i join medicalcard c on i.id = c.slipusepackagetest_id where c.id= :medicalCardId order by id desc limit 1")
    SlipUsePackageTest getSlipUsePackageTestByMedicalCardId(Integer medicalCardId);
}
