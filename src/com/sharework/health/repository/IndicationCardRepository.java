package com.sharework.health.repository;

import com.sharework.health.entity.IndicationCard;
import com.sharework.health.entity.IndicationCardDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IndicationCardRepository extends JpaRepository<IndicationCard, Integer> {

    @Query(nativeQuery = true, value = "select i.* from indicationcard i join customer c on i.customer_id = c.id where c.id= :customerId")
    List<IndicationCard> getIndicationCardByCustomer(Integer customerId);

    @Query(nativeQuery = true, value = "select i.* from indicationcard i join users u on i.user_id = u.id where u.id= :userId")
    List<IndicationCard> getIndicationCardByUser(Integer userId);


    @Query(nativeQuery = true, value = "select m.* from indicationcard m order by m.id desc limit 1")
    IndicationCard getIndicationCardAfterInsert();

    @Query(nativeQuery = true, value = "select i.* from indicationcard i join customer c on i.customer_id = c.id where c.id= :customerId order by id desc limit 1")
    IndicationCard getIndicationCardLatelyByCustomer(Integer customerId);

    @Query(nativeQuery = true, value = "select i.* from indicationcard i join medicalcard c on i.id = c.indicationcard_id where c.id= :medicalCardId order by id desc limit 1")
    IndicationCard getIndicationCardByMedicalCardId(Integer medicalCardId);
}
