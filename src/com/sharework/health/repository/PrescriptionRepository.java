package com.sharework.health.repository;

import com.sharework.health.entity.Prescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PrescriptionRepository extends JpaRepository<Prescription, Integer> {

    @Query(nativeQuery = true, value = "select * from prescription order by id desc limit 1")
    Prescription getPrescriptionAfterInsert();


}
