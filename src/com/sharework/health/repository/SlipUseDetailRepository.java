package com.sharework.health.repository;

import com.sharework.health.entity.SlipUseDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SlipUseDetailRepository extends JpaRepository<SlipUseDetail, Integer> {

    @Query(nativeQuery = true, value = "select sd.* from slipusedetail sd join slipuse s on sd.slipuse_id = s.id where s.id= :slipUseId")
    List<SlipUseDetail> findAllBySlipUse(Integer slipUseId);

}
