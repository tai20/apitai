package com.sharework.health.repository;

import com.sharework.health.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    /**
     * Find customer exactly
     *
     * @param phoneNumber
     * @return Customer
     */
    Customer findByPhoneNumber(String phoneNumber);

    Customer findByName(String name);

    List<Customer> findByNameLike(String name);

    Customer findByEmail(String email);

    Customer findByCmnd(String cmnd);

    @Query(nativeQuery = true, value = "select * from customer where customer_category_id=1 and createddate between :startDate and :endDate")
    List<Customer> statisticalCustomerByCreatedDateIsBetween(LocalDateTime startDate, LocalDateTime endDate);

    @Query(nativeQuery = true, value = "select c.* from customer c join customercategory cc on c.customer_category_id = cc.id where cc= :customerCategoryId")
    List<Customer> findAllByCustomerCategory(Integer customerCategoryId);

    @Query("select c from Customer c where MONTH(birthdate)= :month")
    List<Customer> findAllByBirthDateWithMonth(Integer month);
}
