package com.sharework.health.convert;

import com.sharework.health.dto.ServiceDto;
import com.sharework.health.dto.SlipUseDetailDto;
import com.sharework.health.dto.SlipUseDto;
import com.sharework.health.entity.SlipUseDetail;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class SlipUseDetailConvert implements BaseConvert<SlipUseDetail, SlipUseDetailDto> {

    @Override
    public SlipUseDetailDto entityToDto(SlipUseDetail entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, SlipUseDetailDto.class)
                .setSlipUseDto(modelMapper.map(entity.getSlipUse(), SlipUseDto.class))
                .setServiceDto(modelMapper.map(entity.getService(), ServiceDto.class));
    }

    @Override
    public SlipUseDetail dtoToEntity(SlipUseDetailDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, SlipUseDetail.class);
    }
}
