package com.sharework.health.convert;

import com.sharework.health.dto.ExaminationCardDetailDto;
import com.sharework.health.dto.ExaminationCardDto;
import com.sharework.health.dto.ServiceDto;
import com.sharework.health.dto.UserDto;
import com.sharework.health.entity.ExaminationCardDetail;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ExaminationCardDetailConvert implements BaseConvert<ExaminationCardDetail, ExaminationCardDetailDto> {

    @Override
    public ExaminationCardDetailDto entityToDto(ExaminationCardDetail entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, ExaminationCardDetailDto.class)
                .setExaminationCardDto(modelMapper.map(entity.getExaminationCard(), ExaminationCardDto.class))
                .setServiceDto(modelMapper.map(entity.getService(), ServiceDto.class))
                .setUserDto(modelMapper.map(entity.getUser(), UserDto.class));
    }

    @Override
    public ExaminationCardDetail dtoToEntity(ExaminationCardDetailDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, ExaminationCardDetail.class);
    }
}
