package com.sharework.health.convert;

import com.sharework.health.dto.*;
import com.sharework.health.entity.MedicalCard;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class MedicalCardConvert implements BaseConvert<MedicalCard, MedicalCardDto> {

    @Override
    public MedicalCardDto entityToDto(MedicalCard entity) {
        ModelMapper modelMapper = new ModelMapper();

        //Convert
        TestServiceConvert testServiceConvert = new TestServiceConvert();

        SlipUsePackageTestConvert slipUsePackageTestConvert = new SlipUsePackageTestConvert();

        TotalCostConvert totalCostConvert = new TotalCostConvert();

        PrescriptionConvert prescriptionConvert = new PrescriptionConvert();

        IndicationCardConvert indicationCardConvert = new IndicationCardConvert();

        MedicalCardDto medicalCardDto = new MedicalCardDto();

        medicalCardDto = modelMapper.map(entity, MedicalCardDto.class)
                .setTestServiceDto(testServiceConvert.entityToDto(entity.getTestService()));
        //  .setSlipUsePackageTestDto(slipUsePackageTestConvert.entityToDto(entity.getSlipUsePackageTest()));

        if (entity.getSlipUsePackageTest() == null) {
            medicalCardDto.setSlipUsePackageTestDto(null);
        } else {
            medicalCardDto.setSlipUsePackageTestDto(slipUsePackageTestConvert.entityToDto(entity.getSlipUsePackageTest()));
        }

        if (entity.getTotalCost() == null) {
            medicalCardDto.setTotalCostDto(null);
        } else {
            medicalCardDto.setTotalCostDto(totalCostConvert.entityToDto(entity.getTotalCost()));
        }
        if (entity.getPrescription() == null) {
            medicalCardDto.setPrescriptionDto(null);
        } else {
            medicalCardDto.setPrescriptionDto(prescriptionConvert.entityToDto(entity.getPrescription()));
        }
        if (entity.getIndicationCard() == null) {
            medicalCardDto.setIndicationCardDto(null);
        } else {
            medicalCardDto.setIndicationCardDto(indicationCardConvert.entityToDto(entity.getIndicationCard()));
        }

        return medicalCardDto;
    }

    @Override
    public MedicalCard dtoToEntity(MedicalCardDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, MedicalCard.class);
    }
}
