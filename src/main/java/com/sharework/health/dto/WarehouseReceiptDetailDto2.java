package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WarehouseReceiptDetailDto2 {
    private Integer numberProductImport;
    private Integer product_id;
    private Integer receipt_id;

}
