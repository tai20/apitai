package com.sharework.health.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sharework.health.entity.Clinic;
import com.sharework.health.entity.ExportReceiptDetails;
import com.sharework.health.entity.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExportReceiptDto {
    private Integer id;

    private ClinicDto clinicDto;

    private UserDto userDto;

    private OrderDto orderDto;

    private LocalDateTime dateexport;

    private BigDecimal sumprice;

    private String reason;

    private String status;

    @JsonIgnore
    private List<ExportReceiptDetails> exportReceiptDetails;

    @JsonIgnore
    private List<Order> orders;
}
