package com.sharework.health.dto;

import com.sharework.health.entity.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RankCustomerDto {
    private CustomerDto customer;
    String nameRank;
}
