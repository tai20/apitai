package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sharework.health.entity.Clinic;
import com.sharework.health.entity.TransferReceiptDetails;
import com.sharework.health.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TransferReceiptDto implements Serializable {

    private Integer id;

    private UserDto userDto;

    private ClinicDto clinicSendingDto;

    private ClinicDto clinicRecevingDto;

    private LocalDateTime dateexport;

    private LocalDateTime dateimport;

    private String status;


    @JsonIgnore
    List<TransferReceiptDetails> transferReceiptDetails;
}
