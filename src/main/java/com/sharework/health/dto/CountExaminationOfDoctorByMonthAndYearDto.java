package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Accessors
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CountExaminationOfDoctorByMonthAndYearDto {

    private Integer userId;

    private String userName;

    private Integer clinicId;

    private String clinicName;

    private Integer serviceId;

    private String serviceName;

    private Integer customerId;

    private String customerName;

    //private LocalDateTime dateOfExamination;

    private Long countExamination;

}
