package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GetReceivingClinicStockDto implements Serializable {
    private Integer clinic_receving;

    private Integer product_id;

    private Integer number_product_transfer;

}
