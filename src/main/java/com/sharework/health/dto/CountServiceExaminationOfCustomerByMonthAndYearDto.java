package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Accessors
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CountServiceExaminationOfCustomerByMonthAndYearDto {

    private Integer customerId;

    private String customerName;

    private Integer serviceId;

    private String serviceName;

    private Integer clinicId;

    private String clinicName;

    private long countService;
}
