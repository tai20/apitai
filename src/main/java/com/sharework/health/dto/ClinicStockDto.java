package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sharework.health.entity.ExportReceiptDetails;
import com.sharework.health.entity.ImportReceiptDetails;
import com.sharework.health.entity.OthersWarehouseReceiptDetails;
import com.sharework.health.entity.TransferReceiptDetails;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClinicStockDto {
    private Integer id;

    private Integer quantity;

    private String unit;

    private ClinicDto clinicDto;

    private ProductDto productDto;

    private ProductCategoryDto productCategoryDto;
    @JsonIgnore
    private List<ImportReceiptDetails> importReceiptDetails;

    @JsonIgnore
    private List<ExportReceiptDetails> exportReceiptDetails;

    @JsonIgnore
    private List<TransferReceiptDetails> transferReceiptDetails;

    @JsonIgnore
    private List<OthersWarehouseReceiptDetails> othersWarehouseReceiptDetails;
}
