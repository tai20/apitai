package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExportReceiptDetailsDto {
    private ExportReceiptDto exportReceiptDto;

    private ClinicStockDto clinicStockDto;

    private Integer number_product_export;

    private BigDecimal export_price;

    private ProductBatchDto productBatchDto;

}
