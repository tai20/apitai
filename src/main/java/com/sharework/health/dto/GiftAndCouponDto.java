package com.sharework.health.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class GiftAndCouponDto {

    private  Integer id;

    private  String name;

    private LocalDate startDate;

    private  LocalDate endDate;

    private Integer times;

    private String type;

    private ServiceCategoryDto service;

    private double percent;

    private String code;
}
