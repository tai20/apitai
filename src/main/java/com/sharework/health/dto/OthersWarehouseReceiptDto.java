package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class OthersWarehouseReceiptDto {
    private Integer id;

    private UserDto userDto;

    private ClinicDto clinicDto;

    private Boolean type;

    private LocalDateTime datecreate;

    private String content;

    private String status;
}
