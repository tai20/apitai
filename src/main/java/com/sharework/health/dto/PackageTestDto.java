package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PackageTestDto {

    private Integer packagetest_id;

    private String packagetestname;

    private String status;

    private BigDecimal price;

    private Integer numberofuse;

    private TestServiceDto testServiceDto;


}
