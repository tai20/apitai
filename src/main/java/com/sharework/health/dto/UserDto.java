package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sharework.health.entity.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserDto {

    private Integer id;

    private String name;

    private String email;

    private String password;

    private LocalDate birthDate;

    private String gender;

    private String address;

    private String phoneNumber;

    private Status status;

    private String certificate;

    private int workExperience;

    private String cmnd;

    private RoleDto roleDto;

    @JsonIgnore
    private List<SalaryDto> salaryDtos;

    @JsonIgnore
    private List<OrderDto> orderDtos;

    @JsonIgnore
    private List<ExaminationCardDetailDto> examinationCardDetailDtos;

    private ClinicDto clinicDto;
}
