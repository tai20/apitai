package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SlipUseQuery {
    private Integer idService;
    private Integer idSlipuse;
    private Integer useofstock;
}
