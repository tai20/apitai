package com.sharework.health.dto;

import com.sharework.health.entity.Clinic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CustomerLevelDto {
    private CustomerDto customer_id;
    private LevelDto level_id;
    private BigDecimal total_order_value;
}
