package com.sharework.health.dto;

import com.sharework.health.entity.Customer;
import com.sharework.health.entity.TestService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDate;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SlipUsePackageTestDto {

    private Integer id;

    private CustomerDto customerDto;

    private TestServiceDto testServiceDto;

    private LocalDate startdate;

    private LocalDate enddate;

    private String status;

    private BigDecimal price;

    private Integer numberofuse;

    private String name;
}
