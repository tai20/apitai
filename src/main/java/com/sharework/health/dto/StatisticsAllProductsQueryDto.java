package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class StatisticsAllProductsQueryDto {
    private Integer product_id;

    private String product_name;

    private String product_category;

    private String unit;

    private BigDecimal import_price;

    private BigDecimal export_price;

    private String warehouse_name;

    private Integer warehouse_id;

    private Integer quantity_import;

    private Integer quantity_export;

    private Integer stock_first;

    private Integer stock_end;

    private String type;
}
