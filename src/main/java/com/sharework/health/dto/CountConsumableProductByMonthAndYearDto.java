package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CountConsumableProductByMonthAndYearDto {

    private Integer serviceId;

    private Integer productId;

    private String productName;

    private String unit;

    private Integer clinicId;

    private String clinicName;

    private Long countConsumableProduct;
}
