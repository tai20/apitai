package com.sharework.health.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data

public class GiftDto  {

    private  Integer id;

    private  String name;

    private  LocalDate startDate;

    private  LocalDate endDate;

    private Integer quantity;

    @JsonIgnore
    private List<GiftListDto> giftListDtos;

}
