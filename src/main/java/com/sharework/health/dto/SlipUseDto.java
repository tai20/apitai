package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SlipUseDto {

    private Integer id;

    private String nameSlipUse;

    private LocalDate startDate;

    private LocalDate endDate;

    private String status;

    private String imageBefore;

    private String imageAfter;


    private TreatmentPackageDto treatmentPackageDto;

    private CustomerDto customerDto;

}
