package com.sharework.health.service;

import com.sharework.health.dto.LoginDto;

public interface AuthService {

    String auth(LoginDto dto);
}
