package com.sharework.health.service;

import com.sharework.health.dto.ProductServiceDetailDto;
import com.sharework.health.dto.ServiceDto;
import com.sharework.health.entity.Service;
import com.sharework.health.response.ProductReponse;
import com.sharework.health.response.ServiceReponse;

import java.util.List;

public interface ServiceService extends BaseService<ServiceDto, Integer> {

    boolean addProductServiceDetail(ProductServiceDetailDto dto);

    List<ProductServiceDetailDto> findAllByServiceId(Integer serviceId);

    ServiceDto findServiceAfterInsert();

    List<ServiceDto> findAllByCustomerId(Integer customerId);

    ServiceReponse getAllService (int pageNo, int pageSize, String sortBy, String sortDir);

    ServiceReponse findAllServiceByName(int pageNo, int pageSize, String sortBy, String sortDir, String name);

}
