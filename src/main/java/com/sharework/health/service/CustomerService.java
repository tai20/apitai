package com.sharework.health.service;

import com.sharework.health.dto.*;
import com.sharework.health.entity.Customer;
import com.sharework.health.entity.CustomerLevel;
import com.sharework.health.entity.GiftList;
import com.sharework.health.response.CustomerReponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public interface CustomerService extends BaseService<CustomerDto, Integer> {
    CustomerDto findByPhoneNumber(String phoneNumber);

    CustomerDto getProfile();

    String changePassword(PasswordDto dto);

    CustomerDto findCustomerAfterInsert();

  boolean insertCustomer(String name, String email,
                              String birthDate, String gender,
                              String address, String cmnd,
                              String phone, String skinStatus,
                              String customerResource, MultipartFile avatar,
                              Integer clinicId, String typeCustomer) throws IOException;

    boolean addCustomerLevel(CustomerLevelDto dto);
    boolean updateCustomerLevel(CustomerLevelDto dto);
    boolean deleteCustomerLevel(Integer id);

    List<CustomerLevelDto> findByUID(Integer id);
    List<CustomerLevelDto> findAllByAdmin();

    List<CustomerDto> findAllByCustomerCategory(Integer customerCategoryId);

    List<CustomerLevelDto> findAllByType(String type);

    boolean updateAvatar(Integer id, MultipartFile avatar) throws IOException;

    List<String> getListNotification();

    List<RankCustomerDto> findAllRankCustomer();

    CustomerDto getCustomerByIDSU(Integer id);

    CustomerReponse getAllCustomer(int pageNo, int pageSize, String sortBy, String sortDir);
    CustomerReponse findAllByType(int pageNo, int pageSize, String sortBy, String sortDir,String type);
    CustomerReponse findByType(int pageNo, int pageSize, String sortBy, String sortDir,String type, String index);
    CustomerReponse findByTypeOfAdmin(int pageNo, int pageSize, String sortBy, String sortDir, String index);
}
