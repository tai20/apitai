package com.sharework.health.service;

import com.sharework.health.dto.GetIdAndNameProduct;
import com.sharework.health.dto.ProductDto;
import com.sharework.health.entity.Product;
import com.sharework.health.response.ProductReponse;

import java.util.List;

public interface ProductService extends BaseService<ProductDto, Integer> {
    List<ProductDto> searchProductByProductName(String name);

    List<ProductDto> findAllByClinic(Integer clinicId);

    List<ProductDto> findAllByClinicName(String clinicName);

    List<GetIdAndNameProduct> findAllOfProductBatch();

    boolean deactiveProduct(Integer id);

    boolean activeProduct(Integer id);

    ProductReponse getAllProduct(int pageNo, int pageSize, String sortBy, String sortDir);

    ProductReponse findAllByName(int pageNo, int pageSize, String sortBy, String sortDir, String name);
}
