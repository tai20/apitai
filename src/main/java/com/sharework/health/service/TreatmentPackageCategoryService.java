package com.sharework.health.service;

import com.sharework.health.dto.ServiceCategoryDto;

public interface TreatmentPackageCategoryService extends BaseService<ServiceCategoryDto, Integer> {

    ServiceCategoryDto searchTreatmentPackageCategoryByName (String name);
}
