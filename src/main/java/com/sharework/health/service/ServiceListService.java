package com.sharework.health.service;

import com.sharework.health.dto.ServiceListDto;
import com.sharework.health.service.BaseService;

import java.util.List;

public interface ServiceListService extends BaseService<ServiceListDto, Integer> {
    List<ServiceListDto> findAllServiceListByCustomer(Integer customerId);
}
