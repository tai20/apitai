package com.sharework.health.service;

import com.sharework.health.dto.*;
import com.sharework.health.entity.ExportReceipt;
import com.sharework.health.entity.ImportReceipt;
import com.sharework.health.response.ExportReceiptReponse;
import com.sharework.health.response.ImportRecepitReponse;

import java.util.ArrayList;
import java.util.List;

public interface ExportReceiptService extends BaseService<ExportReceiptDto, Integer> {
    public ExportReceiptDto insertExportReceipt(ExportReceiptDto dto);

    public ExportReceiptDetailsDto insertExportReceiptDetail(ExportReceiptDetailsDto dto);

    public List<ExportReceiptDetailsQueryDto> getAllExportReceiptDetailByReceiptid(Integer receipt_id);

    public boolean lockExportReceipt(Integer receipt_id);

    public boolean deleteExportReceipt(Integer id);

    public boolean deleteExportReceiptDetail(Integer receipt_id, Integer clinic_stock_id);

    public ExportReceiptDetailsQueryDto updateExportReceiptDetail(ExportReceiptDetailsQueryDto dto);

    public boolean restoreExportReceipt(Integer id);

    public List<ExportReceiptDto> getListExportReceiptByClinicId(Integer id);

    ExportReceiptReponse getAllIExportReceipt(int pageNo, int pageSize, String sortBy, String sortDir);
    ExportReceiptReponse  getAllExportReceiptClinic(int pageNo, int pageSize, String sortBy, String sortDir, int id);
}