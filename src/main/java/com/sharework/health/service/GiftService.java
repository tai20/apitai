package com.sharework.health.service;

import com.sharework.health.dto.*;

import java.security.Principal;
import java.util.List;
import java.util.Map;

public interface GiftService extends BaseService<GiftDto, Integer> {

    List<GiftListDto> findGiftListByGID(Integer gId);

    CouponGiftDto getCouponGift();

    List<GiftAndCouponDto> getAllCouponGift();

    GiftDetailDto getDetalGiftDtosById(Integer idGift);

    boolean addGiftList(GiftListDto dto);
    boolean deleteGiftList(GiftListDto dto);
    boolean updateGiftList(Integer id, GiftListDto dto);

    GiftDto findGiftAfterInsert();

    boolean reloadTimesGift(Integer[] ids);

    List<GiftAndCouponDto> findByTimes(Integer times);



    List<GiftUserDto> getGiftUser(Principal principal, Integer id_customer);
}


