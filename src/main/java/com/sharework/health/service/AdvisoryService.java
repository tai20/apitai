package com.sharework.health.service;

import com.sharework.health.dto.AdvisoryDto;

import java.util.List;

public interface AdvisoryService extends BaseService<AdvisoryDto, Integer> {
    List<AdvisoryDto> findAllByCustomer(Integer customerId);
}
