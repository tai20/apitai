package com.sharework.health.service.impl;

import com.sharework.health.convert.BillConvert;
import com.sharework.health.dto.BillDto;
import com.sharework.health.dto.OrderDto;
import com.sharework.health.entity.Bill;
import com.sharework.health.entity.Order;
import com.sharework.health.repository.BillRepository;
import com.sharework.health.repository.OrderRepository;
import com.sharework.health.service.BillService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(rollbackOn = Exception.class)
@AllArgsConstructor
public class BillServiceImpl implements BillService {
    private BillRepository billRepository;

    private BillConvert billConvert;

    private OrderRepository orderRepository;

    @Override
    public List<BillDto> findAll() {
        List<Bill> bills = billRepository.findAll();
        List<BillDto> billDtos = new ArrayList<>();

        for(Bill entity:  bills){
            BillDto dto = billConvert.entityToDto(entity);
            billDtos.add(dto);
        }
        return billDtos;
    }

    @Override
    public BillDto findById(Integer id) {
        return null;
    }

    @Override
    public boolean insert(BillDto dto) {
        return false;
    }

    @Override
    public boolean insert(OrderDto orderDto) {
        BillDto dto = new BillDto();
        Order order = orderRepository.findById(orderDto.getId()).get();
        if(order != null){
            Bill entity = billConvert.dtoToEntity(dto);
            entity.setOrder(order);
            entity.setPaymentDate(order.getOrderDate());
            entity.setPaymentAmount(order.getTotal());
            entity.setPaymentMethod(order.getPaymentMethod());

            billRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public boolean update(Integer id, BillDto dto) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        Bill bill = billRepository.findById(id).get();
        if(bill == null){
            return false;
        }
        billRepository.delete(bill);
        return true;
    }

    @Override
    public List<BillDto> findBillByOrder(Integer orderId) {
        //Bill bill = billRepository.findByOrderId(orderID);
//        Order order = orderRepository.findById(orderID).get();
//        if(order.getStatus().equals(OrderStatus.Paid) == true){
//            //Bill entity = billConvert.dtoToEntity();
//
        List<Bill> bills = billRepository.findByOrderId(orderId);
        List<BillDto> billDtos = new ArrayList<>();

        for(Bill entity: bills){
            BillDto dto = billConvert.entityToDto(entity);
            billDtos.add(dto);
        }
        return billDtos;

    }
}
