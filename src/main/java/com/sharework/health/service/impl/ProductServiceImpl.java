package com.sharework.health.service.impl;

import com.sharework.health.convert.ProductConvert;
import com.sharework.health.dto.CustomerLevelDto;
import com.sharework.health.dto.GetIdAndNameProduct;
import com.sharework.health.dto.ProductDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.response.CustomerReponse;
import com.sharework.health.response.ProductReponse;
import com.sharework.health.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    private ProductConvert productConvert;

    private ProductCategoryRepository productCategoryRepository;

    private ClinicRepository clinicRepository;

    private ClinicStockRepository clinicStockRepository;

    private ProductStatisticsRepository productStatisticsRepository;


    @Override
    public List<ProductDto> findAll() {
        List<Product> products = productRepository.findAll();
        List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

        List<ProductDto> productDtos = new ArrayList<>();

        for (Product entity: products) {
            for (ClinicStock entity2 : clinicStocks){
                if(entity.getId() == entity2.getProduct().getId()){
                    entity.setQuantity(productStatisticsRepository.getSumProduct(entity.getId()));
                }
                productRepository.save(entity);
            }
            ProductDto dto = productConvert.entityToDto(entity);
            productDtos.add(dto);
        }
        return productDtos;
    }

    @Override
    public List<GetIdAndNameProduct> findAllOfProductBatch() {
        List<GetIdAndNameProduct> products = productRepository.getIdAndNameProduct();
        return products;
    }



    @Override
    public ProductDto findById(Integer id) {
        Product entity = productRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return productConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(ProductDto dto) {
        if (dto == null) {
            return false;
        }
        ProductCategory productCategory = productCategoryRepository.findById(dto.getProductCategoryDto().getId()).get();
        //Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
        if (productCategory == null){
            return false;
        }
            Product product = productConvert.dtoToEntity(dto);
            if (dto.getCapacity() <=0 ){
                int totalCapacity = dto.getQuantity() * dto.getCapacity();
                product.setTotalCapacity(totalCapacity);
            }
            product.setId(null);
            product.setStatus(Status.ACTIVE);
            product.setProductCategory(productCategory);
            //product.setClinic(clinic);
            Product result =  productRepository.save(product);

            // Thêm các sản phẩm vào clinicstock
            List<Clinic> clinics = clinicRepository.findAll();
            for (int i=0; i<clinics.size(); i++){
                ClinicStock clinicStock = new ClinicStock()
                        .setId(null)
                        .setQuantity(0)
                        .setUnit(result.getUnit())
                        .setClinic(clinics.get(i))
                        .setProduct(result);
                clinicStockRepository.save(clinicStock);

            }

            return true;

    }

    @Override
    public boolean update(Integer id, ProductDto dto) {
        Product entity = productRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        ProductCategory productCategory = productCategoryRepository.findById(dto.getProductCategoryDto().getId()).get();
        //Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
        if (productCategory == null ){
            return false;
        }
        entity.setName(dto.getName());
        entity.setStatus(Status.ACTIVE);
        entity.setQuantity(dto.getQuantity());
        entity.setWholesalePrice(dto.getWholesalePrice());
        entity.setUnit(dto.getUnit());
        entity.setCapacity(dto.getCapacity());
        entity.setTotalCapacity(dto.getQuantity() * dto.getCapacity());
        entity.setPrice(dto.getPrice());
        entity.setProductCategory(productCategory);
        //entity.setClinic(clinic);
        productRepository.save(entity);
        return true;

    }

    @Override
    public boolean delete(Integer id) {
        Product product = productRepository.findById(id).get();

        if (product == null) {
            return false;
        }
        List<ClinicStock> clinicStocks = clinicStockRepository.getClinicStockByProductId(id);
//        tìm kiếm sp có ở các phiếu hay không, nếu có thì trả về false
        for (ClinicStock clinicStockEntity:clinicStocks){
            List<ClinicStock> listSearchImport= clinicStockRepository.getClinicStockFromImportReceipt(clinicStockEntity.getId());
            List<ClinicStock> listSearchExport= clinicStockRepository.getClinicStockFromExportReceipt(clinicStockEntity.getId());
            List<ClinicStock> listSearchTransfer= clinicStockRepository.getClinicStockFromTransferReceipt(clinicStockEntity.getId());
            List<ClinicStock> listSearchOther= clinicStockRepository.getClinicStockFromOtherReceipt(clinicStockEntity.getId());
            if (!listSearchImport.isEmpty() || !listSearchExport.isEmpty() || !listSearchTransfer.isEmpty() || !listSearchOther.isEmpty()) return false;
        }
//        Xóa product nếu không có ở các phiếu
            for (ClinicStock clinicStockEntity:clinicStocks){
                clinicStockRepository.delete(clinicStockEntity);
            }
            productRepository.delete(product);
            return true;

    }



    @Override
    public List<ProductDto> searchProductByProductName(String name) {
        List<Product> products = productRepository.findByNameContains(name);
        List<ProductDto> productDtos = new ArrayList<>();
        for (Product entity: products
             ) {
            ProductDto dto = productConvert.entityToDto(entity);
            productDtos.add(dto);
        }
        return productDtos;
    }

    @Override
    public List<ProductDto> findAllByClinic(Integer clinicId) {
        List<Product> products = productRepository.findByClinicStocks_Clinic_Id(clinicId);
        List<ProductDto> productDtos = new ArrayList<>();
        for (Product entity: products
        ) {
            ProductDto dto = productConvert.entityToDto(entity);
            productDtos.add(dto);
        }
        return productDtos;
    }

    @Override
    public List<ProductDto> findAllByClinicName(String clinicName) {
        List<Product> products = productRepository.findByClinicStocks_Clinic_NameEquals(clinicName);
        List<ProductDto> productDtos = new ArrayList<>();
        for (Product entity: products
        ) {
            ProductDto dto = productConvert.entityToDto(entity);
            productDtos.add(dto);
        }
        return productDtos;
    }



    @Override
    public boolean deactiveProduct(Integer id){
        Product product = productRepository.findById(id).get();
        if (product.getStatus() == Status.ACTIVE){
            product.setStatus(Status.DEACTIVE);
            productRepository.save(product);
            return true;
        }
            return false;

    }

    @Override
    public boolean activeProduct(Integer id){
        Product product = productRepository.findById(id).get();
        if (product.getStatus() == Status.DEACTIVE){
            product.setStatus(Status.ACTIVE);
            productRepository.save(product);
            return true;
        }
        return false;
    }

    @Override
    public ProductReponse getAllProduct(int pageNo, int pageSize, String sortBy, String sortDir) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize,sort);

        Page<Product> products = productRepository.findAll(pageable);
        List<Product> listOfProducts = products.getContent();

        List<ProductDto> productDtos = listOfProducts.stream().map(post -> productConvert.entityToDto(post)).collect(Collectors.toList());

        ProductReponse productResponse = new ProductReponse();
        productResponse.setData(productDtos);
        productResponse.setCurrent(products.getNumber());
        productResponse.setPageSize(products.getSize());
        productResponse.setTotalElements(products.getTotalElements());
        productResponse.setTotalPages(products.getTotalPages());
        productResponse.setLast(products.isLast());

        return productResponse;
    }

    @Override
    public ProductReponse findAllByName(int pageNo, int pageSize, String sortBy, String sortDir, String input) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize,sort);

        boolean isNum = false;
        isNum = input.matches("[0-9]+[\\.]?[0-9]*");
        Page<Product> products = null;

        if(isNum == false) {
            products = productRepository.findByName(pageable, input);
        }
        if(isNum == true) {
            products = productRepository.findByCategory(pageable, Integer.valueOf(input));
        }


        List<Product> listOfProducts = products.getContent();

        List<ProductDto> test = listOfProducts.stream().map(post -> productConvert.entityToDto(post)).collect(Collectors.toList());
        ProductReponse productResponse = new ProductReponse();
        productResponse.setData(test);
        productResponse.setCurrent(products.getNumber());
        productResponse.setPageSize(products.getSize());
        productResponse.setTotalElements(products.getTotalElements());
        productResponse.setTotalPages(products.getTotalPages());
        productResponse.setLast(products.isLast());

        return productResponse;
    }

}
