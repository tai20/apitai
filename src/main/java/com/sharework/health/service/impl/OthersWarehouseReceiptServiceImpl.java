package com.sharework.health.service.impl;

import com.sharework.health.convert.OthersWarehouseReceiptConvert;
import com.sharework.health.convert.OthersWarehouseReceiptDetailsConvert;
import com.sharework.health.dto.OtherReceiptDetailsQueryDto;
import com.sharework.health.dto.OthersWarehouseReceiptDetailsDto;
import com.sharework.health.dto.OthersWarehouseReceiptDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.service.OthersWarehouseReceiptService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class OthersWarehouseReceiptServiceImpl implements OthersWarehouseReceiptService {

    private OthersWarehouseReceiptConvert othersWarehouseReceiptConvert;

    private OthersWarehouseReceiptRepository othersWarehouseReceiptRepository;

    private UserRepository userRepository;

    private ClinicRepository clinicRepository;

    private ClinicStockRepository clinicStockRepository;

    private OthersWarehouseReceiptDetailsConvert othersWarehouseReceiptDetailsConvert;

    private OthersWarehouseReceiptDetailsRepository othersWarehouseReceiptDetailsRepository;

    private ProductBatchRepository productBatchRepository;

    private ProductRepository productRepository;

    private ProductStatisticsRepository productStatisticsRepository;


    @Override
    public List<OthersWarehouseReceiptDto> findAll(){
        List<OthersWarehouseReceipt> othersWarehouseReceipts = othersWarehouseReceiptRepository.findAllSortDateDESC();
        List<OthersWarehouseReceiptDto> othersWarehouseReceiptDtos =  new ArrayList<>();
        for(OthersWarehouseReceipt entity:othersWarehouseReceipts){
            OthersWarehouseReceiptDto dto = othersWarehouseReceiptConvert.entityToDto(entity);
            othersWarehouseReceiptDtos.add(dto);
        }

        return othersWarehouseReceiptDtos;
    }

    //ham lay danh sach phieu nhap xuat khac theo kho
    @Override
    public List<OthersWarehouseReceiptDto> getAllOtherReceiptClinicDetailByReceipt(Integer clinic_id){
        List<OthersWarehouseReceipt> othersWarehouseReceipts =
                othersWarehouseReceiptRepository.findAllClinicSortDateDESC(clinic_id);
        List<OthersWarehouseReceiptDto> othersWarehouseReceiptDtos =  new ArrayList<>();
        for(OthersWarehouseReceipt entity:othersWarehouseReceipts){
            OthersWarehouseReceiptDto dto = othersWarehouseReceiptConvert.entityToDto(entity);
            othersWarehouseReceiptDtos.add(dto);
        }

        return othersWarehouseReceiptDtos;
    }

    @Override
    public OthersWarehouseReceiptDto findById(Integer id){
        Optional<OthersWarehouseReceipt> foundDto = othersWarehouseReceiptRepository.findById(id);
        if (foundDto.isPresent()){
            OthersWarehouseReceipt entity = othersWarehouseReceiptRepository.findById(id).get();
            return othersWarehouseReceiptConvert.entityToDto(entity);
        }
        return null;
    }

    @Override
    public boolean insert(OthersWarehouseReceiptDto dto){
        return false;
    }

    @Override
    public boolean update(Integer id, OthersWarehouseReceiptDto othersWarehouseReceiptDto){
        return  false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }


    @Override
    public OthersWarehouseReceiptDto insertOtherReceipt(OthersWarehouseReceiptDto dto){
        OthersWarehouseReceipt entity = othersWarehouseReceiptConvert.dtoToEntity(dto);

        Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
        User user = userRepository.findById(dto.getUserDto().getId()).get();

        entity.setId(null);
        if(clinic == null || user == null){
            return null;
        } else{
            entity.setClinic(clinic);
            entity.setUser(user);
            entity.setDatecreate(LocalDateTime.now());
            entity.setType(dto.getType());
            entity.setContent(dto.getContent());
            entity.setStatus(ReceiptStatus.ACTIVE);
        }
        OthersWarehouseReceipt result = othersWarehouseReceiptRepository.save(entity);
        return othersWarehouseReceiptConvert.entityToDto(result);
    }


    @Override
    public OthersWarehouseReceiptDetailsDto insertOtherReceiptDetail(OthersWarehouseReceiptDetailsDto dto){

        OthersWarehouseReceiptDetails entity = othersWarehouseReceiptDetailsConvert.dtoToEntity(dto);
        OthersWarehouseReceipt othersWarehouseReceipt = othersWarehouseReceiptRepository.findById(dto.getOthersWarehouseReceiptDto().getId()).get();
        ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinicStockDto().getId()).get();

        ProductBatch productBatch = productBatchRepository.findById(dto.getProductBatchDto().getId()).get();
        ClinicStock clinicStockByClinicIdAndAndProductId = clinicStockRepository.getClinicStockByClinicIdAndAndProductId(clinicStock.getId(),productBatch.getProductId().getId());
        List<OthersWarehouseReceiptDetails> othersWarehouseReceiptDetails = othersWarehouseReceiptDetailsRepository.getOtherReceiptByClinicStockIdReceiptId(dto.getOthersWarehouseReceiptDto().getId(), clinicStockByClinicIdAndAndProductId.getId());
        if (othersWarehouseReceipt.getClinic().getId() != clinicStock.getClinic().getId() || dto.getNumber_product()<=0 || clinicStock.getQuantity() < dto.getNumber_product()) return null;
        if (othersWarehouseReceipt == null || productBatch == null || clinicStock == null || othersWarehouseReceipt.getStatus() != ReceiptStatus.ACTIVE ||clinicStock.getQuantity()<0){
            return null;
        } else
            // Xem sản phẩm có trong phiếu hay chưa, nếu có thì cộng/trừ dồn, nếu chưa thì cộng mới
            if (othersWarehouseReceiptDetails.isEmpty()){
                if(othersWarehouseReceipt.getType() == true){
                    clinicStockByClinicIdAndAndProductId.setQuantity(dto.getNumber_product() + clinicStockByClinicIdAndAndProductId.getQuantity());
                    productBatch.setQuantity(dto.getNumber_product() + productBatch.getQuantity());
                } else{
                    clinicStockByClinicIdAndAndProductId.setQuantity(clinicStock.getQuantity() - dto.getNumber_product());
                    if((productBatch.getQuantity() - dto.getNumber_product())<0){
                        return null;
                    }
                    productBatch.setQuantity(productBatch.getQuantity() - dto.getNumber_product());
                }
                entity.setNumber_product(dto.getNumber_product());
            } else{
                if(othersWarehouseReceipt.getType() == true){
                    clinicStockByClinicIdAndAndProductId.setQuantity(dto.getNumber_product() + clinicStockByClinicIdAndAndProductId.getQuantity());
                    productBatch.setQuantity(dto.getNumber_product() + productBatch.getQuantity());
                } else{
                    clinicStockByClinicIdAndAndProductId.setQuantity(clinicStockByClinicIdAndAndProductId.getQuantity() - dto.getNumber_product());
                    if((productBatch.getQuantity() - dto.getNumber_product())<0){
                        return null;
                    }
                    productBatch.setQuantity(productBatch.getQuantity() - dto.getNumber_product());
                }
                for(OthersWarehouseReceiptDetails itemSearch : othersWarehouseReceiptDetails){
                    entity.setNumber_product(dto.getNumber_product() + itemSearch.getNumber_product());
                }
            }
        entity.setNumber_product(dto.getNumber_product());
        entity.setOthersWarehouseReceipt(othersWarehouseReceipt);
        entity.setClinicStock(clinicStockByClinicIdAndAndProductId);
        entity.setProductBatchId(productBatch);
        productBatchRepository.save(productBatch);
        clinicStockRepository.save(clinicStockByClinicIdAndAndProductId);

        Product products = productRepository.findById(productBatch.getProductId().getId()).get();
        List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

        for (ClinicStock entity2 : clinicStocks){
            if(products.getId() == entity2.getProduct().getId()){
                products.setQuantity(productStatisticsRepository.getSumProduct(productBatch.getProductId().getId()));
            }
            productRepository.save(products);
        }

        OthersWarehouseReceiptDetails result = othersWarehouseReceiptDetailsRepository.save(entity);
        return othersWarehouseReceiptDetailsConvert.entityToDto(result);

    }

    @Override
    public List<OtherReceiptDetailsQueryDto> getAllOtherReceiptDetailByReceiptid(Integer receipt_id){
        List<OtherReceiptDetailsQueryDto> otherReceiptDetailsQueryDtos = othersWarehouseReceiptDetailsRepository.getAllOtherReceiptByReceiptid(receipt_id);
        return otherReceiptDetailsQueryDtos;
    }

    @Override
    public boolean deleteOtherReceipt(Integer id){
        OthersWarehouseReceipt othersWarehouseReceipt = othersWarehouseReceiptRepository.findById(id).get();


        if(othersWarehouseReceipt.getStatus()!=ReceiptStatus.ACTIVE){
            return false;
        }
        List<OtherReceiptDetailsQueryDto> otherReceiptDetailsQueryDtos = othersWarehouseReceiptDetailsRepository.getAllOtherReceiptByReceiptid(id);
        if(otherReceiptDetailsQueryDtos.isEmpty()){
            othersWarehouseReceipt.setStatus(ReceiptStatus.DELETED);
            othersWarehouseReceiptRepository.save(othersWarehouseReceipt);
        } else{
            for(OtherReceiptDetailsQueryDto dto:otherReceiptDetailsQueryDtos){
                ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();
                if (othersWarehouseReceipt.getType()){
                    if (clinicStock.getQuantity()<0 ) return false;
                    clinicStock.setQuantity(clinicStock.getQuantity() - dto.getNumber_product());
                } else {
                    clinicStock.setQuantity(clinicStock.getQuantity() + dto.getNumber_product());
                }
                clinicStockRepository.save(clinicStock);

                Product products = productRepository.findById(clinicStock.getProduct().getId()).get();
                List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

                for (ClinicStock entity2 : clinicStocks){
                    if(products.getId() == entity2.getProduct().getId()){
                        products.setQuantity(productStatisticsRepository.getSumProduct(clinicStock.getProduct().getId()));
                    }
                    productRepository.save(products);
                }

                othersWarehouseReceipt.setStatus(ReceiptStatus.DELETED);
                othersWarehouseReceiptRepository.save(othersWarehouseReceipt);
            }
        }
        return true;
    }


    @Override
    public boolean deleteOtherReceiptDetail(Integer receipt_id, Integer clinic_stock_id){
        OthersWarehouseReceipt othersWarehouseReceipt = othersWarehouseReceiptRepository.findById(receipt_id).get();
        ClinicStock clinicStock = clinicStockRepository.findById(clinic_stock_id).get();

        if (othersWarehouseReceipt.getStatus() !=ReceiptStatus.ACTIVE){
            return false;
        }
        List<OthersWarehouseReceiptDetails> othersWarehouseReceiptDetails = othersWarehouseReceiptDetailsRepository.getOtherReceiptByClinicStockIdReceiptId(receipt_id, clinic_stock_id);
        if (othersWarehouseReceiptDetails.isEmpty()) return false;
        for (OthersWarehouseReceiptDetails entity:othersWarehouseReceiptDetails){
            ProductBatch productBatch = productBatchRepository.findById(entity.getProductBatchId().getId()).get();
            if (othersWarehouseReceipt.getType()){
                if (clinicStock.getQuantity()<0 || productBatch.getQuantity()<0) return false;
                clinicStock.setQuantity(clinicStock.getQuantity() - entity.getNumber_product());
                productBatch.setQuantity(productBatch.getQuantity() - entity.getNumber_product());
            } else{
                clinicStock.setQuantity(clinicStock.getQuantity() + entity.getNumber_product());
                productBatch.setQuantity(productBatch.getQuantity() + entity.getNumber_product());
            }
            productBatchRepository.save(productBatch);
            clinicStockRepository.save(clinicStock);

            Product products = productRepository.findById(productBatch.getProductId().getId()).get();
            List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

            for (ClinicStock entity2 : clinicStocks){
                if(products.getId() == entity2.getProduct().getId()){
                    products.setQuantity(productStatisticsRepository.getSumProduct(productBatch.getProductId().getId()));
                }
                productRepository.save(products);
            }

            othersWarehouseReceiptDetailsRepository.delete(entity);
        }
        return true;
    }

    @Override
    public OtherReceiptDetailsQueryDto updateOtherReceiptDetail(OtherReceiptDetailsQueryDto dto){
        OthersWarehouseReceipt othersWarehouseReceipt = othersWarehouseReceiptRepository.findById(dto.getReceipt_id()).get();
        List<OthersWarehouseReceiptDetails> othersWarehouseReceiptDetails = othersWarehouseReceiptDetailsRepository.getOtherReceiptByClinicStockIdReceiptId(dto.getReceipt_id(), dto.getClinic_stock_id());
        ProductBatch productBatch = productBatchRepository.findById(dto.getProduct_batch_id()).get();
        ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();

        if (othersWarehouseReceipt.getStatus()!=ReceiptStatus.ACTIVE){
            return null;
        }
        for(OthersWarehouseReceiptDetails entity : othersWarehouseReceiptDetails){
            int quantityChange = dto.getNumber_product() - entity.getNumber_product();

            if(othersWarehouseReceipt.getType()){
                if((productBatch.getQuantity() + quantityChange) < 0){
                    return null;
                }
                productBatch.setQuantity(productBatch.getQuantity() + quantityChange);
                clinicStock.setQuantity(clinicStock.getQuantity() + quantityChange);
                productBatchRepository.save(productBatch);
                clinicStockRepository.save(clinicStock);

                Product products = productRepository.findById(productBatch.getProductId().getId()).get();
                List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

                for (ClinicStock entity2 : clinicStocks){
                    if(products.getId() == entity2.getProduct().getId()){
                        products.setQuantity(productStatisticsRepository.getSumProduct(productBatch.getProductId().getId()));
                    }
                    productRepository.save(products);
                }
            }else{
                if((productBatch.getQuantity() - quantityChange) < 0){
                    return null;
                }
                productBatch.setQuantity(productBatch.getQuantity() - quantityChange);
                clinicStock.setQuantity(clinicStock.getQuantity() - quantityChange);
                productBatchRepository.save(productBatch);
                clinicStockRepository.save(clinicStock);

                Product products = productRepository.findById(productBatch.getProductId().getId()).get();
                List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

                for (ClinicStock entity2 : clinicStocks){
                    if(products.getId() == entity2.getProduct().getId()){
                        products.setQuantity(productStatisticsRepository.getSumProduct(productBatch.getProductId().getId()));
                    }
                    productRepository.save(products);
                }
            }
            entity.setNumber_product(dto.getNumber_product());
            othersWarehouseReceiptDetailsRepository.save(entity);
        }
        return dto;
    }


    @Override
    public boolean restoreOtherReceipt(Integer id){
        OthersWarehouseReceipt othersWarehouseReceipt = othersWarehouseReceiptRepository.findById(id).get();

        if(othersWarehouseReceipt.getStatus()!=ReceiptStatus.DELETED){
            return false;
        }
        List<OtherReceiptDetailsQueryDto> otherReceiptDetailsQueryDtos = othersWarehouseReceiptDetailsRepository.getAllOtherReceiptByReceiptid(id);
        if (!otherReceiptDetailsQueryDtos.isEmpty()) {
            for (OtherReceiptDetailsQueryDto dto : otherReceiptDetailsQueryDtos) {
                ProductBatch productBatch = productBatchRepository.findById(dto.getProduct_batch_id()).get();
                ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();
                if (othersWarehouseReceipt.getType()) {
                    clinicStock.setQuantity(clinicStock.getQuantity() + dto.getNumber_product());
                    productBatch.setQuantity(productBatch.getQuantity() + dto.getNumber_product());
                } else {
                    if (clinicStock.getQuantity() < 0 || productBatch.getQuantity() < 0 ) return false;
                    clinicStock.setQuantity(clinicStock.getQuantity() - dto.getNumber_product());
                    productBatch.setQuantity(productBatch.getQuantity() - dto.getNumber_product());
                }
                productBatchRepository.save(productBatch);
                clinicStockRepository.save(clinicStock);

                Product products = productRepository.findById(productBatch.getProductId().getId()).get();
                List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

                for (ClinicStock entity2 : clinicStocks){
                    if(products.getId() == entity2.getProduct().getId()){
                        products.setQuantity(productStatisticsRepository.getSumProduct(productBatch.getProductId().getId()));
                    }
                    productRepository.save(products);
                }
            }
        }
        othersWarehouseReceipt.setStatus(ReceiptStatus.ACTIVE);
        othersWarehouseReceiptRepository.save(othersWarehouseReceipt);
        return true;
    }


    @Override
    public boolean lockOtherReceipt(Integer id){
        OthersWarehouseReceipt othersWarehouseReceipt = othersWarehouseReceiptRepository.findById(id).get();
        if (othersWarehouseReceipt.getStatus() !=ReceiptStatus.ACTIVE){
            return false;
        }
        othersWarehouseReceipt.setStatus(ReceiptStatus.LOCKED);
        othersWarehouseReceiptRepository.save(othersWarehouseReceipt);
        return true;
    }


}
