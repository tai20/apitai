package com.sharework.health.service.impl;

import com.sharework.health.convert.TreatmentPackageCategoryConvert;
import com.sharework.health.dto.ServiceCategoryDto;
import com.sharework.health.entity.Status;
import com.sharework.health.entity.ServiceCategory;
import com.sharework.health.repository.ServiceCategoryRepository;
import com.sharework.health.service.TreatmentPackageCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class TreatmentPackageCategoryServiceImpl implements TreatmentPackageCategoryService {

    private ServiceCategoryRepository serviceCategoryRepository;

    private TreatmentPackageCategoryConvert treatmentPackageCategoryConvert;

    @Override
    public List<ServiceCategoryDto> findAll() {
        List<ServiceCategory> treatmentPackageCategories = serviceCategoryRepository.findAll();
        List<ServiceCategoryDto> serviceCategoryDtos = new ArrayList<>();
        for (ServiceCategory entity: treatmentPackageCategories
             ) {
            if (entity.getStatus() == Status.ACTIVE){
                ServiceCategoryDto dto = treatmentPackageCategoryConvert.entityToDto(entity);
                serviceCategoryDtos.add(dto);
            }
        }
        return serviceCategoryDtos;
    }

    @Override
    public ServiceCategoryDto findById(Integer id) {
        ServiceCategory entity = serviceCategoryRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return treatmentPackageCategoryConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(ServiceCategoryDto dto) {
        if (dto == null) {
            return false;
        }
        try {
            ServiceCategory entity = treatmentPackageCategoryConvert.dtoToEntity(dto);
            entity.setId(null);
            entity.setStatus(Status.ACTIVE);
            serviceCategoryRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Integer id, ServiceCategoryDto dto) {
        ServiceCategory entity = serviceCategoryRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        try {
            entity.setName(dto.getName());
            serviceCategoryRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        ServiceCategory entity = serviceCategoryRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        entity.setStatus(Status.DEACTIVE);
        return true;
    }

    @Override
    public ServiceCategoryDto searchTreatmentPackageCategoryByName(String name) {
        ServiceCategory entity = serviceCategoryRepository.findByName(name);
        if (entity == null) {
            return null;
        }
        return treatmentPackageCategoryConvert.entityToDto(entity);
    }
}
