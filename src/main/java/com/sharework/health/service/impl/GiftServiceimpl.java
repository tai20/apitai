package com.sharework.health.service.impl;

import com.sharework.health.convert.CouponConvert;
import com.sharework.health.dto.*;
import com.sharework.health.convert.GiftConvert;
import com.sharework.health.convert.GiftListConvert;
import com.sharework.health.dto.CouponDto;
import com.sharework.health.dto.CouponGiftDto;
import com.sharework.health.dto.GiftDto;

import com.sharework.health.entity.*;
import com.sharework.health.repository.CouponRepository;

import com.sharework.health.repository.GiftListRepository;
import com.sharework.health.repository.GiftRepository;
import com.sharework.health.repository.ProductRepository;
import com.sharework.health.service.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class GiftServiceimpl implements GiftService {

    private GiftRepository giftRepository;

    private GiftConvert giftConvert;

    private ProductService productService;

    private GiftListConvert giftListConvert;

    private GiftListRepository giftListRepository;

    private ProductRepository productRepository;

    private CouponService couponService;

    private CouponRepository couponRepository;

    private CouponConvert couponConvert;

    private UserService userService;

    private SlipUseService slipUseService;

    private ClinicStockService clinicStockService;

    private ExaminationCardService examinationCardService;

    private  ExaminationCardServiceImpl examinationCardService2;

    private CustomerService customerService;

    @Override
    public List<GiftDto> findAll() {
        List<Gift> gifts = giftRepository.findAll();
        List<GiftDto> giftDtos = new ArrayList<>();
        for (Gift entity : gifts
        ) {
            GiftDto dto = giftConvert.entityToDto(entity);
            giftDtos.add(dto);
        }
        return giftDtos;
    }

    @Override
    public GiftDto findById(Integer id) {
        return giftConvert.entityToDto(giftRepository.findByIdEquals(id));
    }

    @Override
    public boolean insert(GiftDto dto) {
        if (dto == null) {
            return false;
        }
        if(dto.getStartDate().isAfter(dto.getEndDate()) || ( dto.getQuantity() < 0 )){
            return false;
        }
        Gift entity = giftConvert.dtoToEntity(dto);
        entity.setId(null);
        entity.setName(dto.getName());
        entity.setStartDate(dto.getStartDate());
        entity.setEndDate(dto.getEndDate());
        entity.setQuantity(dto.getQuantity());
        giftRepository.save(entity);
        return true;
    }

    @Override
    public boolean update(Integer id, GiftDto dto) {
        Gift entity = giftRepository.findById(id).get();
        if (entity == null){
            return false;
        }
        if(dto.getStartDate().isAfter(dto.getEndDate()) || ( dto.getQuantity() < 0 )){
            return false;
        }
        entity.setName(dto.getName());
        entity.setStartDate(dto.getStartDate());
        entity.setEndDate(dto.getEndDate());
        entity.setQuantity(dto.getQuantity());
        giftRepository.save(entity);
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        Gift entity = giftRepository.findById(id).get();
        if (entity == null){
            return false;
        }
        giftRepository.delete(entity);
        return true;
    }


    @Override
    public List<GiftListDto> findGiftListByGID(Integer id) {
        List<GiftList> giftLists = giftListRepository.findGiftListByGID(id);

        List<GiftListDto> list = new ArrayList<>();

        for (GiftList entity : giftLists) {
            GiftListDto dto = giftListConvert.entityToDto(entity);
            list.add(dto);
        }
        return list;
    }

    @Override
    public CouponGiftDto getCouponGift() {
        LocalDate date = LocalDate.now();
        CouponGiftDto dto = new CouponGiftDto();
        List<CouponDto> couponDtos = couponService.findAll();
        List<CouponDto> newListCoupon = new ArrayList<>();
        List<GiftDto> giftDtos = findAll();
        List<GiftDto> newListGiftDtos = new ArrayList<>();
        for (CouponDto couponDto : couponDtos) {
            if (!couponDto.getEndDate().equals(date)) {
                newListCoupon.add(couponDto);
            }
        }
        for (GiftDto giftDto : giftDtos) {
            if (!giftDto.getEndDate().equals(date)) {
                newListGiftDtos.add(giftDto);
            }
        }

        dto.setCoupon(newListCoupon);
        dto.setGift(newListGiftDtos);
        return dto;
    }


    public List<GiftAndCouponDto> getAllCouponGift() {

        List<GiftAndCouponDto> list = new ArrayList<>();
        List<Gift> gift = giftRepository.findAll();
        List<Coupon> coupons= couponRepository.findAll();

        for(Coupon c : coupons){
            GiftAndCouponDto giftAndCoupon = new GiftAndCouponDto();
            CouponDto c1 = couponConvert.entityToDto(c);
            giftAndCoupon.setId(c1.getId());
            giftAndCoupon.setName(c1.getName());
            giftAndCoupon.setStartDate(c1.getStartDate());
            giftAndCoupon.setEndDate(c1.getEndDate());
            giftAndCoupon.setTimes(c1.getQuantity());
            giftAndCoupon.setCode(c1.getCode());
            giftAndCoupon.setService(c1.getServiceCategoryDto());
            giftAndCoupon.setType("Khuyến mãi");
            giftAndCoupon.setPercent(c1.getDiscount());
            list.add(giftAndCoupon);

        }

        for(Gift g : gift){
            GiftAndCouponDto giftAndCoupon = new GiftAndCouponDto();
            GiftDto g1 = giftConvert.entityToDto(g);
            giftAndCoupon.setId(g1.getId());
            giftAndCoupon.setName(g1.getName());
            giftAndCoupon.setStartDate(g1.getStartDate());
            giftAndCoupon.setEndDate(g1.getEndDate());
            giftAndCoupon.setTimes(g1.getQuantity());
            giftAndCoupon.setType("Quà tặng");
            list.add(giftAndCoupon);

        }
        return list;
    }

    @Override
    public GiftDetailDto getDetalGiftDtosById(Integer idGift) {
        GiftDetailDto giftDetailDto = new GiftDetailDto();
        giftDetailDto.setGiftDto(findById(idGift));
        List<GiftListDto> list = findGiftListByGID(idGift);
        List<GiftDetailProductDto> detailProductDtos = new ArrayList<>();
        for (GiftListDto entity : list
        ) {
            if (entity.getQuantity() != 0) {
                GiftDetailProductDto newDto = new GiftDetailProductDto(entity.getProductDto(), entity.getQuantity());
                detailProductDtos.add(newDto);
            }

        }
        giftDetailDto.setGiftDetailDto(detailProductDtos);
        return giftDetailDto;
    }

    public List<GiftListDto> getGiftListDtoBYID(Integer ID){
        List<GiftList> giftLists = giftListRepository.findByGift_IdEquals(ID);
        List<GiftListDto> dtos =new ArrayList<>();
        for (GiftList gift: giftLists) {
            dtos.add(giftListConvert.entityToDto(gift));
        }
        return dtos;
    }

    @Override
    public boolean addGiftList(GiftListDto dto) {
        Product product = productRepository.findById(dto.getProductDto().getId()).get();
        Gift gift = giftRepository.findById(dto.getGiftDto().getId()).get();
        if (product != null && gift != null) {
            GiftList entity = new GiftList()
                    .setProduct(product)
                    .setGift(gift)
                    .setQuantity(dto.getQuantity());

            giftListRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteGiftList(GiftListDto dto) {
        Gift gift = giftRepository.findById(dto.getGiftDto().getId()).get();
        Product product = productRepository.findById(dto.getProductDto().getId()).get();

        if(gift.getId() != dto.getGiftDto().getId()){
            return false;
        }

        if (gift != null || product != null){
            GiftList entity = new GiftList()
                    .setProduct(product)
                    .setGift(gift)
                    .setQuantity(dto.getQuantity());

            giftListRepository.delete(entity);
            return true;
        }

        return false;
    }

    @Override
    public boolean updateGiftList(Integer id, GiftListDto dto) {
        Gift gift = giftRepository.findById(id).get();
        Product product = productRepository.findById(dto.getProductDto().getId()).get();

        if (gift.getId() != dto.getGiftDto().getId()) {
            return false;
        }

        if (gift != null || product != null) {
            GiftList entity = new GiftList()
                    .setProduct(product)
                    .setGift(gift)
                    .setQuantity(dto.getQuantity());

            giftListRepository.save(entity);
            return true;
        }

        return false;
    }

    @Override
    public GiftDto findGiftAfterInsert() {
        Gift entity = giftRepository.findGiftAfterInsert();
        if (entity == null) {
            return null;
        }
        return giftConvert.entityToDto(entity);
    }

    @Override
    public boolean reloadTimesGift(Integer[] ids) {
        for (Integer id : ids) {
            Gift entity = giftRepository.findById(id).get();
            if (entity == null) {
                return false;
            }
            if (entity.getQuantity() <= 0) {
                return false;
            }
            entity.setQuantity(entity.getQuantity() - 1);
            giftRepository.save(entity);

        }
        return true;
    }

    @Override
    public List<GiftUserDto> getGiftUser(Principal principal, Integer id_customer) {
        String email = principal.getName();
        UserDto u = userService.findByEmail(email);
        LocalDate date = LocalDate.now();
        List<GiftListDto> glists= new ArrayList<>();
        List<GiftListDto> listGiftid = new ArrayList<>();
        List<GiftListDto> listDtos = new ArrayList<>();
        List<GiftDto> giftDtos = findAll();
        List<GiftUserDto> giftuser = new ArrayList<>();
        List<ExaminationCardDto> exam =  examinationCardService.findAll();
        List<SlipUse> uses = slipUseService.getslipuseByCum(id_customer);
        int te =0;
        int te1=0;
        for (ExaminationCardDto e: exam
             ) {
            int dateexam = date.compareTo(LocalDate.from(e.getDateOfExamination()));
            CustomerDto customerDto = customerService.getCustomerByIDSU(e.getSlipUseDto().getId());

            for (SlipUse slipUse: uses) {
                if (slipUse.getId() == e.getSlipUseDto().getId()){
                    te1 = 1;
                }
                if(te1==0){
                    if(id_customer == customerDto.getId() && dateexam != 0 ){
                        te=1;
                    }
                }
            }


        }
        if(te==1||te1==0){
            for (GiftDto dto : giftDtos) {
                int nowvsend = date.compareTo(dto.getEndDate());
                int nowvsstart = date.compareTo(dto.getStartDate());
                if ( nowvsstart >= 0 && nowvsend <= 0) {
                    Gift entity = giftRepository.findById(dto.getId()).get();
                    if(entity.getQuantity()>0){
                        listGiftid = findGiftListByGID(dto.getId());
                        glists.addAll(listGiftid);
                        entity.setQuantity(entity.getQuantity()-1);
                        giftRepository.save(entity);
                    }
                }
            }
            List<ClinicStockDto> listk= clinicStockService.getClinicStockByClinicId(u.getClinicDto().getId());
            for (GiftListDto dtoa: glists
            ) {
                int i=0;
                for (ClinicStockDto dtok: listk
                ) {
                    if(dtok.getClinicDto().getId()== u.getClinicDto().getId() && dtok.getQuantity() >=dtoa.getQuantity()&&dtok.getProductDto().getId()==dtoa.getProductDto().getId()){
                        i=1;
                        ProductDto pdto = productService.findById(dtok.getProductDto().getId());
                        pdto.setQuantity(pdto.getQuantity()-dtoa.getQuantity());
                        productService.update(dtoa.getProductDto().getId(),pdto);
                        dtok.setQuantity(dtok.getQuantity() - dtoa.getQuantity());
                        clinicStockService.update(dtok.getId(), dtok);
                    }
                }
                if(i==1){
                    listDtos.add(dtoa);
                }
            }
            for (GiftListDto dto: listDtos
            ) {
                GiftUserDto gu = new GiftUserDto();
                gu.setId(dto.getProductDto().getId());
                gu.setName(dto.getProductDto().getName());
                gu.setQuantity(dto.getQuantity());
                giftuser.add(gu);
            }

        }

        return giftuser;
    }


    @Override
    public List<GiftAndCouponDto> findByTimes(Integer times) {
        List<Gift> gift = giftRepository.findByTimes(times);
        List<Coupon> coupons= couponRepository.findByTimes(times);

        List<GiftAndCouponDto> list = new ArrayList<>();


        for (Gift g : gift){
            GiftAndCouponDto giftAndCoupon = new GiftAndCouponDto();
            GiftDto g1 = giftConvert.entityToDto(g);
            giftAndCoupon.setId(g1.getId());
            giftAndCoupon.setName(g1.getName());
            giftAndCoupon.setStartDate(g1.getStartDate());
            giftAndCoupon.setEndDate(g1.getEndDate());
            giftAndCoupon.setTimes(g1.getQuantity());
            giftAndCoupon.setType("Quà tặng");
            list.add(giftAndCoupon);

        }

        for(Coupon c : coupons){
            GiftAndCouponDto giftAndCoupon = new GiftAndCouponDto();
            CouponDto c1 = couponConvert.entityToDto(c);
            giftAndCoupon.setId(c1.getId());
            giftAndCoupon.setName(c1.getName());
            giftAndCoupon.setStartDate(c1.getStartDate());
            giftAndCoupon.setEndDate(c1.getEndDate());
            giftAndCoupon.setTimes(c1.getQuantity());
            giftAndCoupon.setCode(c1.getCode());
            giftAndCoupon.setService(c1.getServiceCategoryDto());
            giftAndCoupon.setType("Khuyến mãi");
            giftAndCoupon.setPercent(c1.getDiscount());
            list.add(giftAndCoupon);
        }
        return list;
    }

}
