package com.sharework.health.service.impl;

import com.sharework.health.Constants;
import com.sharework.health.convert.SlipUseConvert;
import com.sharework.health.convert.SlipUseDetailConvert;
import com.sharework.health.dto.SlipUseDetailDto;
import com.sharework.health.dto.SlipUseDto;
import com.sharework.health.dto.SlipUseQuery;
import com.sharework.health.dto.TreatmentPackageDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.response.SlipUseReponse;
import com.sharework.health.response.TreatmentPackageReponse;
import com.sharework.health.service.SlipUseService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class SlipUseServiceImpl implements SlipUseService {

    private static final Path CURRENT_FOLDER = Paths.get(System.getProperty("user.dir"));

    private SlipUseRepository slipUseRepository;

    private SlipUseConvert slipUseConvert;

    private TreatmentPackageRepository treatmentPackageRepository;

    private CustomerRepository customerRepository;

    private SlipUseDetailRepository slipUseDetailRepository;

    private SlipUseDetailConvert slipUseDetailConvert;

    private ServiceRepository serviceRepository;

    @Override
    public List<SlipUseDto> findAll() {
        List<SlipUse> slipUses = slipUseRepository.findAll();
        List<SlipUseDto> slipUseDtos = new ArrayList<>();

        for (SlipUse entity: slipUses
        ) {
            if (entity.getEndDate().compareTo(LocalDate.now()) < 0){
                entity.setStatus(Status.DEACTIVE);
                slipUseRepository.save(entity);
            }
            SlipUseDto dto = slipUseConvert.entityToDto(entity);
            List<SlipUseDetailDto> slipUseDetailsDtos = this.getAllSlipUseDetailBySlipUse(dto.getId());
            String nameSlipUse = "";
            if(slipUseDetailsDtos.size()==0){
                dto.setNameSlipUse(nameSlipUse);
                slipUseDtos.add(dto);
            }
            else{
                for (SlipUseDetailDto test: slipUseDetailsDtos){
                    String nameEx = String.valueOf(test.getNumberOfUse()+test.getServiceDto().getName().toString()+"_");
                    nameSlipUse += nameEx;

                }
                dto.setNameSlipUse(nameSlipUse + dto.getStartDate());
                slipUseDtos.add(dto);
            }
        }
        return slipUseDtos;
    }

    @Override
    public SlipUseDto findById(Integer id) {
        SlipUse entity = slipUseRepository.findById(id).get();
        if (entity == null){
            return null;
        }
        return slipUseConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(SlipUseDto dto) {
        if (dto == null){
            return false;
        }

        try {
            TreatmentPackage treatmentPackage = treatmentPackageRepository.findById(dto.getTreatmentPackageDto().getId()).get();
            Customer customer = customerRepository.findById(dto.getCustomerDto().getId()).get();
            if (treatmentPackage == null){
                return false;
            }
            if (customer == null){
                return false;
            }
            SlipUse entity = slipUseConvert.dtoToEntity(dto);
            entity.setTreatmentPackage(treatmentPackage);
            entity.setCustomer(customer);
            entity.setStatus(Status.ACTIVE);
            slipUseRepository.save(entity);
            return true;

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Integer id, SlipUseDto dto) {
        if (dto == null){
            return false;
        }
        SlipUse entity = slipUseRepository.findById(id).get();
        try {
            //TreatmentPackage treatmentPackage = treatmentPackageRepository.findById(dto.getTreatmentPackageDto().getId()).get();
            //Customer customer = customerRepository.findById(dto.getCustomerDto().getId()).get();
//            if (treatmentPackage == null){
//                return false;
//            }
//            if (customer == null){
//                return false;
//            }
            //entity.setStartDate(dto.getStartDate());
            if (dto.getEndDate() != null){
                entity.setEndDate(dto.getEndDate());
            }

            //entity.setTreatmentPackage(treatmentPackage);
            //entity.setCustomer(customer);
            slipUseRepository.save(entity);
            return true;

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        SlipUse entity = slipUseRepository.findById(id).get();
        if (entity == null){
            return false;
        }
        try {
            entity.setStatus(Status.DEACTIVE);
            slipUseRepository.save(entity);
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<SlipUseDto> findAllByCustomer(Integer customerId) {
        Customer customer = customerRepository.findById(customerId).get();
        if (customer == null){
            return null;
        }
        List<SlipUse> slipUses = slipUseRepository.findAllByCustomer(customer);
        List<SlipUseDto> slipUseDtos = new ArrayList<>();

        for (SlipUse entity: slipUses
        ) {
            if (entity.getStatus() == Status.ACTIVE){
                SlipUseDto dto = slipUseConvert.entityToDto(entity);
                List<SlipUseDetailDto> slipUseDetailsDtos = this.getAllSlipUseDetailBySlipUse(dto.getId());
                String nameSlipUse = "";
                for (SlipUseDetailDto test: slipUseDetailsDtos){
                    String nameEx = String.valueOf(test.getNumberOfUse()+test.getServiceDto().getName().toString()+"_");
                    nameSlipUse += nameEx;

                }
                dto.setNameSlipUse(nameSlipUse + dto.getStartDate());
                slipUseDtos.add(dto);
            }
        }

        return slipUseDtos;
    }


    @Override
    public List<SlipUseDto> findAllByTreatmentPackage(String treatmentPackageName) {
        TreatmentPackage treatmentPackage = treatmentPackageRepository.findByName(treatmentPackageName);
        if (treatmentPackage == null){
            return null;
        }
        List<SlipUse> slipUses = slipUseRepository.findAllByTreatmentPackage(treatmentPackage);
        List<SlipUseDto> slipUseDtos = new ArrayList<>();

        for (SlipUse entity: slipUses
        ) {
            SlipUseDto dto = slipUseConvert.entityToDto(entity);
            slipUseDtos.add(dto);
        }
        return slipUseDtos;
    }

    @Override
    public List<SlipUseDetailDto> getAllSlipUseDetailBySlipUse(Integer slipUseId) {
        List<SlipUseDetail> slipUseDetails = slipUseDetailRepository.findAllBySlipUse(slipUseId);
        List<SlipUseDetailDto> slipUseDetailDtos = new ArrayList<>();
        for (SlipUseDetail entity: slipUseDetails
             ) {
            SlipUseDetailDto dto = slipUseDetailConvert.entityToDto(entity);
            slipUseDetailDtos.add(dto);
        }
        return slipUseDetailDtos;
    }

    @Override
    public List<SlipUseDetailDto> getAllSlipUserDetailByCustomerId(Integer customerId) {
        List<SlipUseDetail> slipUseDetails = slipUseDetailRepository.findAllByCustomerId(customerId);
        List<SlipUseDetailDto> slipUseDetailDtos = new ArrayList<>();
        for(SlipUseDetail entity: slipUseDetails){
            SlipUseDetailDto dto = slipUseDetailConvert.entityToDto(entity);
            slipUseDetailDtos.add(dto);
        }
        return slipUseDetailDtos;
    }

    @Override
    public boolean uploadImageBeforeSlipUse(Integer slipUseId, MultipartFile imageBefore) throws IOException {
        SlipUse entity = slipUseRepository.findById(slipUseId).get();
        if (entity == null){
            return false;
        }

        Path staticPath = Paths.get("static");
        Path imagePath = Paths.get("images");

        if (!Files.exists(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath))){
            Files.createDirectories(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath));
        }
        if (imageBefore != null && entity.getImageBefore() == null){
            //Path file = CURRENT_FOLDER.resolve(staticPath).resolve(imagePath).resolve(imageBefore.getOriginalFilename());
            Path path = Paths.get("static/images/");
            try {
                InputStream inputStream = imageBefore.getInputStream();
                Files.copy(inputStream, path.resolve(imageBefore.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
                entity.setImageBefore(imageBefore.getOriginalFilename().toLowerCase());
                slipUseRepository.save(entity);
                return true;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean uploadImageAfterSlipUse(Integer slipUseId, MultipartFile imageAfter) throws IOException {
        SlipUse entity = slipUseRepository.findById(slipUseId).get();
        if (entity == null){
            return false;
        }

        Path staticPath = Paths.get("static");
        Path imagePath = Paths.get("images");

        if (!Files.exists(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath))){
            Files.createDirectories(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath));
        }
        if (imageAfter != null && entity.getImageBefore() != null){
            //Path file = CURRENT_FOLDER.resolve(staticPath).resolve(imagePath).resolve(imageBefore.getOriginalFilename());
            Path path = Paths.get("static/images/");
            try {
                InputStream inputStream = imageAfter.getInputStream();
                Files.copy(inputStream, path.resolve(imageAfter.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
                entity.setImageAfter(imageAfter.getOriginalFilename().toLowerCase());
                slipUseRepository.save(entity);
                return true;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean activeSlipUse(Integer slipUseId) {
        SlipUse entity = slipUseRepository.findById(slipUseId).get();
        if (entity != null && entity.getStatus() == Status.DEACTIVE){
            entity.setStatus(Status.ACTIVE);
            slipUseRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public List<SlipUseDto> findAllnew() {
        List<SlipUse> slipUses = slipUseRepository.findAll();
        List<SlipUseDto> slipUseDtos = new ArrayList<>();
        for (SlipUse ent: slipUses) {
            SlipUseDto a = slipUseConvert.entityToDto(ent);
            if(a!=null)
            {
                List<SlipUseDetail> b= slipUseDetailRepository.findByIDSlipUse(a.getId());
                String x ="";
                for (SlipUseDetail d: b) {
                    x= d.getNumberOfStock()+""+d.getService().getName()+"_"+x;
                }
                a.setNameSlipUse(x+""+a.getStartDate());
            }
            if(a.getNameSlipUse()==null){
                a.setNameSlipUse("Phiếu chưa có dịch vụ");
            }
            slipUseDtos.add(a);
        }
        return slipUseDtos;
    }

    @Override
    public List<SlipUse> getslipuseByCum(Integer id) {
        return slipUseRepository.findByCustomer_Id(id);
    }

    @Override
    public SlipUseReponse getAllSlipUse(int pageNo, int pageSize, String sortBy, String sortDir) {

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize );

        Page<SlipUse> slip = slipUseRepository.findAll(pageable);

        List<SlipUse> listOfPosts = slip.getContent();
        List<SlipUseDto> slipUseDtos = new ArrayList<>();

        for (SlipUse entity: listOfPosts
        ) {
            if (entity.getEndDate().compareTo(LocalDate.now()) < 0){
                entity.setStatus(Status.DEACTIVE);
                slipUseRepository.save(entity);
            }
        }

        slipUseDtos= listOfPosts.stream().map(post -> slipUseConvert.entityToDto(post)).collect(Collectors.toList());

        SlipUseReponse postResponse = new SlipUseReponse();
        postResponse.setData(slipUseDtos);
        postResponse.setCurrent(slip.getNumber());
        postResponse.setPageSize(slip.getSize());
        postResponse.setTotalElements(slip.getTotalElements());
        postResponse.setTotalPages(slip.getTotalPages());
        postResponse.setLast(slip.isLast());

        return postResponse;
    }


    @Override
    public void mergeNameSlipUse() {
        Sort sort = Constants.DEFAULT_SORT_DIRECTION.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by("slipUse").ascending()
                : Sort.by("slipUse").descending();
        List<SlipUseDetail> entity = slipUseDetailRepository.findAll(sort);

        com.sharework.health.entity.Service service = serviceRepository.findById(entity.get(entity.size() - 1).getService().getId()).get();
        SlipUse slipuse = slipUseRepository.findById(entity.get(entity.size() - 1).getSlipUse().getId()).get();

        StringBuffer name = new StringBuffer();
        name.append(entity.get(entity.size() - 1).getNumberOfStock()+"_");
        name.append(service.getName()+"_");
        name.append(slipuse.getStartDate());
        slipuse.setName(String.valueOf(name));
//        slipuse.getId();
        slipUseRepository.save(slipuse);
    }

    public static LocalDate
    getDateFromString(String string,
                      DateTimeFormatter format)
    {
        // Converting the string to date
        // in the specified format
        LocalDate date = LocalDate.parse(string, format);

        // Returning the converted date
        return date;
    }

    @Override
    public SlipUseReponse findOfSlipUse(int pageNo, int pageSize, String sortBy, String sortDir, String input) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize,sort);

        Page<SlipUse> slipUses = null;

        if(input.equalsIgnoreCase(Constants.DEACTIVE) || input.equalsIgnoreCase(Constants.ACTIVE)) {
            slipUses = slipUseRepository.findAllByStatus(pageable, input);
        }else if(input.contains("_")){
            slipUses = slipUseRepository.findAllByNameSlipUse(pageable, input);
        }else if(input.contains("-")){
            DateTimeFormatter format
                    = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate date
                    = getDateFromString(input, format);
            slipUses = slipUseRepository.findAllByDate(pageable, date);
        }else{
            slipUses = slipUseRepository.findAllByNameCustomer(pageable, input);
        }


        List<SlipUse> listOfSlipUse = slipUses.getContent();

        List<SlipUseDto> slipUseDtos = listOfSlipUse.stream().map(post -> slipUseConvert.entityToDto(post)).collect(Collectors.toList());

        SlipUseReponse slipuseResponse = new SlipUseReponse();
        slipuseResponse.setData(slipUseDtos);
        slipuseResponse.setCurrent(slipUses.getNumber());
        slipuseResponse.setPageSize(slipUses.getSize());
        slipuseResponse.setTotalElements(slipUses.getTotalElements());
        slipuseResponse.setTotalPages(slipUses.getTotalPages());
        slipuseResponse.setLast(slipUses.isLast());

        return slipuseResponse;
    }
}
