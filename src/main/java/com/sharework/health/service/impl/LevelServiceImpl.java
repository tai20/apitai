package com.sharework.health.service.impl;

import com.sharework.health.convert.LevelConvert;
import com.sharework.health.dto.LevelDto;
import com.sharework.health.entity.Level;
import com.sharework.health.repository.LevelRepository;
import com.sharework.health.service.LevelService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class LevelServiceImpl implements LevelService {

    private LevelRepository levelRepository;

    private LevelConvert levelConvert;


    @Override
    public List<LevelDto> findAll() {
        List<Level> level = levelRepository.findAll();
        List<LevelDto> levelDto = new ArrayList<>();
        for (Level entity : level) {
            LevelDto dto = levelConvert.entityToDto(entity);
            levelDto.add(dto);
        }
        return levelDto;
    }

    @Override
    public LevelDto findById(Integer id) {
        Level entity = levelRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return levelConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(LevelDto dto) {
        if (dto == null) {
            return false;
        }
        Level entity = levelConvert.dtoToEntity(dto);
        entity.setId(null);
        entity.setName(dto.getName());
        entity.setMin_value(dto.getMin_value());
        entity.setMax_value(dto.getMax_value());
        levelRepository.save(entity);

        return true;

    }

    @Override
    public boolean update(Integer id, LevelDto dto) {
        Level entity = levelRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        if (dto == null) {
            return false;
        }
        entity.setName(dto.getName());
        entity.setMin_value(dto.getMin_value());
        entity.setMax_value(dto.getMax_value());
        levelRepository.save(entity);
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public Level findNameCustomerLever(BigDecimal i) {
        List<Level> list = levelRepository.findAll();
        if (i != null ) {

            for (Level level : list) {

                if (( i.compareTo(level.getMin_value()) == 0 || i.compareTo(level.getMin_value()) == 1  ) &&
                        (i.compareTo(level.getMax_value()) == 0 || i.compareTo(level.getMax_value()) == -1))
                    return level;
                if( ( i.compareTo(level.getMin_value()) == 0 || i.compareTo(level.getMin_value()) == 1 ) && ( new BigDecimal(0).compareTo(level.getMax_value()) == 0) ){
                    return level;
                }
            }
        }
        System.err.println(" MAX: " + list.get(0).getMax_value() + " MIN: " + list.get(0).getMin_value());
        return list.get(0);
//        return levelRepository.findByMin_valueIsGreaterThanEqualAndMax_valueIsLessThanEqual(i,i);
    }

}
