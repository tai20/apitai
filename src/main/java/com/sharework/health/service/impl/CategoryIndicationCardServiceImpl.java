package com.sharework.health.service.impl;

import com.sharework.health.convert.CategoryIndicationCardConvert;
import com.sharework.health.dto.CategoryIndicationCardDto;
import com.sharework.health.entity.PackageTest;
import com.sharework.health.entity.SubCategoryIndicationCard;
import com.sharework.health.entity.CategoryIndicationCard;
import com.sharework.health.service.CategoryIndicationCardService;
import com.sharework.health.repository.SubCategoryIndicationCardRepository;
import com.sharework.health.repository.CategoryIndicationCardRepository;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class CategoryIndicationCardServiceImpl implements CategoryIndicationCardService {

    private CategoryIndicationCardRepository categoryIndicationCardRepository;

    private CategoryIndicationCardConvert categoryIndicationCardConvert;

    private SubCategoryIndicationCardRepository subCategoryIndicationCardRepository;

    @Override
    public List<CategoryIndicationCardDto> findAll() {
        List<CategoryIndicationCard> categoryIndicationCards = categoryIndicationCardRepository.findAll();
        List<CategoryIndicationCardDto> categoryIndicationCardDtos = new ArrayList<>();
        for (CategoryIndicationCard entity : categoryIndicationCards
        ) {
            CategoryIndicationCardDto dto = categoryIndicationCardConvert.entityToDto(entity);
            categoryIndicationCardDtos.add(dto);
        }
        return categoryIndicationCardDtos;
    }

    @Override
    public CategoryIndicationCardDto findById(Integer id) {
        CategoryIndicationCard entity = categoryIndicationCardRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return categoryIndicationCardConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(CategoryIndicationCardDto dto) {
        if (dto == null) {
            return false;
        }
        SubCategoryIndicationCard subCategoryIndicationCard = subCategoryIndicationCardRepository.findById(dto.getSubCategoryIndicationCardDto().getId()).get();

        if (subCategoryIndicationCard == null) {
            return false;
        }
        CategoryIndicationCard categoryIndicationCard = categoryIndicationCardConvert.dtoToEntity(dto);

        categoryIndicationCard.setId(null);
        categoryIndicationCard.setSubCategoryIndicationCard(subCategoryIndicationCard);
        categoryIndicationCardRepository.save(categoryIndicationCard);
        return true;
    }

    @Override
    public boolean update(Integer id, CategoryIndicationCardDto dto) {
        CategoryIndicationCard entity = categoryIndicationCardRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        SubCategoryIndicationCard subCategoryIndicationCard = subCategoryIndicationCardRepository.findById(dto.getSubCategoryIndicationCardDto().getId()).get();
        if (subCategoryIndicationCard == null) {
            return false;
        }
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        entity.setPrice(dto.getPrice());
        entity.setSubCategoryIndicationCard(subCategoryIndicationCard);
        categoryIndicationCardRepository.save(entity);
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        CategoryIndicationCard entity = categoryIndicationCardRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        categoryIndicationCardRepository.deleteById(id);
        return true;
    }

    @Override
    public List<CategoryIndicationCardDto> findAllBySubCategoryIndicationCardName(String subCategoryIndicationCardName) {
        List<CategoryIndicationCard> categoryIndicationCards = categoryIndicationCardRepository.findAllBySubCategoryIndicationCardName(subCategoryIndicationCardName);
        List<CategoryIndicationCardDto> categoryIndicationCardDtos = new ArrayList<>();
        for (CategoryIndicationCard entity : categoryIndicationCards
        ) {
            CategoryIndicationCardDto dto = categoryIndicationCardConvert.entityToDto(entity);
            categoryIndicationCardDtos.add(dto);
        }
        return categoryIndicationCardDtos;
    }
}
