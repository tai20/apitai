package com.sharework.health.service.impl;


import com.sharework.health.convert.ProductConvert;
import com.sharework.health.dto.*;
import com.sharework.health.entity.Clinic;
import com.sharework.health.entity.ImportReceiptDetails;
import com.sharework.health.entity.Product;
import com.sharework.health.entity.ReceiptStatus;
import com.sharework.health.repository.ClinicRepository;
import com.sharework.health.repository.ClinicStockRepository;
import com.sharework.health.repository.ProductRepository;
import com.sharework.health.repository.ProductStatisticsRepository;
import com.sharework.health.service.ProductStatisticsService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;

@Service
@Transactional(rollbackOn = Exception.class)
@AllArgsConstructor
public class ProductStatisticsServiceImpl implements ProductStatisticsService {


    private ProductStatisticsRepository productStatisticsRepository;
    private ProductRepository productRepository;
    private ProductConvert productConvert;
    private ClinicRepository clinicRepository;
    private ClinicStockRepository clinicStockRepository;


    @Override
    public List<RemainingProductStatisticsQueryDto> remainingProductStatistics() {

        List<Product> products = productRepository.findAll();
        for(Product entity:products){
            entity.setQuantity(productStatisticsRepository.getSumProduct(entity.getId()));
            productRepository.save(entity);
        }

        List<Product> productsSum = productRepository.findAll();
        List<RemainingProductStatisticsQueryDto> dtos = productStatisticsRepository.getClinicStockProduct();
        for(Product entity:productsSum){
            RemainingProductStatisticsQueryDto dtoSum = new RemainingProductStatisticsQueryDto();
            dtoSum.setProduct_id(entity.getId());
            dtoSum.setProduct_name(entity.getName());
            dtoSum.setProduct_category(entity.getProductCategory().getName());
            dtoSum.setPrice(entity.getPrice());
            dtoSum.setWho_sale_price(entity.getWholesalePrice());
            dtoSum.setUnit(entity.getUnit());
            dtoSum.setWarehouse_name(null);
            dtoSum.setQuantity(entity.getQuantity());
            dtos.add(dtoSum);
        }
    return dtos;

    }

    //Hàm thống kê tất cả sản phẩm nhập kho theo khoảng thời gian
    public List<StatisticsImportedProductsQueryDto> getImported(LocalDateTime from, LocalDateTime to){
        List<Clinic> clinics = clinicRepository.findAll();
        List<StatisticsImportedProductsQueryDto> allImportReceiptDetails = productStatisticsRepository.getAllImportReceiptDetails(from,to);
        List<Product> products = productRepository.findAll();
        List<StatisticsImportedProductsQueryDto> dtoResult = new ArrayList<>();

        for(Clinic dtoClinic:clinics){
            for(StatisticsImportedProductsQueryDto itemSearch1:allImportReceiptDetails){
                Integer quantitySum=0;
                StatisticsImportedProductsQueryDto dtoInsert = new StatisticsImportedProductsQueryDto();
                if(itemSearch1.getWarehouse_name()!=null && itemSearch1.getWarehouse_id()==dtoClinic.getId()){
                    for(StatisticsImportedProductsQueryDto itemSearch2:allImportReceiptDetails){
                        if(itemSearch2.getWarehouse_name()!=null && itemSearch1.getProduct_id()== itemSearch2.getProduct_id() && itemSearch2.getWarehouse_id()==dtoClinic.getId()){
                            quantitySum+=itemSearch2.getQuantity_import();
                            itemSearch2.setWarehouse_name(null);
                        }

                    }
                }
                if (quantitySum>0){
                    dtoInsert.setProduct_id(itemSearch1.getProduct_id());
                    dtoInsert.setProduct_name(itemSearch1.getProduct_name());
                    dtoInsert.setProduct_category(itemSearch1.getProduct_category());
                    dtoInsert.setImport_price(itemSearch1.getImport_price());
                    dtoInsert.setUnit(itemSearch1.getUnit());
                    dtoInsert.setQuantity_import(quantitySum);
                    dtoInsert.setWarehouse_id(dtoClinic.getId());
                    dtoInsert.setWarehouse_name(dtoClinic.getName());
                    dtoResult.add(dtoInsert);
                }
            }
        }

        for(Product dtoProduct:products){
            StatisticsImportedProductsQueryDto dtoInsert = new StatisticsImportedProductsQueryDto();
            Integer quantitySum=0;
            for(StatisticsImportedProductsQueryDto itemSearch:allImportReceiptDetails){
                if (itemSearch.getProduct_id()==dtoProduct.getId() && itemSearch.getWarehouse_id()!=null){
                    quantitySum+=itemSearch.getQuantity_import();
                    itemSearch.setWarehouse_id(null);
                }
            }
            if (quantitySum>0){
                dtoInsert.setProduct_id(dtoProduct.getId());
                dtoInsert.setProduct_name(dtoProduct.getName());
                dtoInsert.setProduct_category(dtoProduct.getProductCategory().getName());
                dtoInsert.setImport_price(dtoProduct.getWholesalePrice());
                dtoInsert.setUnit(dtoProduct.getUnit());
                dtoInsert.setQuantity_import(quantitySum);
                dtoInsert.setWarehouse_id(null);
                dtoInsert.setWarehouse_name(null);
                dtoResult.add(dtoInsert);
            }
        }
        return dtoResult;
    }


    //Hàm thống kê tất cả sản phẩm xuất kho theo khoảng thời gian
    public List<StatisticsExportedProductsQueryDto> getExported(LocalDateTime from, LocalDateTime to){
        List<Clinic> clinics = clinicRepository.findAll();
        List<StatisticsExportedProductsQueryDto> allExportReceiptDetails = productStatisticsRepository.getAllExportReceiptDetails(from,to);
        List<Product> products = productRepository.findAll();
        List<StatisticsExportedProductsQueryDto> dtoResult = new ArrayList<>();

        for(Clinic dtoClinic:clinics){
            for(StatisticsExportedProductsQueryDto itemSearch1:allExportReceiptDetails){
                Integer quantitySum=0;
                StatisticsExportedProductsQueryDto dtoInsert = new StatisticsExportedProductsQueryDto();
                if(itemSearch1.getWarehouse_name()!=null && itemSearch1.getWarehouse_id()==dtoClinic.getId()){
                    for(StatisticsExportedProductsQueryDto itemSearch2:allExportReceiptDetails){
                        if(itemSearch2.getWarehouse_name()!=null && itemSearch1.getProduct_id()== itemSearch2.getProduct_id() && itemSearch2.getWarehouse_id()==dtoClinic.getId()){
                            quantitySum+=itemSearch2.getQuantity_export();
                            itemSearch2.setWarehouse_name(null);
                        }

                    }
                }
                if (quantitySum>0){
                    dtoInsert.setProduct_id(itemSearch1.getProduct_id());
                    dtoInsert.setProduct_name(itemSearch1.getProduct_name());
                    dtoInsert.setProduct_category(itemSearch1.getProduct_category());
                    dtoInsert.setExport_price(itemSearch1.getExport_price());
                    dtoInsert.setUnit(itemSearch1.getUnit());
                    dtoInsert.setQuantity_export(quantitySum);
                    dtoInsert.setWarehouse_id(dtoClinic.getId());
                    dtoInsert.setWarehouse_name(dtoClinic.getName());
                    dtoResult.add(dtoInsert);
                }
            }
        }

        for(Product dtoProduct:products){
            StatisticsExportedProductsQueryDto dtoInsert = new StatisticsExportedProductsQueryDto();
            Integer quantitySum=0;
            for(StatisticsExportedProductsQueryDto itemSearch:allExportReceiptDetails){
                if (itemSearch.getProduct_id()==dtoProduct.getId() && itemSearch.getWarehouse_id()!=null){
                    quantitySum+=itemSearch.getQuantity_export();
                    itemSearch.setWarehouse_id(null);
                }
            }
            if (quantitySum>0){
                dtoInsert.setProduct_id(dtoProduct.getId());
                dtoInsert.setProduct_name(dtoProduct.getName());
                dtoInsert.setProduct_category(dtoProduct.getProductCategory().getName());
                dtoInsert.setExport_price(dtoProduct.getWholesalePrice());
                dtoInsert.setUnit(dtoProduct.getUnit());
                dtoInsert.setQuantity_export(quantitySum);
                dtoInsert.setWarehouse_id(null);
                dtoInsert.setWarehouse_name(null);
                dtoResult.add(dtoInsert);
            }
        }
        return dtoResult;
    }


    //Hàm thống kê tất cả sản phẩm nhập xuất kho khác theo khoảng thời gian
    public List<StatisticsOtherProductsQueryDto> getOther(LocalDateTime from, LocalDateTime to){
        List<Clinic> clinics = clinicRepository.findAll();
        List<StatisticsOtherProductsQueryDto> allOtherReceiptDetails = productStatisticsRepository.getAllOtherReceiptDetails(from,to);
        List<Product> products = productRepository.findAll();
        List<StatisticsOtherProductsQueryDto> dtoResult = new ArrayList<>();

        for(Clinic dtoClinic:clinics){
            for(StatisticsOtherProductsQueryDto itemSearch1:allOtherReceiptDetails){
                Integer quantitySum=0;
                StatisticsOtherProductsQueryDto dtoInsert = new StatisticsOtherProductsQueryDto();
                if(itemSearch1.getWarehouse_name()!=null && itemSearch1.getWarehouse_id()==dtoClinic.getId()){
                    for(StatisticsOtherProductsQueryDto itemSearch2:allOtherReceiptDetails){
                        if(itemSearch2.getWarehouse_name()!=null && itemSearch1.getProduct_id()== itemSearch2.getProduct_id() && itemSearch2.getWarehouse_id()==dtoClinic.getId()  && itemSearch1.isType()== itemSearch2.isType()){
                            quantitySum+=itemSearch2.getQuantity();
                            itemSearch2.setWarehouse_name(null);
                        }

                    }
                }
                if (quantitySum>0){
                    dtoInsert.setProduct_id(itemSearch1.getProduct_id());
                    dtoInsert.setProduct_name(itemSearch1.getProduct_name());
                    dtoInsert.setProduct_category(itemSearch1.getProduct_category());
                    dtoInsert.setUnit(itemSearch1.getUnit());
                    dtoInsert.setQuantity(quantitySum);
                    dtoInsert.setWarehouse_id(dtoClinic.getId());
                    dtoInsert.setWarehouse_name(dtoClinic.getName());
                    dtoInsert.setType(itemSearch1.isType());
                    dtoResult.add(dtoInsert);
                }
            }
        }

        for(Product dtoProduct:products){
            StatisticsOtherProductsQueryDto dtoInsert = new StatisticsOtherProductsQueryDto();
            Integer quantitySumImport=0;
            Integer quantitySumExport=0;
            for(StatisticsOtherProductsQueryDto itemSearch:allOtherReceiptDetails){
                if (itemSearch.getProduct_id()==dtoProduct.getId() && itemSearch.getWarehouse_id()!=null  && itemSearch.isType()==true){
                    quantitySumImport+=itemSearch.getQuantity();
                    itemSearch.setWarehouse_id(null);
                }
                else if (itemSearch.getProduct_id()==dtoProduct.getId() && itemSearch.getWarehouse_id()!=null  && itemSearch.isType()==false){
                    quantitySumExport+=itemSearch.getQuantity();
                    itemSearch.setWarehouse_id(null);
                }
            }
            if (quantitySumImport>0){
                dtoInsert.setProduct_id(dtoProduct.getId());
                dtoInsert.setProduct_name(dtoProduct.getName());
                dtoInsert.setProduct_category(dtoProduct.getProductCategory().getName());
                dtoInsert.setUnit(dtoProduct.getUnit());
                dtoInsert.setQuantity(quantitySumImport);
                dtoInsert.setWarehouse_id(null);
                dtoInsert.setWarehouse_name(null);
                dtoInsert.setType(true);
                dtoResult.add(dtoInsert);
            }
            else if (quantitySumExport>0){
                dtoInsert.setProduct_id(dtoProduct.getId());
                dtoInsert.setProduct_name(dtoProduct.getName());
                dtoInsert.setProduct_category(dtoProduct.getProductCategory().getName());
                dtoInsert.setUnit(dtoProduct.getUnit());
                dtoInsert.setQuantity(quantitySumExport);
                dtoInsert.setWarehouse_id(null);
                dtoInsert.setWarehouse_name(null);
                dtoInsert.setType(false);
                dtoResult.add(dtoInsert);
            }
        }
        return dtoResult;
    }



    public List<StatisticsAllProductsQueryDto> getAllReceipt(LocalDateTime from, LocalDateTime to){
        List<Clinic> clinics = clinicRepository.findAll();
        List<StatisticsImportedProductsQueryDto> dtoImport = getImported(from, to);
        List<StatisticsExportedProductsQueryDto> dtoExport = getExported(from, to);
        List<StatisticsOtherProductsQueryDto> dtoOther = getOther(from, to);
        List<StatisticsAllProductsQueryDto> dtoResult = new ArrayList<>();

        for (StatisticsImportedProductsQueryDto dto : dtoImport) {
            StatisticsAllProductsQueryDto dtoInsert = new StatisticsAllProductsQueryDto();
            dtoInsert.setProduct_id(dto.getProduct_id());
            dtoInsert.setProduct_name(dto.getProduct_name());
            dtoInsert.setProduct_category(dto.getProduct_category());
            dtoInsert.setImport_price(dto.getImport_price());
            dtoInsert.setExport_price(null);
            dtoInsert.setUnit(dto.getUnit());
            dtoInsert.setWarehouse_name(dto.getWarehouse_name());
            dtoInsert.setWarehouse_id(dto.getWarehouse_id());
            dtoInsert.setQuantity_import(dto.getQuantity_import());
            dtoInsert.setQuantity_export(0);
            dtoInsert.setType("Nhập hàng");
            dtoResult.add(dtoInsert);
        }
        for (StatisticsExportedProductsQueryDto dto : dtoExport) {
            StatisticsAllProductsQueryDto dtoInsert = new StatisticsAllProductsQueryDto();
            dtoInsert.setProduct_id(dto.getProduct_id());
            dtoInsert.setProduct_name(dto.getProduct_name());
            dtoInsert.setProduct_category(dto.getProduct_category());
            dtoInsert.setImport_price(null);
            dtoInsert.setExport_price(dto.getExport_price());
            dtoInsert.setUnit(dto.getUnit());
            dtoInsert.setWarehouse_name(dto.getWarehouse_name());
            dtoInsert.setWarehouse_id(dto.getWarehouse_id());
            dtoInsert.setQuantity_import(0);
            dtoInsert.setQuantity_export(dto.getQuantity_export());
            dtoInsert.setType("Xuất bán");
            dtoResult.add(dtoInsert);
        }
        for (StatisticsOtherProductsQueryDto dto : dtoOther) {
            StatisticsAllProductsQueryDto dtoInsert = new StatisticsAllProductsQueryDto();
            dtoInsert.setProduct_id(dto.getProduct_id());
            dtoInsert.setProduct_name(dto.getProduct_name());
            dtoInsert.setProduct_category(dto.getProduct_category());
            dtoInsert.setImport_price(null);
            dtoInsert.setExport_price(null);
            dtoInsert.setUnit(dto.getUnit());
            dtoInsert.setWarehouse_name(dto.getWarehouse_name());
            dtoInsert.setWarehouse_id(dto.getWarehouse_id());
            if (dto.isType() == true){
                dtoInsert.setQuantity_import(dto.getQuantity());
                dtoInsert.setQuantity_export(null);
                dtoInsert.setType("Nhập khác");
            } else{
                dtoInsert.setQuantity_import(null);
                dtoInsert.setQuantity_export(dto.getQuantity());
                dtoInsert.setType("Xuất khác");
            }
            dtoResult.add(dtoInsert);

        }

        return dtoResult;
    }



    @Override
    public List<StatisticsImportedProductsQueryDto> getStatisticsImportedProducts(LocalDateTime from, LocalDateTime to) {
        List<StatisticsImportedProductsQueryDto> dtoResult = getImported(from,to);
        return dtoResult;
    }

    @Override
    public List<StatisticsExportedProductsQueryDto> getStatisticsExportedProducts(LocalDateTime from, LocalDateTime to) {
        List<StatisticsExportedProductsQueryDto> dtoResult = getExported(from,to);
        return dtoResult;
    }

    @Override
    public List<StatisticsOtherProductsQueryDto> getStatisticsOtherProducts(LocalDateTime from, LocalDateTime to) {
        List<StatisticsOtherProductsQueryDto> dtoResult = getOther(from,to);
        return dtoResult;
    }
    @Override
    public List<StatisticsAllProductsQueryDto> getStatisticsAllProducts(LocalDateTime from, LocalDateTime to) {
        List<Product> products = productRepository.findAll();
        List<Clinic> clinics = clinicRepository.findAll();
        List<StatisticsAllProductsQueryDto> dtoSearch = getAllReceipt(from,to);
        List<StatisticsAllProductsQueryDto> dtoResult = new ArrayList<>();
        LocalDateTime to_present = LocalDateTime.now();
        LocalDateTime from_full_day = LocalDateTime.of(from.getYear(), from.getMonth(), from.getDayOfMonth(), 23,59,59);
        //Lấy tổng tất cả kho
        for (Product dtoProduct:products){
            StatisticsAllProductsQueryDto dtoInsert = new StatisticsAllProductsQueryDto();
            Integer sumImport=0;
            Integer sumExport=0;
            for(StatisticsAllProductsQueryDto itemSearch:dtoSearch){
                System.out.println("So id:" +dtoProduct.getId());
                System.out.println("Số id :" +itemSearch.getProduct_id());
                if(dtoProduct.getId()== itemSearch.getProduct_id() && (itemSearch.getType()=="Nhập hàng" || itemSearch.getType()=="Nhập khác")){
                    sumImport += itemSearch.getQuantity_import();
                    System.out.println("Tổng nhập: "+sumImport);
                } else if(dtoProduct.getId()== itemSearch.getProduct_id() && (itemSearch.getType()=="Xuất bán" || itemSearch.getType()=="Xuất khác")){
                    sumExport += itemSearch.getQuantity_export();
                    System.out.println("Tổng xuat: "+sumExport);
                }
            }
            if(sumExport>0||sumImport>0){
                Integer stock_present = clinicStockRepository.stockPresentByProductId(dtoProduct.getId(), null);
                Integer stock_first = stock_present
                        + (productStatisticsRepository.getAllProductExportByClinic(null, dtoProduct.getId(), from_full_day, to_present)==null ? 0 : productStatisticsRepository.getAllProductExportByClinic(null, dtoProduct.getId(), from_full_day, to_present))
                        + (productStatisticsRepository.getAllProductImportOtherByClinic(null, false, dtoProduct.getId(), from_full_day, to_present)==null ? 0 :productStatisticsRepository.getAllProductImportOtherByClinic(null, false, dtoProduct.getId(), from_full_day, to_present))
                        - (productStatisticsRepository.getAllProductImportByClinic(null, dtoProduct.getId(), from_full_day, to_present)==null ? 0 : productStatisticsRepository.getAllProductImportByClinic(null, dtoProduct.getId(), from_full_day, to_present))
                        - (productStatisticsRepository.getAllProductImportOtherByClinic(null, true, dtoProduct.getId(), from_full_day, to_present)==null ? 0 : productStatisticsRepository.getAllProductImportOtherByClinic(null, true, dtoProduct.getId(), from_full_day, to_present));
                Integer stock_end = stock_present
                        + (productStatisticsRepository.getAllProductExportByClinic(null, dtoProduct.getId(), to, to_present)==null ? 0 : productStatisticsRepository.getAllProductExportByClinic(null, dtoProduct.getId(), to, to_present))
                        + (productStatisticsRepository.getAllProductImportOtherByClinic(null, false, dtoProduct.getId(), to, to_present)==null ? 0 : productStatisticsRepository.getAllProductImportOtherByClinic(null, false, dtoProduct.getId(), to, to_present))
                        - (productStatisticsRepository.getAllProductImportByClinic(null, dtoProduct.getId(), to, to_present)==null ? 0 : productStatisticsRepository.getAllProductImportByClinic(null, dtoProduct.getId(), to, to_present))
                        - (productStatisticsRepository.getAllProductImportOtherByClinic(null, true, dtoProduct.getId(), to, to_present)==null ? 0 :productStatisticsRepository.getAllProductImportOtherByClinic(null, true, dtoProduct.getId(), to, to_present));
                dtoInsert.setProduct_id(dtoProduct.getId());
                dtoInsert.setProduct_name(dtoProduct.getName());
                dtoInsert.setProduct_category(dtoProduct.getProductCategory().getName());
                dtoInsert.setImport_price(dtoProduct.getWholesalePrice());
                dtoInsert.setExport_price(dtoProduct.getPrice());
                dtoInsert.setUnit(dtoProduct.getUnit());
                dtoInsert.setWarehouse_name(null);
                dtoInsert.setWarehouse_id(null);
                dtoInsert.setQuantity_import(sumImport);
                dtoInsert.setQuantity_export(sumExport);
                dtoInsert.setStock_first(stock_first);
                dtoInsert.setStock_end(stock_end);
                dtoInsert.setType(null);
                dtoResult.add(dtoInsert);
            }
//            Lấy theo từng kho
            for(Clinic dtoClinic:clinics){
                StatisticsAllProductsQueryDto dtoInsertbyClinic = new StatisticsAllProductsQueryDto();
                Integer sumImportClinic=0;
                Integer sumExportClinic=0;
                for(StatisticsAllProductsQueryDto itemSearch:dtoSearch){
                    if(dtoProduct.getId()== itemSearch.getProduct_id() && (itemSearch.getType()=="Nhập hàng" || itemSearch.getType()=="Nhập khác") && itemSearch.getWarehouse_id()==dtoClinic.getId()){
                        sumImportClinic+=itemSearch.getQuantity_import();
                    } else if(dtoProduct.getId()== itemSearch.getProduct_id() && (itemSearch.getType()=="Xuất bán" || itemSearch.getType()=="Xuất khác") && itemSearch.getWarehouse_id()==dtoClinic.getId()){
                        sumExportClinic+=itemSearch.getQuantity_export();
                    }
                }
                if(sumImportClinic>0||sumExportClinic>0){
                    Integer stock_clinic_present = clinicStockRepository.stockPresentByProductId(dtoProduct.getId(), dtoClinic.getId());
                    Integer stock_clinic_first = stock_clinic_present
                            + (productStatisticsRepository.getAllProductExportByClinic(dtoClinic.getId(), dtoProduct.getId(), from_full_day, to_present)==null ? 0 : productStatisticsRepository.getAllProductExportByClinic(dtoClinic.getId(), dtoProduct.getId(), from_full_day, to_present))
                            + (productStatisticsRepository.getAllProductImportOtherByClinic(dtoClinic.getId(), false, dtoProduct.getId(), from_full_day, to_present)==null ? 0 :productStatisticsRepository.getAllProductImportOtherByClinic(dtoClinic.getId(), false, dtoProduct.getId(), from_full_day, to_present))
                            - (productStatisticsRepository.getAllProductImportByClinic(dtoClinic.getId(), dtoProduct.getId(), from_full_day, to_present)==null ? 0 : productStatisticsRepository.getAllProductImportByClinic(dtoClinic.getId(), dtoProduct.getId(), from_full_day, to_present))
                            - (productStatisticsRepository.getAllProductImportOtherByClinic(dtoClinic.getId(), true, dtoProduct.getId(), from_full_day, to_present)==null ? 0 : productStatisticsRepository.getAllProductImportOtherByClinic(dtoClinic.getId(), true, dtoProduct.getId(), from_full_day, to_present));
                    Integer stock_clinic_end = stock_clinic_present
                            + (productStatisticsRepository.getAllProductExportByClinic(dtoClinic.getId(), dtoProduct.getId(), to, to_present)==null ? 0 : productStatisticsRepository.getAllProductExportByClinic(dtoClinic.getId(), dtoProduct.getId(), to, to_present))
                            + (productStatisticsRepository.getAllProductImportOtherByClinic(dtoClinic.getId(), false, dtoProduct.getId(), to, to_present)==null ? 0 : productStatisticsRepository.getAllProductImportOtherByClinic(dtoClinic.getId(), false, dtoProduct.getId(), to, to_present))
                            - (productStatisticsRepository.getAllProductImportByClinic(dtoClinic.getId(), dtoProduct.getId(), to, to_present)==null ? 0 : productStatisticsRepository.getAllProductImportByClinic(dtoClinic.getId(), dtoProduct.getId(), to, to_present))
                            - (productStatisticsRepository.getAllProductImportOtherByClinic(dtoClinic.getId(), true, dtoProduct.getId(), to, to_present)==null ? 0 : productStatisticsRepository.getAllProductImportOtherByClinic(dtoClinic.getId(), true, dtoProduct.getId(), to, to_present));
                    dtoInsert.setProduct_id(dtoProduct.getId());
                    dtoInsertbyClinic.setProduct_id(dtoProduct.getId());
                    dtoInsertbyClinic.setProduct_name(dtoProduct.getName());
                    dtoInsertbyClinic.setProduct_category(dtoProduct.getProductCategory().getName());
                    dtoInsertbyClinic.setImport_price(dtoProduct.getWholesalePrice());
                    dtoInsertbyClinic.setExport_price(dtoProduct.getPrice());
                    dtoInsertbyClinic.setUnit(dtoProduct.getUnit());
                    dtoInsertbyClinic.setWarehouse_name(dtoClinic.getName());
                    dtoInsertbyClinic.setWarehouse_id(dtoClinic.getId());
                    dtoInsertbyClinic.setQuantity_import(sumImportClinic);
                    dtoInsertbyClinic.setQuantity_export(sumExportClinic);
                    dtoInsertbyClinic.setStock_first(stock_clinic_first);
                    dtoInsertbyClinic.setStock_end(stock_clinic_end);
                    dtoInsertbyClinic.setType(null);
                    dtoResult.add(dtoInsertbyClinic);
                }
            }
;        }
        return dtoResult;
    }
}
