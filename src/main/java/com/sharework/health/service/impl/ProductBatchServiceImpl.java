package com.sharework.health.service.impl;

import com.sharework.health.Constants;
import com.sharework.health.convert.ProductBatchConvert;
import com.sharework.health.dto.ProductBatchDto;
import com.sharework.health.dto.QueryProductBatch;
import com.sharework.health.entity.Product;
import com.sharework.health.entity.ProductBatch;
import com.sharework.health.repository.ProductBatchRepository;
import com.sharework.health.repository.ProductRepository;
import com.sharework.health.response.ProductBatchReponse;
import com.sharework.health.service.ProductBatchService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class ProductBatchServiceImpl implements ProductBatchService {

    private ProductBatchRepository productBatchRepository;

    private ProductBatchConvert productBatchConvert;

    private ProductRepository productRepository;

    @Override
    public List<ProductBatchDto> findAll() {

        List<ProductBatch> entitys = productBatchRepository.findAll();
        List<ProductBatchDto> dtos = new ArrayList<>();

        for (ProductBatch entity : entitys){
            ProductBatchDto dto = productBatchConvert.entityToDto(entity);
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public ProductBatchDto findById(Integer id) {
        ProductBatch entity = productBatchRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return productBatchConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(ProductBatchDto dto) {
        Product product = productRepository.findById(dto.getProductDto().getId()).get();
        if (dto == null || product == null ){
            return false;
        }
        ProductBatch entity = productBatchConvert.dtoToEntity(dto);
        entity.setId(null);
        entity.setExpiry(dto.getExpiry());
        entity.setQuantity(0);
        entity.setProductId(product);
        String name = product.getName() + "_" +dto.getExpiry();
        entity.setName(name);
        productBatchRepository.save(entity);
        return true;
    }

    @Override
    public boolean update(Integer id, ProductBatchDto dto) {
        ProductBatch entity = productBatchRepository.findById(id).get();

        if (entity == null){
            return false;
        }

        entity.setQuantity(entity.getQuantity());
        entity.setProductId(entity.getProductId());
        entity.setExpiry(dto.getExpiry());
        String name = entity.getProductId().getName() + "_" +dto.getExpiry();
        entity.setName(name);
        productBatchRepository.save(entity);
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        ProductBatch entity = productBatchRepository.findById(id).get();

        if (entity == null){
            return false;
        }

        try {
            productBatchRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public List<ProductBatchDto> findProductBatchIdByIdProduct(Integer id) {
        List<ProductBatch> entitys = productBatchRepository.findProductBatchIdByIdProduct(id);
        List<ProductBatchDto> dtos = new ArrayList<>();

        if (entitys == null){
            return null;
        }

        for (ProductBatch entity : entitys){
            ProductBatchDto dto = productBatchConvert.entityToDto(entity);
            dtos.add(dto);
        }

        return dtos;
    }

    @Override
    public List<ProductBatchDto> findProductByExpiry(LocalDate expiryStart, LocalDate expiryEnd) {
        List<ProductBatch> entitys = productBatchRepository.findProductByExpiry(expiryStart,expiryEnd);

        if (expiryStart == null && expiryEnd == null){
            return null;
        }

        List<ProductBatchDto> dtos = new ArrayList<>();

        for (ProductBatch entity : entitys){
            ProductBatchDto dto = productBatchConvert.entityToDto(entity);
            dtos.add(dto);
        }

        return dtos;
    }

    @Override
    public List<ProductBatchDto> findByLikeName(String name) {
        List<ProductBatch> entitys = productBatchRepository.findByLikeName(name);
        List<ProductBatchDto> dtos = new ArrayList<>();

        if (entitys == null){
            return null;
        }

        for (ProductBatch entity : entitys){
            ProductBatchDto dto = productBatchConvert.entityToDto(entity);
            dtos.add(dto);
        }

        return dtos;
    }

    @Override
    public List<ProductBatchDto> findByLikeNameAndExpiry(String name, LocalDate expiryStart, LocalDate expiryEnd) {
        List<ProductBatch> entitys = productBatchRepository.findByLikeNameAndExpiry(name,expiryStart,expiryEnd);
        List<ProductBatchDto> dtos = new ArrayList<>();

        if (entitys == null){
            return null;
        }

        for (ProductBatch entity : entitys){
            ProductBatchDto dto = productBatchConvert.entityToDto(entity);
            dtos.add(dto);
        }

        return dtos;
    }

    @Override
    public ProductBatchReponse getAllProductBatch(int pageNo, int pageSize, String sortBy, String sortDir) {

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        Page<QueryProductBatch> posts = productBatchRepository.getAllProductBatchPaging(pageable);

        List<QueryProductBatch> listOfPosts = posts.getContent();

        List<QueryProductBatch> content = listOfPosts.stream().collect(Collectors.toList());

        ProductBatchReponse productBatch = new ProductBatchReponse();
        productBatch.setData(content);
        productBatch.setCurrent(posts.getNumber());
        productBatch.setPageSize(posts.getSize());
        productBatch.setTotalElements(posts.getTotalElements());
        productBatch.setTotalPages(posts.getTotalPages());
        productBatch.setLast(posts.isLast());

        return productBatch;
    }
    public static LocalDate
    getDateFromString(String string,
                      DateTimeFormatter format)
    {
        // Converting the string to date
        // in the specified format
        LocalDate date = LocalDate.parse(string, format);

        // Returning the converted date
        return date;
    }
    @Override
    public ProductBatchReponse getAllProductBatchByName(int pageNo, int pageSize, String sortBy, String sortDir, String name) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        System.err.println( Constants.revertInput(name));
        Page<QueryProductBatch> posts = productBatchRepository.findByLikeName(pageable,Constants.revertInput(name));

        List<QueryProductBatch> listOfPosts = posts.getContent();

        List<QueryProductBatch> content = listOfPosts.stream().collect(Collectors.toList());

        ProductBatchReponse productBatch = new ProductBatchReponse();
        productBatch.setData(content);
        productBatch.setCurrent(posts.getNumber());
        productBatch.setPageSize(posts.getSize());
        productBatch.setTotalElements(posts.getTotalElements());
        productBatch.setTotalPages(posts.getTotalPages());
        productBatch.setLast(posts.isLast());

        return productBatch;
    }

    @Override
    public ProductBatchReponse getAllProductBatchByExpiry(int pageNo, int pageSize, String sortBy, String sortDir, String expiryStart, String expiryEnd) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);

        DateTimeFormatter format
                = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate start
                = getDateFromString(expiryStart, format);
        LocalDate end
                = getDateFromString(expiryEnd, format);
        Page<QueryProductBatch> posts = productBatchRepository.findProductByExpiry(pageable, start, end);

        List<QueryProductBatch> listOfPosts = posts.getContent();

        List<QueryProductBatch> content = listOfPosts.stream().collect(Collectors.toList());

        ProductBatchReponse productBatch = new ProductBatchReponse();
        productBatch.setData(content);
        productBatch.setCurrent(posts.getNumber());
        productBatch.setPageSize(posts.getSize());
        productBatch.setTotalElements(posts.getTotalElements());
        productBatch.setTotalPages(posts.getTotalPages());
        productBatch.setLast(posts.isLast());

        return productBatch;
    }
}
