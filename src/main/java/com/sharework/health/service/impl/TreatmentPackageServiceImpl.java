package com.sharework.health.service.impl;

import com.sharework.health.Constants;
import com.sharework.health.convert.TreatmentPackageConvert;
import com.sharework.health.convert.TreatmentPackageServiceConvert;
import com.sharework.health.dto.ClinicStockDto;
import com.sharework.health.dto.TreatmentPackageDto;
import com.sharework.health.dto.TreatmentPackageServiceDto;
import com.sharework.health.dto.UserDto;
import com.sharework.health.entity.ClinicStock;
import com.sharework.health.entity.Status;
import com.sharework.health.entity.TreatmentPackage;
import com.sharework.health.entity.ServiceCategory;
import com.sharework.health.repository.ServiceCategoryRepository;
import com.sharework.health.repository.ServiceRepository;
import com.sharework.health.repository.TreatmentPackageRepository;
import com.sharework.health.repository.TreatmentPackageServiceRepository;
import com.sharework.health.response.ClinicStockReponse;
import com.sharework.health.response.TreatmentPackageReponse;
import com.sharework.health.service.TreatmentPackageService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class TreatmentPackageServiceImpl implements TreatmentPackageService {

    private TreatmentPackageRepository treatmentPackageRepository;

    private TreatmentPackageConvert treatmentPackageConvert;

    private ServiceCategoryRepository serviceCategoryRepository;

    private ServiceRepository serviceRepository;

    private TreatmentPackageServiceRepository treatmentPackageServiceRepository;

    private TreatmentPackageServiceConvert treatmentPackageServiceConvert;
    @Override
    public List<TreatmentPackageDto> findAll() {
        List<TreatmentPackage> treatmentPackages = treatmentPackageRepository.findAll();
        List<TreatmentPackageDto> treatmentPackageDtos = new ArrayList<>();
        for (TreatmentPackage entity: treatmentPackages
             ) {
            if (entity.getStatus() != Status.DEACTIVE){
                TreatmentPackageDto dto = treatmentPackageConvert.entityToDto(entity);
                treatmentPackageDtos.add(dto);
            }
        }
        return treatmentPackageDtos;
    }

    @Override
    public TreatmentPackageDto findById(Integer id) {
        TreatmentPackage entity = treatmentPackageRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return treatmentPackageConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(TreatmentPackageDto dto) {
        if (dto == null) {
            return false;
        }
        try {
            ServiceCategory serviceCategory =
                    serviceCategoryRepository.findById(dto.getServiceCategoryDto().getId()).get();
            TreatmentPackage entity = treatmentPackageConvert.dtoToEntity(dto);
            entity.setId(null);
            entity.setStatus(Status.ACTIVE);
            treatmentPackageRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Integer id, TreatmentPackageDto dto) {
        TreatmentPackage entity = treatmentPackageRepository.findById(id).get();
//        ServiceCategory serviceCategory =
//                serviceCategoryRepository.findById(dto.getServiceCategoryDto().getId()).get();
        if (dto == null) {
            return false;
        }
            entity.setName(dto.getName());
            if(dto.getStatus().equalsIgnoreCase("ACTIVE")){
                entity.setStatus(Status.ACTIVE);
            }
            if(dto.getStatus().equalsIgnoreCase("DEACTIVE")){
                entity.setStatus(Status.DEACTIVE);
            }
            if(dto.getStatus().equalsIgnoreCase("EXAMINED")){
                entity.setStatus(Status.EXAMINED);
            }
            entity.setTotal(dto.getTotal());
            treatmentPackageRepository.save(entity);
            return true;
    }

    @Override
    public boolean delete(Integer id) {
        TreatmentPackage entity = treatmentPackageRepository.findById(id).get();
        if (entity == null){
            return false;
        }
        entity.setStatus(Status.DEACTIVE);
        return true;
    }

    @Override
    public TreatmentPackageDto searchTreatmentPackageByName(String name) {
        TreatmentPackage entity =
                treatmentPackageRepository.findByName(name);
        if (entity == null){
            return null;
        }
        return treatmentPackageConvert.entityToDto(entity);
    }

    @Override
    public boolean addTreatmentPackageService(TreatmentPackageServiceDto dto) {
        TreatmentPackage treatmentPackage = treatmentPackageRepository.findById(dto.getTreatmentPackageDto().getId()).get();
        if (treatmentPackage == null){
            return false;
        }
        com.sharework.health.entity.Service service = serviceRepository.findById(dto.getServiceDto().getId()).get();
        if (service == null){
            return false;
        }
//        System.out.println(treatmentPackage);
//        System.out.println(service);
        com.sharework.health.entity.TreatmentPackageService treatmentPackageService
                = new com.sharework.health.entity.TreatmentPackageService()
                .setService(service)
                .setTreatmentPackage(treatmentPackage)
                .setNumberOfUse(dto.getNumberOfUse())
                .setPrice(dto.getPrice());
//        treatmentPackage.setTreatmentPackageServices(Arrays.asList(treatmentPackageService));
//        treatmentPackageRepository.save(treatmentPackage);
        treatmentPackageServiceRepository.save(treatmentPackageService);
        return true;
    }

    @Override
    public List<TreatmentPackageServiceDto> findAllByTreatmentPackage(Integer treatmentPackageId) {
        List<com.sharework.health.entity.TreatmentPackageService> treatmentPackageServices = treatmentPackageServiceRepository.findAllByTreatmentPackage(treatmentPackageId);
        List<TreatmentPackageServiceDto> treatmentPackageServiceDtos = new ArrayList<>();
        for (com.sharework.health.entity.TreatmentPackageService entity: treatmentPackageServices
             ) {
            TreatmentPackageServiceDto dto = treatmentPackageServiceConvert.entityToDto(entity);
            treatmentPackageServiceDtos.add(dto);
        }
        return treatmentPackageServiceDtos;
    }

    @Override
    public TreatmentPackageDto findTreatmentPackageAfterInsert() {
        TreatmentPackage entity = treatmentPackageRepository.findTreatmentPackageAfterInsert();
        if (entity == null){
            return null;
        }
        return treatmentPackageConvert.entityToDto(entity);
    }

    @Override
    public List<TreatmentPackageServiceDto> findAllByCustomerId(Integer customerId) {
        List<com.sharework.health.entity.TreatmentPackageService> treatmentPackageServices = treatmentPackageServiceRepository.findAllByCustomerId(customerId);
        List<TreatmentPackageServiceDto> treatmentPackageServiceDtos = new ArrayList<>();

        for(com.sharework.health.entity.TreatmentPackageService entity: treatmentPackageServices){
            TreatmentPackageServiceDto dto = treatmentPackageServiceConvert.entityToDto(entity);
            treatmentPackageServiceDtos.add(dto);
        }
        return treatmentPackageServiceDtos;
    }

    @Override
    public List<TreatmentPackageDto> findByCustomerId(Integer customerId) {
        List<TreatmentPackage> treatmentPackages = treatmentPackageRepository.findByCustomerId(customerId);
        List<TreatmentPackageDto> treatmentPackageDtos = new ArrayList<>();

        for(TreatmentPackage entity: treatmentPackages){
            TreatmentPackageDto dto = treatmentPackageConvert.entityToDto(entity);
            treatmentPackageDtos.add(dto);
        }
        return treatmentPackageDtos;
    }

    @Override
    public TreatmentPackageReponse getAllTreatmentPackage(int pageNo, int pageSize, String sortBy, String sortDir) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize,sort);

//        Page<TreatmentPackage> treatmentPackages = treatmentPackageRepository.findAll(pageable);
        List<TreatmentPackage> treatmentPackages = treatmentPackageRepository.findAll();
        List<TreatmentPackage> treatmentPackageDtos = new ArrayList<>();
        for (TreatmentPackage entity: treatmentPackages
        ) {
            if (entity.getStatus() != Status.DEACTIVE){
                treatmentPackageDtos.add(entity);
            }
        }
        //convert Clinic
        int start = Math.min((int)pageable.getOffset(), treatmentPackageDtos.size());
        int end = Math.min((start + pageable.getPageSize()), treatmentPackageDtos.size());
        Page<TreatmentPackage> packages = new PageImpl<TreatmentPackage>(treatmentPackageDtos.subList(start, end), pageable, treatmentPackageDtos.size());
        List<TreatmentPackage> listOfTreatmentPackages = packages.getContent();

        List<TreatmentPackageDto> productDtos = listOfTreatmentPackages.stream().map(post -> treatmentPackageConvert.entityToDto(post)).collect(Collectors.toList());

        TreatmentPackageReponse treatmentPackageReponse = new TreatmentPackageReponse();
        treatmentPackageReponse.setData(productDtos);
        treatmentPackageReponse.setCurrent(packages.getNumber());
        treatmentPackageReponse.setPageSize(packages.getSize());
        treatmentPackageReponse.setTotalElements(packages.getTotalElements());
        treatmentPackageReponse.setTotalPages(packages.getTotalPages());
        treatmentPackageReponse.setLast(packages.isLast());

        return treatmentPackageReponse;

    }

    @Override
    public TreatmentPackageReponse getAllTreatmentPackageByInput(int pageNo, int pageSize, String sortBy, String sortDir, String input) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize,sort);
        Page<TreatmentPackage> treatmentPackages = null;

        if(input.equalsIgnoreCase(Constants.ACTIVE) || input.equalsIgnoreCase(Constants.DEACTIVE) || input.equalsIgnoreCase(Constants.INACTIVE) || input.equalsIgnoreCase(Constants.EXAMINED)){
            treatmentPackages = treatmentPackageRepository.findTreatmentPackageByStatus(pageable, input);
        }else {
            System.err.println(input);
            treatmentPackages = treatmentPackageRepository.findTreatmentPackageByName(pageable, input);
        }

        List<TreatmentPackage> listOfTreatmentPackages = treatmentPackages.getContent();

        List<TreatmentPackageDto> productDtos = listOfTreatmentPackages.stream().map(post -> treatmentPackageConvert.entityToDto(post)).collect(Collectors.toList());

        TreatmentPackageReponse treatmentPackageReponse = new TreatmentPackageReponse();
        treatmentPackageReponse.setData(productDtos);
        treatmentPackageReponse.setCurrent(treatmentPackages.getNumber());
        treatmentPackageReponse.setPageSize(treatmentPackages.getSize());
        treatmentPackageReponse.setTotalElements(treatmentPackages.getTotalElements());
        treatmentPackageReponse.setTotalPages(treatmentPackages.getTotalPages());
        treatmentPackageReponse.setLast(treatmentPackages.isLast());

        return treatmentPackageReponse;
    }
}
