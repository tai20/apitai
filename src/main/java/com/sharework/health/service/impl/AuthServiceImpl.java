package com.sharework.health.service.impl;

import java.util.Date;

import com.sharework.health.dto.LoginDto;
import com.sharework.health.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class AuthServiceImpl implements AuthService {

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Override
	public String auth(LoginDto loginDto) {
		
		Authentication authentication = 
				new UsernamePasswordAuthenticationToken(loginDto.getEmail(), loginDto.getPassword());
		
		authenticationManager.authenticate(authentication);
		
		// TẠO TOKEN
		Date nowDate = new Date();
		
		String token = Jwts.builder()
		.setSubject(loginDto.getEmail())
		.setIssuedAt(nowDate)
		.setExpiration(new Date(nowDate.getTime() + 864000000L))
//		.setExpiration(new Date(nowDate.getTime() + 60000))
		.signWith(SignatureAlgorithm.HS512, "TUAN_BACKEND_SHAREWORK")
		.compact();
		
		return token;
	}

}
