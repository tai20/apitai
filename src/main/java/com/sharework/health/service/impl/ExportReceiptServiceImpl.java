package com.sharework.health.service.impl;

import com.sharework.health.convert.ExportReceiptConvert;
import com.sharework.health.convert.ExportReceiptDetailsConvert;
import com.sharework.health.dto.*;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.response.ExportReceiptReponse;
import com.sharework.health.response.ImportRecepitReponse;
import com.sharework.health.service.ExportReceiptService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class ExportReceiptServiceImpl implements ExportReceiptService {

    private ExportReceiptConvert exportReceiptConvert;

    private ExportReceiptRepository exportReceiptRepository;

    private ClinicRepository clinicRepository;

    private UserRepository userRepository;

    private OrderRepository orderRepository;

    private ClinicStockRepository clinicStockRepository;

    private ExportReceiptDetailsConvert exportReceiptDetailsConvert;

    private ExportReceiptDetailsRepository exportReceiptDetailsRepository;

    private ProductBatchRepository productBatchRepository;

    private ProductRepository productRepository;

    private ProductStatisticsRepository productStatisticsRepository;

    public void setSumPrice(Integer receipt_id){
        BigDecimal sumPrice = exportReceiptRepository.getSumPrice(receipt_id);
        Optional<ExportReceipt> exportReceipt = exportReceiptRepository.findById(receipt_id);
        exportReceipt.get().setSumprice(sumPrice);
        exportReceiptRepository.save(exportReceipt.get());
    }

    @Override
    public List<ExportReceiptDto> findAll(){
        List<ExportReceipt> exportReceipts = exportReceiptRepository.findAllSortDateDESC();
        List<ExportReceiptDto> exportReceiptDtos = new ArrayList<>();
        for (ExportReceipt entity:exportReceipts){
            setSumPrice(entity.getId());
            ExportReceiptDto dto = exportReceiptConvert.entityToDto(entity);
            exportReceiptDtos.add(dto);
        }
        return exportReceiptDtos;
    }

//   lay all phieu xuat theo kho
//    public List<ExportReceiptDto> getAllExportWarehouseReceipt(Integer clinic_id){
//        List<ExportReceipt> exportReceipts = exportReceiptRepository.findAllExportSortDateDESC(clinic_id);
//        List<ExportReceiptDto> exportReceiptDtos = new ArrayList<>();
//        for (ExportReceipt entity:exportReceipts){
//            setSumPrice(entity.getId());
//            ExportReceiptDto dto = exportReceiptConvert.entityToDto(entity);
//            exportReceiptDtos.add(dto);
//        }
//        return exportReceiptDtos;
//    }

    @Override
    public ExportReceiptDto findById(Integer id){
        Optional<ExportReceipt> foundDto = exportReceiptRepository.findById(id);
        if (foundDto.isPresent()){
            ExportReceipt entity = exportReceiptRepository.findById(id).get();
            setSumPrice(entity.getId());
            return exportReceiptConvert.entityToDto(entity);
        }
        return null;
    }


    @Override
    public boolean insert(ExportReceiptDto dto){
        return false;
    }

    @Override
    public ExportReceiptDto insertExportReceipt(ExportReceiptDto dto){
        ExportReceipt entity = exportReceiptConvert.dtoToEntity(dto);
        Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
        Order order = orderRepository.findById(dto.getOrderDto().getId()).get();
        User user = userRepository.findById(dto.getUserDto().getId()).get();
        entity.setId(null);
        if (clinic == null || user == null || order == null){
            return null;
        } else {
            entity.setClinic(clinic);
            entity.setUser(user);
            entity.setOrder(order);
        }
        entity.setDateexport(LocalDateTime.now());
        entity.setReason(dto.getReason());
        entity.setSumprice(new BigDecimal(0));
        entity.setStatus(ReceiptStatus.ACTIVE);
        ExportReceipt result = exportReceiptRepository.save(entity);
        return exportReceiptConvert.entityToDto(result);
    }

    @Override
    public boolean update(Integer id, ExportReceiptDto exportReceiptDto){
        return  false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }


    @Override
    public ExportReceiptDetailsDto insertExportReceiptDetail(ExportReceiptDetailsDto dto){
        ExportReceiptDetails entity = exportReceiptDetailsConvert.dtoToEntity(dto);
        ExportReceipt exportReceipt = exportReceiptRepository.findById(dto.getExportReceiptDto().getId()).get();
        ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinicStockDto().getId()).get();

        ProductBatch productBatch = productBatchRepository.findById(dto.getProductBatchDto().getId()).get();
        ClinicStock clinicStockByClinicIdAndAndProductId = clinicStockRepository.getClinicStockByClinicIdAndAndProductId(clinicStock.getId(),productBatch.getProductId().getId());

        List<ExportReceiptDetails> exportReceiptDetails = exportReceiptDetailsRepository.getExportReceiptByClinicStockIdReceiptId(dto.getExportReceiptDto().getId(),clinicStockByClinicIdAndAndProductId.getId());
        if (exportReceipt.getClinic().getId() != clinicStockByClinicIdAndAndProductId.getClinic().getId() || dto.getNumber_product_export()<=0 || dto.getExport_price().compareTo(new BigDecimal(0))==-1) return null;
        if (clinicStockByClinicIdAndAndProductId.getQuantity() < dto.getNumber_product_export() ||clinicStockByClinicIdAndAndProductId == null) return null;
        if (exportReceipt == null || clinicStockByClinicIdAndAndProductId == null || exportReceipt.getStatus() !=ReceiptStatus.ACTIVE || productBatch == null){
            return null;
        } else
            //Check sp thêm vào có chưa, nếu có rồi thì cộng vào thêm
            if(exportReceiptDetails.isEmpty()){
                entity.setNumber_product_export(dto.getNumber_product_export());
                entity.setExport_price(dto.getExport_price());

            } else{
                for(ExportReceiptDetails itemSearch:exportReceiptDetails){
                    entity.setNumber_product_export(dto.getNumber_product_export()+itemSearch.getNumber_product_export());
                    BigDecimal total = itemSearch.getExport_price().add(dto.getExport_price());
                    entity.setExport_price(total);
                }

            }
        if ( (productBatch.getQuantity() - dto.getNumber_product_export()) < 0){
            return null;
        }
        productBatch.setQuantity(productBatch.getQuantity() - dto.getNumber_product_export());
        clinicStockByClinicIdAndAndProductId.setQuantity(clinicStockByClinicIdAndAndProductId.getQuantity() - dto.getNumber_product_export());
        entity.setExportReceipt(exportReceipt);
        entity.setClinicStock(clinicStockByClinicIdAndAndProductId);
        entity.setProductBatchId(productBatch);
        productBatchRepository.save(productBatch);
        clinicStockRepository.save(clinicStockByClinicIdAndAndProductId);

        Product products = productRepository.findById(productBatch.getProductId().getId()).get();
        List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

        for (ClinicStock entity2 : clinicStocks){
            if(products.getId() == entity2.getProduct().getId()){
                products.setQuantity(productStatisticsRepository.getSumProduct(productBatch.getProductId().getId()));
            }
            productRepository.save(products);
        }

        ExportReceiptDetails result = exportReceiptDetailsRepository.save(entity);
        setSumPrice(entity.getExportReceipt().getId());
        return exportReceiptDetailsConvert.entityToDto(result);
    }

    @Override
    public boolean lockExportReceipt(Integer receipt_id){
        ExportReceipt exportReceipt = exportReceiptRepository.findById(receipt_id).get();
        if (exportReceipt.getStatus() == ReceiptStatus.ACTIVE){
            exportReceipt.setStatus(ReceiptStatus.LOCKED);
            return true;
        }
        return false;
    }

    @Override
    public List<ExportReceiptDetailsQueryDto> getAllExportReceiptDetailByReceiptid(Integer receipt_id){
        List<ExportReceiptDetailsQueryDto> exportReceiptDetailsQueryDtos = exportReceiptDetailsRepository.getAllExportReceiptDetailByReceiptid(receipt_id);
        return exportReceiptDetailsQueryDtos;
    }

    @Override
    public boolean deleteExportReceipt(Integer id){

        ExportReceipt exportReceipt = exportReceiptRepository.findById(id).get();
        if(exportReceipt.getStatus()!=ReceiptStatus.ACTIVE){
            return false;
        }

        List<ExportReceiptDetailsQueryDto> exportReceiptDetailsQueryDtos = exportReceiptDetailsRepository.getAllExportReceiptDetailByReceiptid(id);
        if(!exportReceiptDetailsQueryDtos.isEmpty()){
            for (ExportReceiptDetailsQueryDto dto:exportReceiptDetailsQueryDtos){
                ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();
                clinicStock.setQuantity(clinicStock.getQuantity() + dto.getNumber_product_export());
                clinicStockRepository.save(clinicStock);

                Product products = productRepository.findById(clinicStock.getProduct().getId()).get();
                List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

                for (ClinicStock entity2 : clinicStocks){
                    if(products.getId() == entity2.getProduct().getId()){
                        products.setQuantity(productStatisticsRepository.getSumProduct(clinicStock.getProduct().getId()));
                    }
                    productRepository.save(products);
                }

            }
        }

        exportReceipt.setStatus(ReceiptStatus.DELETED);
        setSumPrice(id);
        exportReceiptRepository.save(exportReceipt);
        return true;
    }

    @Override
    public boolean deleteExportReceiptDetail(Integer receipt_id, Integer clinic_stock_id){
        ExportReceipt exportReceipt = exportReceiptRepository.findById(receipt_id).get();
        ClinicStock clinicStock = clinicStockRepository.findById(clinic_stock_id).get();
        if (exportReceipt.getStatus()!=ReceiptStatus.ACTIVE){
            return false;
        }
        List<ExportReceiptDetails> exportReceiptDetails = exportReceiptDetailsRepository.getExportReceiptByClinicStockIdReceiptId(receipt_id, clinic_stock_id);
        if (exportReceiptDetails.isEmpty()) return false;
        for(ExportReceiptDetails entity:exportReceiptDetails){
            Integer idProductBatch = entity.getProductBatchId().getId();
            ProductBatch productBatch = productBatchRepository.findById(idProductBatch).get();
            clinicStock.setQuantity(clinicStock.getQuantity() + entity.getNumber_product_export());
            productBatch.setQuantity(productBatch.getQuantity() + entity.getNumber_product_export());
            clinicStockRepository.save(clinicStock);

            Product products = productRepository.findById(productBatch.getProductId().getId()).get();
            List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

            for (ClinicStock entity2 : clinicStocks){
                if(products.getId() == entity2.getProduct().getId()){
                    products.setQuantity(productStatisticsRepository.getSumProduct(productBatch.getProductId().getId()));
                }
                productRepository.save(products);
            }

            productBatchRepository.save(productBatch);
            exportReceiptDetailsRepository.delete(entity);
        }

        setSumPrice(receipt_id);
        return true;
    }

    @Override
    public ExportReceiptDetailsQueryDto updateExportReceiptDetail(ExportReceiptDetailsQueryDto dto){
        ExportReceipt exportReceipt = exportReceiptRepository.findById(dto.getReceipt_id()).get();
        List<ExportReceiptDetails> exportReceiptDetails = exportReceiptDetailsRepository.getExportReceiptByClinicStockIdReceiptId(dto.getReceipt_id(),dto.getClinic_stock_id());
        ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();
        ProductBatch productBatch = productBatchRepository.findById(dto.getProduct_branch_id()).get();
        if (exportReceipt == null || exportReceiptDetails == null || clinicStock == null || productBatch == null){
            return null;
        }
        if (exportReceipt.getStatus()!=ReceiptStatus.ACTIVE){
            return null;
        }
        for(ExportReceiptDetails entity:exportReceiptDetails){
            int quantityChange = dto.getNumber_product_export()-entity.getNumber_product_export();
            if (clinicStock.getQuantity()< dto.getNumber_product_export() || productBatch.getQuantity() < dto.getNumber_product_export()) return null;
            clinicStock.setQuantity(clinicStock.getQuantity() - quantityChange);
            productBatch.setQuantity(productBatch.getQuantity() - quantityChange);
            productBatchRepository.save(productBatch);
            clinicStockRepository.save(clinicStock);

            Product products = productRepository.findById(productBatch.getProductId().getId()).get();
            List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

            for (ClinicStock entity2 : clinicStocks){
                if(products.getId() == entity2.getProduct().getId()){
                    products.setQuantity(productStatisticsRepository.getSumProduct(productBatch.getProductId().getId()));
                }
                productRepository.save(products);
            }

            //Luu lai phieu
            entity.setNumber_product_export(dto.getNumber_product_export());
            entity.setExport_price(dto.getExport_price());
            exportReceiptDetailsRepository.save(entity);
        }

        setSumPrice(exportReceipt.getId());
        return dto;
    }

    @Override
    public boolean restoreExportReceipt(Integer id){
        ExportReceipt exportReceipt = exportReceiptRepository.findById(id).get();
        if(exportReceipt.getStatus()!=ReceiptStatus.DELETED){
            return false;
        }
        List<ExportReceiptDetailsQueryDto> exportReceiptDetailsQueryDtos = exportReceiptDetailsRepository.getAllExportReceiptDetailByReceiptid(id);
        if(!exportReceiptDetailsQueryDtos.isEmpty()){
            for(ExportReceiptDetailsQueryDto dto:exportReceiptDetailsQueryDtos){
                ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();
                ProductBatch productBatch = productBatchRepository.findById(dto.getProduct_branch_id()).get();
                if(clinicStock.getQuantity()< dto.getNumber_product_export() || productBatch.getQuantity() < dto.getNumber_product_export()) return false;
                productBatch.setQuantity(productBatch.getQuantity() - dto.getNumber_product_export());
                clinicStock.setQuantity(clinicStock.getQuantity() - dto.getNumber_product_export());
                productBatchRepository.save(productBatch);
                clinicStockRepository.save(clinicStock);

                Product products = productRepository.findById(productBatch.getProductId().getId()).get();
                List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

                for (ClinicStock entity2 : clinicStocks){
                    if(products.getId() == entity2.getProduct().getId()){
                        products.setQuantity(productStatisticsRepository.getSumProduct(productBatch.getProductId().getId()));
                    }
                    productRepository.save(products);
                }
            }
        }
        exportReceipt.setStatus(ReceiptStatus.ACTIVE);
        exportReceiptRepository.save(exportReceipt);

        setSumPrice(id);
        return true;
    }

    @Override
    public List<ExportReceiptDto> getListExportReceiptByClinicId(Integer id){
        List<ExportReceipt> exportReceipts = exportReceiptRepository.findAllExportReceiptByClinicId(id);
        List<ExportReceiptDto> exportReceiptDtos = new ArrayList<>();
        for (ExportReceipt entity:exportReceipts){
            setSumPrice(entity.getId());
            ExportReceiptDto dto = exportReceiptConvert.entityToDto(entity);
            exportReceiptDtos.add(dto);
        }
        return exportReceiptDtos;
    }

    @Override
    public ExportReceiptReponse getAllIExportReceipt(int pageNo, int pageSize, String sortBy, String sortDir) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize,sort);

        Page<ExportReceipt> importReceipts = exportReceiptRepository.findAll(pageable);
        List<ExportReceipt> listOfImport = importReceipts.getContent();

        List<ExportReceiptDto> dtoList = listOfImport.stream().map(post -> exportReceiptConvert.entityToDto(post)).collect(Collectors.toList());

        ExportReceiptReponse exportReceiptReponse = new ExportReceiptReponse();
        exportReceiptReponse.setData(dtoList);
        exportReceiptReponse.setCurrent(importReceipts.getNumber());
        exportReceiptReponse.setPageSize(importReceipts.getSize());
        exportReceiptReponse.setTotalElements(importReceipts.getTotalElements());
        exportReceiptReponse.setTotalPages(importReceipts.getTotalPages());
        exportReceiptReponse.setLast(importReceipts.isLast());

        return exportReceiptReponse;
    }

    @Override
    public ExportReceiptReponse getAllExportReceiptClinic(int pageNo, int pageSize, String sortBy, String sortDir, int id) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize,sort);
        //get list import product follow clinic
        List<ExportReceipt> importReceipts = exportReceiptRepository.findAllExportReceiptByClinicId(id);
        //convert Clinic
        int start = Math.min((int)pageable.getOffset(), importReceipts.size());
        int end = Math.min((start + pageable.getPageSize()), importReceipts.size());
        Page<ExportReceipt> importReceipt = new PageImpl<ExportReceipt>(importReceipts.subList(start, end), pageable, importReceipts.size());
        List<ExportReceipt> listOfImport = importReceipt.getContent();

        List<ExportReceiptDto> dtoList = listOfImport.stream().map(post -> exportReceiptConvert.entityToDto(post)).collect(Collectors.toList());

        ExportReceiptReponse exportReceiptReponse = new ExportReceiptReponse();
        exportReceiptReponse.setData(dtoList);
        exportReceiptReponse.setCurrent(importReceipt.getNumber());
        exportReceiptReponse.setPageSize(importReceipt.getSize());
        exportReceiptReponse.setTotalElements(importReceipt.getTotalElements());
        exportReceiptReponse.setTotalPages(importReceipt.getTotalPages());
        exportReceiptReponse.setLast(importReceipt.isLast());

        return exportReceiptReponse;

    }

}
