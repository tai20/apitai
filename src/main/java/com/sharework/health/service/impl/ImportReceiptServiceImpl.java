package com.sharework.health.service.impl;

import com.sharework.health.convert.ClinicStockConvert;
import com.sharework.health.convert.ImportReceiptConvert;
import com.sharework.health.convert.ImportReceiptDetailsConvert;
import com.sharework.health.dto.*;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.response.ImportRecepitReponse;
import com.sharework.health.response.ProductReponse;
import com.sharework.health.response.TreatmentPackageReponse;
import com.sharework.health.service.ImportReceiptService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class ImportReceiptServiceImpl implements ImportReceiptService {


    private ImportReceiptConvert importReceiptConvert;

    private ImportReceiptRepository importReceiptRepository;

    private ClinicRepository clinicRepository;

    private ClinicStockRepository clinicStockRepository;

    private SupplierRepository supplierRepository;

    private UserRepository userRepository;

    private ImportReceiptDetailsRepository importReceiptDetailsRepository;

    private ImportReceiptDetailsConvert importReceiptDetailsConvert;

    private ClinicStockConvert clinicStockConvert;

    private ProductBatchRepository productBatchRepository;

    private ProductRepository productRepository;

    private ProductStatisticsRepository productStatisticsRepository;

    public synchronized void setSumPrice(Integer receipt_id){

        BigDecimal sumPrice = importReceiptRepository.getSumPrice(receipt_id);
        Optional<ImportReceipt> importReceipt = importReceiptRepository.findById(receipt_id);
        importReceipt.get().setSumprice(sumPrice);
        importReceiptRepository.save(importReceipt.get());
        System.out.println(sumPrice);
    }

    @Override
    public List<ImportReceiptDto> findAll(){
        List<ImportReceipt> importReceipts = importReceiptRepository.findAllSortDateDESC();
        List<ImportReceiptDto> importReceiptDtos = new ArrayList<>();
        for (ImportReceipt entity:importReceipts){
                setSumPrice(entity.getId());
                ImportReceiptDto dto = importReceiptConvert.entityToDto(entity);
                importReceiptDtos.add(dto);
        }

        return importReceiptDtos;
    }

    //ham lay theo kho
    @Override
    public List<ImportReceiptDto> getAllImportReceiptClinicDetail(Integer clinic_id){
        System.out.println("So id cua kho la lân 2: " +clinic_id);//xuat ra man hinh kiem tra
        List<ImportReceipt> importReceipts = importReceiptRepository.findAllImportSortDateDESC(clinic_id);
        List<ImportReceiptDto> importReceiptDtos = new ArrayList<>();
        for (ImportReceipt entity:importReceipts){
            setSumPrice(entity.getId());
            ImportReceiptDto dto = importReceiptConvert.entityToDto(entity);
            importReceiptDtos.add(dto);
        }

        return importReceiptDtos;
    }



    @Override
    public ImportReceiptDto findById(Integer id){
        Optional<ImportReceipt> foundDto = importReceiptRepository.findById(id);
        if (foundDto.isPresent()){
            ImportReceipt entity = importReceiptRepository.findById(id).get();
            setSumPrice(entity.getId());
            return importReceiptConvert.entityToDto(entity);
        }
        return null;
    }


    @Override
    public boolean insert(ImportReceiptDto dto){
        return false;
    }



    @Override
    public ImportReceiptDto insertImportReceipt(ImportReceiptDto dto) {
        ImportReceipt entity = importReceiptConvert.dtoToEntity(dto);
        Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
        Supplier supplier = supplierRepository.findById(dto.getSupplierDto().getId()).get();
        User user = userRepository.findById(dto.getUserDto().getId()).get();

        entity.setId(null);
        if (clinic == null || user == null || supplier == null){
            return null;
        } else {
            entity.setClinic(clinic);
            entity.setUser(user);
            entity.setSupplier(supplier);
        }
        entity.setDateimport(LocalDateTime.now());
        entity.setSumprice(new BigDecimal(0));
        entity.setStatus(ReceiptStatus.ACTIVE);
        ImportReceipt result = importReceiptRepository.save(entity);
        return importReceiptConvert.entityToDto(result);
    }

    @Override
    public boolean update(Integer id, ImportReceiptDto importReceiptDto){
        return  false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }



    @Override
    public ImportReceiptDetailsDto insertImportReceiptDetail(ImportReceiptDetailsDto dto){

        ImportReceiptDetails entity = importReceiptDetailsConvert.dtoToEntity(dto);
        ImportReceipt importReceipt = importReceiptRepository.findById(dto.getImportReceiptDto().getId()).get();
        ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinicStockDto().getId()).get();
        ProductBatch productBatch = productBatchRepository.findById(dto.getProductBatchDto().getId()).get();
        ClinicStock clinicStockByClinicIdAndAndProductId = clinicStockRepository.getClinicStockByClinicIdAndAndProductId(clinicStock.getId(),productBatch.getProductId().getId());
        List<ImportReceiptDetails> importReceiptDetails = importReceiptDetailsRepository.getImportReceiptByClinicStockIdReceiptId(dto.getImportReceiptDto().getId(), clinicStockByClinicIdAndAndProductId.getId());
        if (importReceipt.getClinic().getId() != clinicStockByClinicIdAndAndProductId.getClinic().getId() || dto.getNumber_product_import()<=0 || dto.getImport_price().compareTo(new BigDecimal(0))==-1) return null;
        if (importReceipt == null || clinicStockByClinicIdAndAndProductId == null ||importReceipt.getStatus() != ReceiptStatus.ACTIVE || productBatch == null || clinicStockByClinicIdAndAndProductId.getQuantity()<0){
            return  null;
        } else
        if (importReceiptDetails.isEmpty()){
            entity.setNumber_product_import(dto.getNumber_product_import());
            entity.setImport_price(dto.getImport_price());
        } else{
            for(ImportReceiptDetails itemSearch:importReceiptDetails){
                entity.setNumber_product_import(dto.getNumber_product_import() + itemSearch.getNumber_product_import());
                BigDecimal total = itemSearch.getImport_price().add(dto.getImport_price());
                entity.setImport_price(total);
            }
        }

        clinicStockByClinicIdAndAndProductId.setQuantity(dto.getNumber_product_import() + clinicStockByClinicIdAndAndProductId.getQuantity());
        productBatch.setQuantity(dto.getNumber_product_import() + productBatch.getQuantity());
        entity.setImportReceipt(importReceipt);
        entity.setClinicStock(clinicStockByClinicIdAndAndProductId);

        entity.setProductBatchId(productBatch);
        productBatchRepository.save(productBatch);
        clinicStockRepository.save(clinicStockByClinicIdAndAndProductId);


        Product products = productRepository.findById(productBatch.getProductId().getId()).get();
        List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

        for (ClinicStock entity2 : clinicStocks){
            if(products.getId() == entity2.getProduct().getId()){
                products.setQuantity(productStatisticsRepository.getSumProduct(productBatch.getProductId().getId()));
            }
            productRepository.save(products);
        }

        ImportReceiptDetails result = importReceiptDetailsRepository.save(entity);
        setSumPrice(entity.getImportReceipt().getId());
        return importReceiptDetailsConvert.entityToDto(result);
    }


    @Override
    public List<ImportReceiptDetailsQueryDto> getAllImportReceiptDetailByReceiptid(Integer receipt_id){
        List<ImportReceiptDetailsQueryDto> importReceiptDetailsQueryDtos = importReceiptDetailsRepository.getAllImportReceiptDetailByReceiptid(receipt_id);
        return importReceiptDetailsQueryDtos;
    }


//    Chỉ chuyển trạng thái sang DELETED, không xóa detail của phiếu
    @Override
    public boolean deleteImportReceipt(Integer id){
        ImportReceipt importReceipt = importReceiptRepository.findById(id).get();

        if (importReceipt.getStatus()!=ReceiptStatus.ACTIVE ){
            return false;
        }

        List<ImportReceiptDetailsQueryDto> importReceiptDetailsQueryDtos = importReceiptDetailsRepository.getAllImportReceiptDetailByReceiptid(id);
        if (!importReceiptDetailsQueryDtos.isEmpty()) {

            for (ImportReceiptDetailsQueryDto dto : importReceiptDetailsQueryDtos) {
                ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();
                if(clinicStock.getQuantity()<dto.getNumber_product_import() ) return false;
                clinicStock.setQuantity(clinicStock.getQuantity() - dto.getNumber_product_import());
                clinicStockRepository.save(clinicStock);

                Product products = productRepository.findById(clinicStock.getProduct().getId()).get();
                List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

                for (ClinicStock entity2 : clinicStocks){
                    if(products.getId() == entity2.getProduct().getId()){
                        products.setQuantity(productStatisticsRepository.getSumProduct(clinicStock.getProduct().getId()));
                    }
                    productRepository.save(products);
                }
            }

        }

        importReceipt.setStatus(ReceiptStatus.DELETED);
        setSumPrice(id);
        importReceiptRepository.save(importReceipt);
        return true;
    }



    @Override
    public boolean deleteImportReceiptDetail(Integer receipt_id, Integer clinic_stock_id){
        ImportReceipt importReceipt = importReceiptRepository.findById(receipt_id).get();
        ClinicStock clinicStock = clinicStockRepository.findById(clinic_stock_id).get();


        if (importReceipt.getStatus()!=ReceiptStatus.ACTIVE){
            return false;
        }
        List<ImportReceiptDetails> importReceiptDetails = importReceiptDetailsRepository.getImportReceiptByClinicStockIdReceiptId(receipt_id, clinic_stock_id);
        if (importReceiptDetails.isEmpty()) {return false;}
        for(ImportReceiptDetails entity:importReceiptDetails){
            Integer idProductBatch = entity.getProductBatchId().getId();
            ProductBatch productBatch = productBatchRepository.findById(idProductBatch).get();
            productBatch.setQuantity(productBatch.getQuantity() - entity.getNumber_product_import());
            clinicStock.setQuantity(clinicStock.getQuantity() - entity.getNumber_product_import());
            productBatchRepository.save(productBatch);
            clinicStockRepository.save(clinicStock);

            Product products = productRepository.findById(productBatch.getProductId().getId()).get();
            List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

            for (ClinicStock entity2 : clinicStocks){
                if(products.getId() == entity2.getProduct().getId()){
                    products.setQuantity(productStatisticsRepository.getSumProduct(productBatch.getProductId().getId()));
                }
                productRepository.save(products);
            }

            importReceiptDetailsRepository.delete(entity);
        }
        setSumPrice(receipt_id);
        return true;

    }


    @Override
    public ImportReceiptDetailsQueryDto updateImportReceiptDetail(ImportReceiptDetailsQueryDto dto){
        ImportReceipt importReceipt = importReceiptRepository.findById(dto.getReceipt_id()).get();
        List<ImportReceiptDetails> importReceiptDetails = importReceiptDetailsRepository.getImportReceiptByClinicStockIdReceiptId(dto.getReceipt_id(), dto.getClinic_stock_id());
        ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();
        ProductBatch productBatch = productBatchRepository.findById(dto.getProduct_batch_id()).get();

        if (importReceipt.getStatus()!=ReceiptStatus.ACTIVE){
            return null;
        }

        for(ImportReceiptDetails entity : importReceiptDetails){

            int quantityChange = dto.getNumber_product_import() - entity.getNumber_product_import(); // 10 - 20
            clinicStock.setQuantity(clinicStock.getQuantity() + quantityChange);

            if ((productBatch.getQuantity() + quantityChange) < 0) {
                return null;
            }
            productBatch.setQuantity(productBatch.getQuantity() + quantityChange);
            clinicStockRepository.save(clinicStock);
            productBatchRepository.save(productBatch);
            //Lưu lại phiếu
            entity.setNumber_product_import(dto.getNumber_product_import());
            entity.setImport_price(dto.getImport_price());

            Product products = productRepository.findById(productBatch.getProductId().getId()).get();
            List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

            for (ClinicStock entity2 : clinicStocks){
                if(products.getId() == entity2.getProduct().getId()){
                    products.setQuantity(productStatisticsRepository.getSumProduct(productBatch.getProductId().getId()));
                }
                productRepository.save(products);
            }

            importReceiptDetailsRepository.save(entity);

        }

        setSumPrice(importReceipt.getId());
        return dto;
    }

    @Override
    public boolean restoreImportReceipt(Integer id){
        ImportReceipt importReceipt = importReceiptRepository.findById(id).get();

        if (importReceipt.getStatus()!=ReceiptStatus.DELETED){
            return false;
        }
        List<ImportReceiptDetailsQueryDto> importReceiptDetailsQueryDtos = importReceiptDetailsRepository.getAllImportReceiptDetailByReceiptid(id);
        if (!importReceiptDetailsQueryDtos.isEmpty()) {
            for (ImportReceiptDetailsQueryDto dto : importReceiptDetailsQueryDtos) {
                ProductBatch productBatch = productBatchRepository.findById(dto.getProduct_batch_id()).get();
                ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();
                clinicStock.setQuantity(clinicStock.getQuantity() + dto.getNumber_product_import());
                productBatch.setQuantity(productBatch.getQuantity() + dto.getNumber_product_import());
                productBatchRepository.save(productBatch);
                clinicStockRepository.save(clinicStock);

                Product products = productRepository.findById(productBatch.getProductId().getId()).get();
                List<ClinicStock> clinicStocks = clinicStockRepository.findAll();

                for (ClinicStock entity2 : clinicStocks){
                    if(products.getId() == entity2.getProduct().getId()){
                        products.setQuantity(productStatisticsRepository.getSumProduct(productBatch.getProductId().getId()));
                    }
                    productRepository.save(products);
                }
            }
        }
        importReceipt.setStatus(ReceiptStatus.ACTIVE);
        importReceiptRepository.save(importReceipt);
        setSumPrice(id);
        return true;
    }

    @Override
    public boolean lockImportReceipt(Integer id){
        ImportReceipt importReceipt = importReceiptRepository.findById(id).get();
        if (importReceipt.getStatus() !=ReceiptStatus.ACTIVE){
            return false;
        }
        importReceipt.setStatus(ReceiptStatus.LOCKED);
        importReceiptRepository.save(importReceipt);
        return true;
    }

    @Override
    public List<ImportReceiptDto> findAllByIdClinic(Integer id) {
        List<ImportReceipt> importReceipts = importReceiptRepository.findAllByClinicSortDateDESC(id);
        List<ImportReceiptDto> importReceiptDtos = new ArrayList<>();
        for (ImportReceipt entity:importReceipts){
            setSumPrice(entity.getId());
            ImportReceiptDto dto = importReceiptConvert.entityToDto(entity);
            importReceiptDtos.add(dto);
        }

        return importReceiptDtos;
    }

    @Override
    public ImportRecepitReponse getAllImportRecepit(int pageNo, int pageSize, String sortBy, String sortDir) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize,sort);

        Page<ImportReceipt> importReceipts = importReceiptRepository.findAll(pageable);
        List<ImportReceipt> listOfImport = importReceipts.getContent();

        List<ImportReceiptDto> dtoList = listOfImport.stream().map(post -> importReceiptConvert.entityToDto(post)).collect(Collectors.toList());

        ImportRecepitReponse importRecepitReponse = new ImportRecepitReponse();
        importRecepitReponse.setData(dtoList);
        importRecepitReponse.setCurrent(importReceipts.getNumber());
        importRecepitReponse.setPageSize(importReceipts.getSize());
        importRecepitReponse.setTotalElements(importReceipts.getTotalElements());
        importRecepitReponse.setTotalPages(importReceipts.getTotalPages());
        importRecepitReponse.setLast(importReceipts.isLast());

        return importRecepitReponse;
    }

    @Override
    public ImportRecepitReponse getAllImportRecepitClinic(int pageNo, int pageSize, String sortBy, String sortDir, int id) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize,sort);
        //get list import product follow clinic
        List<ImportReceipt> importReceipts = importReceiptRepository.findAllImportSortDateDESC(id);
        //convert Clinic
        int start = Math.min((int)pageable.getOffset(), importReceipts.size());
        int end = Math.min((start + pageable.getPageSize()), importReceipts.size());
        Page<ImportReceipt> importReceipt = new PageImpl<ImportReceipt>(importReceipts.subList(start, end), pageable, importReceipts.size());
        List<ImportReceipt> listOfImport = importReceipt.getContent();

        List<ImportReceiptDto> dtoList = listOfImport.stream().map(post -> importReceiptConvert.entityToDto(post)).collect(Collectors.toList());

        ImportRecepitReponse importRecepitReponse = new ImportRecepitReponse();
        importRecepitReponse.setData(dtoList);
        importRecepitReponse.setCurrent(importReceipt.getNumber());
        importRecepitReponse.setPageSize(importReceipt.getSize());
        importRecepitReponse.setTotalElements(importReceipt.getTotalElements());
        importRecepitReponse.setTotalPages(importReceipt.getTotalPages());
        importRecepitReponse.setLast(importReceipt.isLast());

        return importRecepitReponse;

    }
}








