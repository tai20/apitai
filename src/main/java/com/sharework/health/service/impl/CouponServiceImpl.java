package com.sharework.health.service.impl;

import com.sharework.health.convert.CouponConvert;
import com.sharework.health.convert.CustomerCouponConvert;
import com.sharework.health.dto.CouponDto;
import com.sharework.health.dto.CustomerCouponDto;
import com.sharework.health.entity.*;
import com.sharework.health.helper.InitValue;
import com.sharework.health.repository.*;
import com.sharework.health.service.CouponService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class CouponServiceImpl implements CouponService {

    private CouponRepository couponRepository;

    private CouponConvert couponConvert;

    private UserRepository userRepository;

    private ServiceCategoryRepository serviceCategoryRepository;

    //private TreatmentCouponRepository treatmentCouponRepository;

    private CustomerCouponRepository customerCouponRepository;

    private CustomerRepository customerRepository;

    private CustomerCouponConvert customerCouponConvert;

    @Override
    public List<CouponDto> findAll() {
        List<Coupon> coupons = couponRepository.findAll();
        List<CouponDto> couponDtos = new ArrayList<>();
        for (Coupon entity: coupons
             ) {
            if (entity.getEndDate().compareTo(LocalDate.now()) < 0){
                couponRepository.delete(entity);
            }else {
                CouponDto dto = couponConvert.entityToDto(entity);
                couponDtos.add(dto);
            }
        }
        return couponDtos;
    }

    @Override
    public CouponDto findById(Integer id) {
        Coupon entity = couponRepository.findById(id).get();
        if (entity == null){
            return null;
        }
        return couponConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(CouponDto dto) {
        if (dto == null) {
            return false;
        }
        if(dto.getStartDate().isAfter(dto.getEndDate()) || ( dto.getQuantity() < 0 )){
            return false;
        }
        if (dto.getStartDate() == null){
            return false;
        }
        if (dto.getEndDate() == null){
            return false;
        }
        ServiceCategory serviceCategory = serviceCategoryRepository.findById(dto.getServiceCategoryDto().getId()).get();
        if (serviceCategory == null){
            return false;
        }
        try {
            InitValue initValue = new InitValue();
            Coupon coupon = couponConvert.dtoToEntity(dto);
            coupon.setId(null);
            coupon.setCode(initValue.generateCouponCode());
            coupon.setServiceCategory(serviceCategory);
            couponRepository.save(coupon);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Integer id, CouponDto dto) {
        Coupon entity = couponRepository.findById(id).get();

        if (entity == null) {
            return false;
        }
        try {
            entity = couponConvert.dtoToEntity(dto);
            couponRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }

    @Override
    public boolean delete(Integer id) {
        couponRepository.deleteById(id);
        return true;
    }

    @Override
    public CouponDto searchCouponByName(String name) {
        Coupon entity = couponRepository.findByName(name);
        if (entity == null){
            return null;
        }
        return couponConvert.entityToDto(entity);
    }

    @Override
    public CouponDto checkCouponByCouponCode(String code) {
        Coupon entity = couponRepository.findByCode(code);
        if (entity == null){
            return null;
        }
        return couponConvert.entityToDto(entity);
    }

    @Override
    public boolean addCouponForServiceCategory(Integer couponId, Integer serviceCategoryId) {
        Coupon coupon = couponRepository.findById(couponId).get();
        ServiceCategory serviceCategory = serviceCategoryRepository.findById(serviceCategoryId).get();

        if (coupon == null){
            return false;
        }
        if (serviceCategory == null){
            return false;
        }
//
//            TreatmentCoupon treatmentCoupon = new TreatmentCoupon()
//                    .setCoupon(coupon)
//                    .setServiceCategory(serviceCategory);
//
//            treatmentCouponRepository.save(treatmentCoupon);
            return true;

    }

    @Override
    public boolean addCouponForCustomer(Integer couponId, Integer customerId) {
        Coupon coupon = couponRepository.findById(couponId).get();
        Customer customer = customerRepository.findById(customerId).get();
        if (coupon == null){
            return false;
        }
        if (customer == null){
            return false;
        }
        CustomerCoupon customerCoupon = new CustomerCoupon()
                .setCoupon(coupon)
                .setCustomer(customer)
                .setStatus(Status.ACTIVE);
                ;
        customerCouponRepository.save(customerCoupon);
        return true;
    }

    @Override
    public List<CouponDto> getCouponsByCustomer(Integer customerId) {
        List<Coupon> coupons = couponRepository.findByCustomer(customerId);
        List<CouponDto> couponDtos = new ArrayList<>();
        for (Coupon entity: coupons
             ) {
            CouponDto dto = couponConvert.entityToDto(entity);
            couponDtos.add(dto);
        }
        return couponDtos;
    }

    @Override
    public List<CustomerCouponDto> findAllByCustomer(Integer customerId) {
        List<CustomerCoupon> customerCoupons = customerCouponRepository.findAllByCustomerId(customerId);
        List<CustomerCouponDto> customerCouponDtos = new ArrayList<>();
        for (CustomerCoupon entity: customerCoupons
        ) {
            if(entity.getCoupon().getQuantity()>0){
                CustomerCouponDto dto = customerCouponConvert.entityToDto(entity);
                customerCouponDtos.add(dto);
            }
        }
        return customerCouponDtos;
    }

    @Override
    public List<CustomerCouponDto> findAllByCoupon(Integer couponId) {
        List<CustomerCoupon> customerCoupons = customerCouponRepository.findAllByCouponId(couponId);
        List<CustomerCouponDto> customerCouponDtos = new ArrayList<>();
        for (CustomerCoupon entity: customerCoupons
        ) {
            CustomerCouponDto dto = customerCouponConvert.entityToDto(entity);
            customerCouponDtos.add(dto);
        }
        return customerCouponDtos;
    }
}
