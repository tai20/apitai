package com.sharework.health.service.impl;

import com.sharework.health.convert.ServiceListConvert;
import com.sharework.health.dto.ServiceListDto;
import com.sharework.health.entity.ServiceList;
import com.sharework.health.repository.ServiceListRepository;
import com.sharework.health.service.ServiceListService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(rollbackOn = Exception.class)
@AllArgsConstructor
public class ServiceListServiceImpl implements ServiceListService {
    private ServiceListRepository serviceListRepository;

    private ServiceListConvert serviceListConvert;

    @Override
    public List<ServiceListDto> findAll() {
        return null;
    }

    @Override
    public ServiceListDto findById(Integer id) {
        return null;
    }

    @Override
    public boolean insert(ServiceListDto dto) {
        return false;
    }

    @Override
    public boolean update(Integer id, ServiceListDto dto) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public List<ServiceListDto> findAllServiceListByCustomer(Integer customerId) {
        List<ServiceList> serviceLists = serviceListRepository.findAllServiceListByCustomerId(customerId);
        List<ServiceListDto> serviceListDtos = new ArrayList<>();

        for(ServiceList entity: serviceLists){
            ServiceListDto dto = serviceListConvert.entityToDto(entity);
            serviceListDtos.add(dto);
        }
        return serviceListDtos;
    }
}
