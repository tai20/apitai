package com.sharework.health.service;

import com.sharework.health.dto.SlipUseDetailDto;
import com.sharework.health.dto.SlipUseDto;
import com.sharework.health.dto.SlipUseQuery;
import com.sharework.health.entity.SlipUse;
import com.sharework.health.entity.SlipUseDetail;
import com.sharework.health.response.SlipUseReponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface SlipUseService extends BaseService<SlipUseDto, Integer> {

    List<SlipUseDto> findAllByCustomer(Integer customerId);

    List<SlipUseDto> findAllByTreatmentPackage(String treatmentPackageName);

    List<SlipUseDetailDto> getAllSlipUseDetailBySlipUse(Integer slipUseId);

    List<SlipUseDetailDto> getAllSlipUserDetailByCustomerId(Integer customerId);

    boolean uploadImageBeforeSlipUse(Integer slipUseId, MultipartFile imageBefore) throws IOException;

    boolean uploadImageAfterSlipUse(Integer slipUseId, MultipartFile imageAfter) throws IOException;

    boolean activeSlipUse(Integer slipUseId);

    List<SlipUseDto> findAllnew();

    List<SlipUse> getslipuseByCum(Integer id);

    SlipUseReponse getAllSlipUse(int pageNo, int pageSize, String sortBy, String sortDir);

    void mergeNameSlipUse();

    SlipUseReponse findOfSlipUse(int pageNo, int pageSize, String sortBy, String sortDir, String name);
}
