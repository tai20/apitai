package com.sharework.health.service;

import com.sharework.health.dto.ClinicStockDto;
import com.sharework.health.response.ClinicStockReponse;

import java.security.Principal;
import java.util.List;

public interface ClinicStockService extends BaseService<ClinicStockDto, Integer>{
    public List<ClinicStockDto> getClinicStockByClinicId(Integer id);
   public List<ClinicStockDto> getClinicStockByClinicIdOfUsers(Integer id);

    List<ClinicStockDto> getClinicStockByProductName(Principal principal, String name);

    ClinicStockReponse getAllClinicStock(int pageNo, int pageSize, String sortBy, String sortDir);
    ClinicStockReponse getAllClinicStockClinic(int pageNo, int pageSize, String sortBy, String sortDir, int id);
    ClinicStockReponse getClinicStockByProductNamePage(Principal principal, int pageNo, int pageSize, String sortBy, String sortDir, String name);
    ClinicStockReponse getClinicStockByCategoryNamePage(Principal principal, int pageNo, int pageSize, String sortBy, String sortDir, String productCategoryDto);

}
