package com.sharework.health.service;

import com.sharework.health.dto.ProductBatchDto;
import com.sharework.health.dto.QueryProductBatch;
import com.sharework.health.response.ProductBatchReponse;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface ProductBatchService extends BaseService<ProductBatchDto, Integer> {

    List<ProductBatchDto> findProductBatchIdByIdProduct(Integer id);

    List<ProductBatchDto> findProductByExpiry(LocalDate expiryStart, LocalDate expiryEnd);

    List<ProductBatchDto> findByLikeName(String name);

    List<ProductBatchDto> findByLikeNameAndExpiry(String name, LocalDate expiryStart, LocalDate expiryEnd);

    ProductBatchReponse getAllProductBatch(int pageNo, int pageSize, String sortBy, String sortDir);

    ProductBatchReponse getAllProductBatchByName(int pageNo, int pageSize, String sortBy, String sortDir,String name);

    ProductBatchReponse getAllProductBatchByExpiry(int pageNo, int pageSize, String sortBy, String sortDir, String start, String expiry);
}
