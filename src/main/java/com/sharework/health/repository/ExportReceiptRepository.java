package com.sharework.health.repository;

import com.sharework.health.entity.ExportReceipt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface ExportReceiptRepository extends JpaRepository<ExportReceipt, Integer> {

    @Query(value = "select sum(exportreceiptdetails.number_product_export*exportreceiptdetails.export_price) from exportreceiptdetails where exportreceiptdetails.receipt_id= :receipt_id", nativeQuery = true)
    BigDecimal getSumPrice(Integer receipt_id);

    @Query(nativeQuery = true, value = "select er.* from exportreceipt er order by er.dateexport DESC ")
    List<ExportReceipt> findAllSortDateDESC();

    @Query(nativeQuery = true, value = "select * from exportreceipt where clinic_id= :id ")
    List<ExportReceipt> findAllExportReceiptByClinicId(Integer id);
}
