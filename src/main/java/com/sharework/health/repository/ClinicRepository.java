package com.sharework.health.repository;

import com.sharework.health.entity.Clinic;
import com.sharework.health.entity.ClinicStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClinicRepository extends JpaRepository<Clinic, Integer> {

    Clinic findByName(String name);

    List<Clinic> findByNameContaining(String name);

    @Query(nativeQuery = true, value="select  * from clinic where id = :id")
    Clinic getClinicByClinicId(Integer id);



}
