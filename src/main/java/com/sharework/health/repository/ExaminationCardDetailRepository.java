package com.sharework.health.repository;

import com.sharework.health.dto.ExaminationCardDetailQueryDto;
import com.sharework.health.dto.SelectCountExaminationUserDto;
import com.sharework.health.entity.ExaminationCardDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExaminationCardDetailRepository extends JpaRepository<ExaminationCardDetail, Integer> {

    @Query(nativeQuery = true, value = "select ed.* from examinationcard e join examinationcarddetail ed on e.id = ed.examinationcard_id where e.id= :examinationCardId")
    List<ExaminationCardDetail> findAllByExaminationCard(Integer examinationCardId);

    @Query(nativeQuery = true, value = "select * from examinationcarddetail where examinationcard_id= :examinationCardId and service_id= :serviceId and user_id= :userId")
    ExaminationCardDetail findAllByExaminationCardServiceUser(Integer examinationCardId, Integer serviceId, Integer userId);

    @Query(nativeQuery = true, value = "select * from examinationcarddetail where examinationcard_id= :examinationCardId and service_id= :serviceId and status like 'Examined'")
    List<ExaminationCardDetail> findAllByExaminationCardServiceStatusExamined(Integer examinationCardId, Integer serviceId);

//    @Query(nativeQuery = true, value = "select * from examinationcarddetail ed join users u on ed.user_id = u.id where u.id= :userId and ed.status like 'Examining'")
//    List<ExaminationCardDetailQueryDto> findAllByUser(Integer userId);

    @Query("select new com.sharework.health.dto.ExaminationCardDetailQueryDto(e.id, e.dateOfExamination, u.name, ser.id, ser.name,c.name, ed.status, ed.timeUse) from ExaminationCardDetail ed " +
            "join User u on ed.user.id = u.id join ExaminationCard e on e.id = ed.examinationCard.id " +
            "join SlipUse s on s.id = e.slipUse.id join Customer c on c.id = s.customer.id join Service ser on ser.id = ed.service.id where u.id= :userId and ed.status = 'Examining' and e.status = 'ACTIVE'")
    List<ExaminationCardDetailQueryDto> findAllByUser(Integer userId);

     @Query("SELECT distinct new com.sharework.health.dto.SelectCountExaminationUserDto(u.id, count(e.id)) from ExaminationCard e join ExaminationCardDetail ed on " +
             "e.id = ed.examinationCard.id join User u on ed.user.id = u.id join Role r on u.role.id = r.id" +
            " where MONTH(e.dateOfExamination) = :month and YEAR(e.dateOfExamination) = :year and r.name = 'ROLE_DOCTOR' and ed.status = 'Examined' group by u.id having count(e.id) > 0")
     List<SelectCountExaminationUserDto> findAllUserWithRoleDoctorAndCountExaminationByMonthAndYear(Integer month, Integer year);

    @Query("SELECT distinct new com.sharework.health.dto.SelectCountExaminationUserDto(u.id, count(e.id)) from ExaminationCard e join ExaminationCardDetail ed on " +
            "e.id = ed.examinationCard.id join User u on ed.user.id = u.id join Role r on u.role.id = r.id" +
            " where MONTH(e.dateOfExamination) = :month and YEAR(e.dateOfExamination) = :year and r.name = 'ROLE_TECHNICIAN' and ed.status = 'Examined' group by u.id having count(e.id) > 0")
    List<SelectCountExaminationUserDto> findAllUserWithRoleTechnicianAndCountExaminationByMonthAndYear(Integer month, Integer year);


}
