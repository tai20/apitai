package com.sharework.health.repository;

import com.sharework.health.dto.TransferReceiptDto;
import com.sharework.health.entity.TransferReceipt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransferReceiptRepository extends JpaRepository<TransferReceipt, Integer> {

    @Query(nativeQuery = true, value = "select tr.* from transferreceipt tr order by tr.dateexport DESC")
    List<TransferReceipt> findAllSortDateDESC();

    @Query(nativeQuery = true, value = "select tr.* from transferreceipt tr where tr.sending_warehouse_id=:id order by tr.dateexport DESC")
    List<TransferReceipt> findAllByClinicIdSortDateDESC(Integer id);

    @Query(nativeQuery = true, value = "select * from transferreceipt where id=:id and user_id=:user_id")
    TransferReceiptDto findByIdAndRole(Integer id, Integer user_id);

    //lay thong tin chuyen kho di và kho den theo chi nhanh
    @Query(nativeQuery = true, value = "select tr.* from transferreceipt tr where tr.receiving_warehouse_id= :id or tr.sending_warehouse_id= :id order by tr.dateexport DESC")
    List<TransferReceipt> findAllTransferreceiptByClinicIdSortDateDESC(Integer id);
}
