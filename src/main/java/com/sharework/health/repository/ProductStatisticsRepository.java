package com.sharework.health.repository;


import com.sharework.health.dto.*;
import com.sharework.health.entity.ClinicStock;
import com.sharework.health.entity.ImportReceiptDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ProductStatisticsRepository extends  JpaRepository<ClinicStock, Integer>{

//    Thống kê sản phẩm còn lại
    @Query("select new com.sharework.health.dto.RemainingProductStatisticsQueryDto(cs.product.id, cs.product.name, cs.product.productCategory.name, cs.product.price, cs.product.wholesalePrice, cs.unit, cs.clinic.name, cs.quantity) " +
            "from ClinicStock cs  where  cs.product.status='ACTIVE' order by cs.product.id ASC , cs.clinic.id ASC ")
    List<RemainingProductStatisticsQueryDto> getClinicStockProduct();

    @Query( nativeQuery = true, value = "select sum(quantity) from clinicstock where product_id= :product_id")
    Integer getSumProduct(Integer product_id);


    //Thống kê tất cả sản phẩm nhập
    @Query("select new com.sharework.health.dto.StatisticsImportedProductsQueryDto(ird.clinicStock.product.id, ird.clinicStock.product.name, ird.clinicStock.product.productCategory.name, ird.clinicStock.product.wholesalePrice, ird.clinicStock.unit, ird.number_product_import, ird.importReceipt.clinic.id, ird.importReceipt.clinic.name) " +
            "from ImportReceiptDetails ird where ird.importReceipt.dateimport between :from and :to and ird.importReceipt.status<>'DELETED'")
    List<StatisticsImportedProductsQueryDto> getAllImportReceiptDetails(LocalDateTime from, LocalDateTime to);

    //Thống kê tất cả sản phẩm xuất
    @Query("select new com.sharework.health.dto.StatisticsExportedProductsQueryDto(erd.clinicStock.product.id, erd.clinicStock.product.name, erd.clinicStock.product.productCategory.name, erd.clinicStock.product.price, erd.clinicStock.unit, erd.number_product_export, erd.exportReceipt.clinic.id, erd.exportReceipt.clinic.name) " +
            "from ExportReceiptDetails erd where erd.exportReceipt.dateexport between :from and :to and erd.exportReceipt.status<>'DELETED'")
    List<StatisticsExportedProductsQueryDto> getAllExportReceiptDetails(LocalDateTime from, LocalDateTime to);

    @Query("select  new com.sharework.health.dto.StatisticsOtherProductsQueryDto(ord.clinicStock.product.id, ord.clinicStock.product.name,ord.clinicStock.product.productCategory.name, ord.clinicStock.unit, ord.number_product,ord.othersWarehouseReceipt.clinic.id, ord.othersWarehouseReceipt.clinic.name, ord.othersWarehouseReceipt.type) " +
            "from OthersWarehouseReceiptDetails ord where ord.othersWarehouseReceipt.datecreate between :from and :to and ord.othersWarehouseReceipt.status<>'DELETED'")
    List<StatisticsOtherProductsQueryDto> getAllOtherReceiptDetails(LocalDateTime from, LocalDateTime to);

    //TÍNH TỒN ĐẦU KỲ CUỐI KỲ
    //Lấy tất cả sản phẩm nhập kho ở kho cụ thể từ ngày hiện tại trở về ngày from
    @Query("select sum(ird.number_product_import) from ImportReceiptDetails ird " +
            "where ird.clinicStock.product.id= :product_id " +
            "and ird.importReceipt.dateimport between :from and :to and ird.importReceipt.status<>'DELETED' " +
            "and (:clinic_id is null or ird.importReceipt.clinic.id= :clinic_id)")
    Integer getAllProductImportByClinic(Integer clinic_id, Integer product_id, LocalDateTime from, LocalDateTime to);

    // Lấy tất cả sản phẩm xuất kho ở kho cụ thể từ ngày hiện tại trở về ngày from
    @Query("select sum(erd.number_product_export) from ExportReceiptDetails erd " +
            "where erd.clinicStock.product.id= :product_id " +
            "and erd.exportReceipt.dateexport between :from and :to and erd.exportReceipt.status<>'DELETED' " +
            "and (:clinic_id is null or erd.exportReceipt.clinic.id= :clinic_id)")
    Integer getAllProductExportByClinic(Integer clinic_id, Integer product_id, LocalDateTime from, LocalDateTime to);

    //Lấy tất cả sản phẩm nhập/xuất kho khác từ ngày hiện tại trở về ngày from
    @Query("select sum(owrd.number_product) from OthersWarehouseReceiptDetails owrd " +
            "where owrd.clinicStock.product.id= :product_id " +
            "and owrd.othersWarehouseReceipt.datecreate between :from and :to " +
            "and owrd.othersWarehouseReceipt.status<>'DELETED' " +
            "and (:clinic_id is null or owrd.othersWarehouseReceipt.clinic.id= :clinic_id) " +
            "and owrd.othersWarehouseReceipt.type= :type")
    Integer getAllProductImportOtherByClinic(Integer clinic_id, Boolean type, Integer product_id, LocalDateTime from, LocalDateTime to);


}
