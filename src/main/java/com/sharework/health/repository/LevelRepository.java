package com.sharework.health.repository;

import com.sharework.health.entity.Level;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface LevelRepository extends JpaRepository<Level, Integer> {
    @Query(nativeQuery = true, value = "select * from level")
    List<Level> findAll();

    @Query(nativeQuery = true, value ="select lv FROM level lv where :total  between lv.min_value and lv.max_value")
    Level findNameCustomerLever(BigDecimal total);

//    Level findByMin_valueIsGreaterThanEqualAndMax_valueIsLessThanEqual(BigDecimal min_value, BigDecimal max_value);
}