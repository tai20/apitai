package com.sharework.health.repository;

import com.sharework.health.dto.ExportReceiptDetailsQueryDto;
import com.sharework.health.entity.ExportReceiptDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExportReceiptDetailsRepository extends JpaRepository<ExportReceiptDetails, Integer> {

    @Query("select new com.sharework.health.dto.ExportReceiptDetailsQueryDto(erd.exportReceipt.id, erd.clinicStock.id, erd.productBatchId.id, erd.productBatchId.name,erd.number_product_export, cs.product.id, erd.productBatchId.productId.name, erd.export_price) " +
            "from ExportReceiptDetails erd join ClinicStock cs on cs.id=erd.clinicStock.id " +
            "join Product p on p.id=cs.product.id where erd.exportReceipt.id= :receipt_id")
    List<ExportReceiptDetailsQueryDto> getAllExportReceiptDetailByReceiptid(Integer receipt_id);

    @Query(nativeQuery = true, value = "select erd.* from exportreceiptdetails erd " +
            "where erd.receipt_id= :receipt_id and erd.clinic_stock_id= :clinic_stock_id")
    List<ExportReceiptDetails> getExportReceiptByClinicStockIdReceiptId(Integer receipt_id, Integer clinic_stock_id);



}
