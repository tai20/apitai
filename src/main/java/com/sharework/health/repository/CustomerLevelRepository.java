package com.sharework.health.repository;

import com.sharework.health.entity.Customer;
import com.sharework.health.entity.CustomerLevel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerLevelRepository extends JpaRepository<CustomerLevel, Customer> {

    @Query(nativeQuery = true, value="select * from customerlevel where customer_id= :customer_id")
    CustomerLevel findCustomerlevelById(Integer customer_id);

    @Query(nativeQuery = true, value="select * from customerlevel where customer_id= :customer_id")
    CustomerLevel findCustomerByUID(Integer customer_id);

    List<CustomerLevel> findByCustomer_IdEquals(Integer id);

    @Query(nativeQuery = true, value="SELECT cl.* FROM public.customerlevel AS cl LEFT JOIN public.customer AS c ON cl.customer_id = c.id " +
            " WHERE c.phonenumber like %:phonenumber%")
    Page<CustomerLevel> findAllByPhone(Pageable pageable, String phonenumber);

    @Query(nativeQuery = true, value="SELECT cl.* FROM public.customerlevel AS cl LEFT JOIN public.customer AS c ON cl.customer_id = c.id " +
            " WHERE c.name ilike %:nameCustomer% ")
    Page<CustomerLevel> findAllByName(Pageable pageable, String nameCustomer);

    @Query(nativeQuery = true, value="SELECT cl.* FROM public.customerlevel AS cl LEFT JOIN public.customer AS c ON cl.customer_id = c.id WHERE c.typecustomer = :type")
    Page<CustomerLevel> findByType(Pageable pageable, String type);

    @Query(nativeQuery = true, value="SELECT cl.* FROM public.customerlevel AS cl LEFT JOIN public.customer AS c ON cl.customer_id = c.id " +
            " WHERE c.typecustomer = :type and c.name ilike %:nameCustomer% ")
    Page<CustomerLevel> findByName(Pageable pageable, String type, String nameCustomer);

    @Query(nativeQuery = true, value="SELECT cl.* FROM public.customerlevel AS cl LEFT JOIN public.customer AS c ON cl.customer_id = c.id " +
            " WHERE c.typecustomer = :type and c.phonenumber like %:phonenumber%")
    Page<CustomerLevel> findByPhone(Pageable pageable, String type, String phonenumber);

}
