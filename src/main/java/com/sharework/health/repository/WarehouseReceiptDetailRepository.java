package com.sharework.health.repository;

import com.sharework.health.entity.Product;
import com.sharework.health.entity.WarehouseReceipt;
import com.sharework.health.entity.WarehouseReceiptDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface WarehouseReceiptDetailRepository extends JpaRepository<WarehouseReceiptDetail, Integer> {

    @Query(nativeQuery = true, value = "select * from warehousereceipt w join warehousereceiptdetail wd on w.id = wd.receipt_id where w.id= :id")
    List<WarehouseReceiptDetail> findAllByWarehouseReceipt(Integer id);

    @Query(nativeQuery = true, value = "update warehousereceiptdetail set numberproductimport = :num where product_id = :pid and receipt_id = :rid ")
    void updateNew(Integer num,Integer pid,Integer rid);

    @Transactional
    @Modifying
    @Query("update WarehouseReceiptDetail w set w.numberProductImport = ?1 " +
            "where w.product = ?2 and w.warehouseReceipt = ?3")
    void updateNumberProductImportByProductEqualsAndWarehouseReceiptEquals(int numberProductImport, Product product, WarehouseReceipt warehouseReceipt);


}
