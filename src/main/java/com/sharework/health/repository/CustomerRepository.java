package com.sharework.health.repository;

import com.sharework.health.entity.Customer;
import com.sharework.health.entity.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    /**
     * Find customer exactly
     *
     * @param phoneNumber
     * @return Customer
     */
    Customer findByPhoneNumber(String phoneNumber);

    Customer findByName(String name);

    List<Customer> findByNameLike(String name);

    Customer findByEmail(String email);

    Customer findByCmnd(String cmnd);

    @Query(nativeQuery = true, value = "select * from customer where customer_category_id=1 and createddate between :startDate and :endDate")
    List<Customer> statisticalCustomerByCreatedDateIsBetween(LocalDateTime startDate, LocalDateTime endDate);

    @Query(nativeQuery = true, value = "select c.* from customer c join customercategory cc on c.customer_category_id = cc.id where cc= :customerCategoryId")
    List<Customer> findAllByCustomerCategory(Integer customerCategoryId);

    @Query("select c from Customer c where MONTH(birthdate)= :month")
    List<Customer> findAllByBirthDateWithMonth(Integer month);

    @Query("select  c from Customer c where (MONTH(c.birthDate)= :month and DAY (c.birthDate)= :day)")
    List<Customer> findAllByBirthDay(@Param("month") Integer month , @Param("day") Integer day );

    @Query("select  c from Customer c , ScheduleExamination s where c.id = s.customer.id and s.dateOfExamination = :time")
    List<Customer> findAllByAppointments(@Param("time") LocalDate time );

    @Query(nativeQuery = true, value = "select * from customer order by id desc limit 1")
    Customer findCustomerAfterInsert();

    Customer findBySlipUses_IdEquals(Integer id);

}
