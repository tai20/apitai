package com.sharework.health.repository;

import com.sharework.health.dto.ProductBatchDto;
import com.sharework.health.dto.QueryProductBatch;
import com.sharework.health.entity.ProductBatch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ProductBatchRepository extends JpaRepository<ProductBatch, Integer> {

    @Query(nativeQuery = true, value = "SELECT * FROM productbatch where productid = :id")
    List<ProductBatch> findProductBatchIdByIdProduct(Integer id);

    @Query(nativeQuery = true, value = "select * from productbatch where expiry between :expiryStart and :expiryEnd")
    List<ProductBatch> findProductByExpiry(LocalDate expiryStart, LocalDate expiryEnd);

    @Query(nativeQuery = true, value = "SELECT * FROM productbatch WHERE name ILIKE %:name%")
    List<ProductBatch> findByLikeName(String name);

    @Query(nativeQuery = true, value = "SELECT * FROM productbatch WHERE name LIKE %:name% and expiry between :expiryStart and :expiryEnd")
    List<ProductBatch> findByLikeNameAndExpiry(String name, LocalDate expiryStart, LocalDate expiryEnd);

    @Query("select new com.sharework.health.dto.QueryProductBatch(pb.id, pb.name, pb.quantity, pb.expiry, p.name) from ProductBatch pb join Product p on pb.productId.id = p.id ORDER BY pb.id ASC ")
    Page<QueryProductBatch> getAllProductBatchPaging(Pageable pageable);

//    @Query(nativeQuery = true, value = "SELECT * FROM productbatch WHERE name ilike %:name%")
    @Query("SELECT new com.sharework.health.dto.QueryProductBatch(pb.id, pb.name, pb.quantity, pb.expiry, p.name) from ProductBatch pb join Product p on pb.productId.id = p.id WHERE lower(pb.name) like lower(concat('%', concat(:name, '%')))")
    Page<QueryProductBatch> findByLikeName(Pageable pageable,String name);

    @Query("SELECT new com.sharework.health.dto.QueryProductBatch(pb.id, pb.name, pb.quantity, pb.expiry, p.name) from ProductBatch pb join Product p on pb.productId.id = p.id  where pb.expiry between :expiryStart and :expiryEnd")
    Page<QueryProductBatch> findProductByExpiry(Pageable pageable, LocalDate expiryStart, LocalDate expiryEnd);
}
