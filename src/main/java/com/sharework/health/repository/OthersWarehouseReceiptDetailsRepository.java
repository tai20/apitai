package com.sharework.health.repository;

import com.sharework.health.dto.OtherReceiptDetailsQueryDto;
import com.sharework.health.entity.OthersWarehouseReceiptDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OthersWarehouseReceiptDetailsRepository extends JpaRepository<OthersWarehouseReceiptDetails, Integer> {
    @Query("select new com.sharework.health.dto.OtherReceiptDetailsQueryDto(ord.othersWarehouseReceipt.id, ord.clinicStock.id, ord.productBatchId.id, ord.productBatchId.name, ord.number_product, cs.product.id, ord.productBatchId.productId.name) " +
            "from OthersWarehouseReceiptDetails ord " +
            "join ClinicStock cs on cs.id=ord.clinicStock.id " +
            "join Product p on p.id=cs.product.id where ord.othersWarehouseReceipt.id= :receipt_id")
    List<OtherReceiptDetailsQueryDto> getAllOtherReceiptByReceiptid(Integer receipt_id);


    @Query(nativeQuery = true, value = "select ord.* from otherswarehousereceiptdetails ord " +
            "where ord.receipt_id= :receipt_id and ord.clinic_stock_id= :clinic_stock_id")
    List<OthersWarehouseReceiptDetails> getOtherReceiptByClinicStockIdReceiptId(Integer receipt_id, Integer clinic_stock_id);
}
