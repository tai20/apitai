package com.sharework.health.repository;

import com.sharework.health.entity.CustomerLevel;
import com.sharework.health.entity.Product;
import com.sharework.health.entity.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Integer> {

    @Query(nativeQuery = true, value = "select * from service where status like 'ACTIVE'")
    List<Service> findALlByStatusLike();

    @Query(nativeQuery = true, value="select s.id, s.name, s.period, s.price, s.status, s.service_category_id from service s where s.status like 'ACTIVE'")
    Page<Service> findActive(Pageable pageable);

    @Query(nativeQuery = true, value = "select * from service order by id desc limit 1")
    Service findServiceAfterInsert();

    @Query(nativeQuery = true, value = "select s.* from service s " +
        "join orderdetailservice ods on s.id = ods.service_id join orders o on o.id = ods.order_id join customer c on c.id = o.customer_id " +
        "where c.id= :customerId")
    List<Service> findAllByCustomerId(Integer customerId);

    @Query(nativeQuery = true, value = "select s.* from service s where s.name ilike %:nameService% and s.status like 'ACTIVE' ")
    Page<Service> findByName(Pageable pageable, String nameService);

    @Query(nativeQuery = true, value = "select s.* from service s where s.service_category_id =:category and s.status like 'ACTIVE' ")
    Page<Service> findByCategory(Pageable pageable, Integer category);

}
