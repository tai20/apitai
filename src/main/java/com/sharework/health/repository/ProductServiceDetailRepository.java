package com.sharework.health.repository;

import com.sharework.health.entity.ProductServiceDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductServiceDetailRepository extends JpaRepository<ProductServiceDetail, Integer> {

    @Query(nativeQuery = true, value = "select * from service s join productservicedetail ps on s.id = ps.service_id where s.id= :serviceId")
    List<ProductServiceDetail> findAllByServiceId(Integer serviceId);
}
