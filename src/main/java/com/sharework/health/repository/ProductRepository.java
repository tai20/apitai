package com.sharework.health.repository;

import com.sharework.health.dto.GetIdAndNameProduct;
import com.sharework.health.entity.ClinicStock;
import com.sharework.health.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    List<Product> findByNameContains(String name);

    @Query(nativeQuery = true, value = "select p.* from product p join clinic c on p.clinic_id = c.id where c.id= :clinicId")
    List<Product> findAllByClinic(Integer clinicId);


    List<Product> findByClinicStocks_Clinic_Id(Integer id);


    List<Product> findByClinicStocks_Clinic_NameEquals(String name);

    @Query(nativeQuery = true, value = "select p.* from product p join clinic c on p.clinic_id = c.id where c.name= :clinicName")
    List<Product> findAllByClinicName(String clinicName);

    @Query(nativeQuery = true, value = "select p.*\n" +
            "from warehousereceiptdetail wr join product p on wr.product_id = p.id join warehousereceipt w on w.id = wr.receipt_id\n" +
            "where dateimport between :from and :to")
    List<Product> findAllProductImportFromTo(LocalDate from, LocalDate to);

    @Query(nativeQuery = true, value = "select p.*\n" +
            "from orderdetailproduct op join product p on op.product_id = p.id join orders o on o.id = op.order_id\n" +
            "where date(o.orderdate) between :from and :to")
    List<Product> findAllProductExportFromTo(LocalDate from, LocalDate to);

    @Query("select new com.sharework.health.dto.GetIdAndNameProduct(p.id, p.name) from Product p")
    List<GetIdAndNameProduct> getIdAndNameProduct();

    @Query(nativeQuery = true, value = "select p.* from product p where p.name ilike %:nameProduct% and p.status like 'ACTIVE'")
    Page<Product> findByName(Pageable pageable, String nameProduct);

    @Query(nativeQuery = true, value = "select p.* from product p where p.product_category_id =:category and p.status like 'ACTIVE'")
    Page<Product> findByCategory(Pageable pageable, Integer category);
}
