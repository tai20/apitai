package com.sharework.health.repository;

import com.sharework.health.entity.TreatmentPackage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TreatmentPackageRepository extends JpaRepository<TreatmentPackage, Integer> {

    TreatmentPackage findByName(String name);

    @Query(nativeQuery = true, value = "select * from treatmentpackage order by id desc limit 1")
    TreatmentPackage findTreatmentPackageAfterInsert();

    @Query(nativeQuery = true,value = "select * from treatmentpackage tp join slipuse s on tp.id = s.treatment_id where s.customer_id= :customerId ")
    List<TreatmentPackage> findByCustomerId(Integer customerId);

    @Query(nativeQuery = true,value = "select * from treatmentpackage where name ilike %:name%")
    Page<TreatmentPackage> findTreatmentPackageByName(Pageable pageable, String name);

    @Query(nativeQuery = true,value = "select * from treatmentpackage where status =:status")
    Page<TreatmentPackage> findTreatmentPackageByStatus(Pageable pageable, String status);
}
