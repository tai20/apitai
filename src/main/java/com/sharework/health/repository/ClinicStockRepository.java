package com.sharework.health.repository;


import com.sharework.health.entity.ClinicStock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClinicStockRepository extends JpaRepository<ClinicStock, Integer> {

    @Query(nativeQuery = true, value="select  * from clinicstock where clinic_id= :clinic_id")
    List<ClinicStock> getClinicStockByClinicId(Integer clinic_id);

    @Query(nativeQuery = true, value="select  * from clinicstock where product_id= :product_id")
    List<ClinicStock> getClinicStockByProductId(Integer product_id);

    @Query(nativeQuery = true, value="select  * from clinicstock where product_id= :product_id and clinic_id= :clinic_id ")
    ClinicStock getClinicStockByClinicIdAndAndProductId(Integer clinic_id, Integer product_id);

    //Kiểm tra xem sp đó có ở phiếu hay không

    @Query(nativeQuery = true, value="select cs.* from clinicstock cs join importreceiptdetails ON importreceiptdetails.clinic_stock_id = cs.id where cs.id= :clinic_stock_id")
    List<ClinicStock> getClinicStockFromImportReceipt(Integer clinic_stock_id);


    @Query(nativeQuery = true, value="select cs.* from clinicstock cs join exportreceiptdetails ON exportreceiptdetails.clinic_stock_id = cs.id where cs.id= :clinic_stock_id")
    List<ClinicStock> getClinicStockFromExportReceipt(Integer clinic_stock_id);


    @Query(nativeQuery = true, value="select cs.* from clinicstock cs join transferreceiptdetails ON transferreceiptdetails.clinic_stock_id = cs.id where cs.id= :clinic_stock_id")
    List<ClinicStock> getClinicStockFromTransferReceipt(Integer clinic_stock_id);

    @Query(nativeQuery = true, value="select cs.* from clinicstock cs join otherswarehousereceiptdetails ON otherswarehousereceiptdetails.clinic_stock_id = cs.id where cs.id= :clinic_stock_id")
    List<ClinicStock> getClinicStockFromOtherReceipt(Integer clinic_stock_id);


    //Tính tổng sản phẩm ở thời điểm hiện tại
    @Query("select sum(cs.quantity) from ClinicStock cs " +
            "where cs.product.id= :product_id and (:clinic_id is null or cs.clinic.id=: clinic_id)")
    Integer stockPresentByProductId(Integer product_id, Integer clinic_id);

    //Tìm mã chi nhánh ứng với bảng Users
    @Query(nativeQuery = true, value="select *\n" +
            "from clinicstock cs\n" +
            "inner join product pr ON cs.product_id = pr.id\n" +
            "inner join productcategory pc ON pr.product_category_id = pc.id\n" +
            "inner join clinic cn ON cn.id = cs.clinic_id\n" +
            "inner join users u ON u.clinic_id = cn.id\n" +
            "where cs.clinic_id = cn.id and u.clinic_id= :clinic_id")
    List<ClinicStock> getClinicStockByClinicIdOfUsers(Integer clinic_id);

    List<ClinicStock> findByClinic_Users_IdEqualsAndProduct_NameLike(Integer id, String name);

    //tim san pham theo tren kem phan trang
    @Query(nativeQuery = true, value="select  c.* from clinicstock c inner join product pr ON c.product_id = pr.id where c.clinic_id= :clinic_id \n" +
            "and pr.name ilike %:name% ")
    Page<ClinicStock> findClinicStockByPage(Pageable pageable,int clinic_id, String name);

    @Query(nativeQuery = true, value="select  c.* from clinicstock c inner join product pr ON c.product_id = pr.id where " +
            " pr.name ilike %:name% ")
    Page<ClinicStock> findClinicStockByPageOfDepot(Pageable pageable, String name);

    @Query(nativeQuery = true, value="SELECT cs.* " +
            " FROM public.clinicstock cs " +
            " join public.product p " +
            " on cs.product_id = p.id " +
            " join public.productcategory pc " +
            " on p.product_category_id = pc.id " +
            " where pc.name =:categoryName and cs.clinic_id =:clinic_id ")
    Page<ClinicStock> findClinicStockByCategoryPage(Pageable pageable,int clinic_id, String categoryName);

    @Query(nativeQuery = true, value="SELECT cs.* " +
            " FROM public.clinicstock cs " +
            " join public.product p " +
            " on cs.product_id = p.id " +
            " join public.productcategory pc " +
            " on p.product_category_id = pc.id " +
            " where pc.name =:categoryName  ")
    Page<ClinicStock> findClinicStockByCategoryPage(Pageable pageable, String categoryName);

}
