package com.sharework.health.repository;

import com.sharework.health.entity.TreatmentPackageService;
import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TreatmentPackageServiceRepository extends JpaRepository<TreatmentPackageService, Integer> {

    @Query(nativeQuery = true, value = "select tps.* from treatmentpackageservice tps join treatmentpackage tp on tps.treatmentpackage_id = tp.id where tp.id= :treatmentPackageId")
    List<TreatmentPackageService> findAllByTreatmentPackage(Integer treatmentPackageId);

    @Query(nativeQuery = true, value = "select tps.* from treatmentpackageservice tps join treatmentpackage tp on tps.treatment_id = tp.id join slipuse s on s.treatment_id = tp.id where s.customer_id= :customerId ")
    List<TreatmentPackageService> findAllByCustomerId(Integer customerId);
}
