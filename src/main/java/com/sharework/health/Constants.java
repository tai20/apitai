package com.sharework.health;

public class Constants {
    public static final String DEFAULT_PAGE_NUMBER = "1";
    public  static final String DEFAULT_PAGE_SIZE = "10";
    public static final String DEFAULT_SORT_BY = "id";
    public static final String DEFAULT_SORT_DIRECTION = "asc";

    public static final String CUSTOMER_DEFAULT_SORT_BY = "c.id";

    public static final String REMOVE_NAME = "name";
    public static final String REMOVE_PHONE = "phoneNumber";

    public static final String ACTIVE = "ACTIVE";
    public static final String DEACTIVE = "DEACTIVE";
    public static final String INACTIVE = "INACTIVE";
    public static final String EXAMINED = "EXAMINED";

    public static final String DEPOT = "DEPOT";
    public static final String BRANCH = "BRANCH";

    // FUNCTIONS

    public static String revertInput(String input) {
        String result = input.replaceAll("[\"-+.^:,}{]", "")
                .replaceAll(Constants.REMOVE_NAME, "")
                .replaceAll(Constants.REMOVE_PHONE, "");
        return result;
    }
}
