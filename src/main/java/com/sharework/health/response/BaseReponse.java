package com.sharework.health.response;

import lombok.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseReponse<T> {
    private List<T> data;
    private int current;
    private int pageSize;
    private long totalElements;
    private int totalPages;
    private boolean last;
}
