package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "gift")
public class Gift {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    private String name ;

    private LocalDate startDate;

    private LocalDate endDate;

    private Integer quantity;

    @OneToMany(mappedBy = "gift", cascade = CascadeType.ALL)
    private List<GiftList> giftLists;
}
