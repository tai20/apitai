package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "exportreceiptdetails")
@IdClass(ExportReceiptDetails_PK.class)
public class ExportReceiptDetails implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "receipt_id")
    private ExportReceipt exportReceipt;

    @Id
    @ManyToOne
    @JoinColumn(name="clinic_stock_id")
    private ClinicStock clinicStock;

    @Column
    private Integer number_product_export;

    @Column(precision = 19, scale = 2)
    private BigDecimal export_price;

    @ManyToOne
    @JoinColumn(name = "productBatchId")
    private ProductBatch productBatchId;
}
