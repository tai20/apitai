package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.poi.hpsf.Decimal;

import java.math.BigDecimal;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "level")
public class Level implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    private String name;

    private BigDecimal min_value;

    private BigDecimal max_value;

    @OneToMany(mappedBy = "level", fetch = FetchType.LAZY)
    private List<CustomerLevel> customerLevels;
}
