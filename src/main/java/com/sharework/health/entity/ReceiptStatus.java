package com.sharework.health.entity;

public enum ReceiptStatus {
    ACTIVE  , DELETED, LOCKED
}
