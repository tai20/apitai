package com.sharework.health.entity;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "clinic")
public class Clinic implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true)
    private String name;

    private String address;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Enumerated(EnumType.STRING)
    private ClinicType type;

    @OneToMany(mappedBy = "clinic", fetch = FetchType.LAZY, cascade = ALL, orphanRemoval = true)
    private List<User> users;

    @OneToMany(mappedBy = "clinic", fetch = FetchType.LAZY)
    private List<WarehouseReceipt> warehouseReceipts;

//    @OneToMany(mappedBy = "clinic")
//    private List<Product> products;

    @OneToMany(mappedBy = "clinic")
    private  List<ClinicStock> clinicStocks;

    @OneToMany(mappedBy = "clinic")
    private  List<ImportReceipt> importReceipts;

    @OneToMany(mappedBy = "clinic")
    private List<ExportReceipt> exportReceipts;

    @OneToMany(mappedBy = "clinic")
    private List<OthersWarehouseReceipt> othersWarehouseReceipts;

    @OneToMany(mappedBy = "clinic_sending")
    private List<TransferReceipt> transferReceipts_sending;

    @OneToMany(mappedBy = "clinic_receiving")
    private List<TransferReceipt> transferReceipts_receiving;

    @OneToMany(mappedBy = "clinic", fetch = FetchType.LAZY, cascade = ALL, orphanRemoval = true)
    private List<Customer> customers;
}
