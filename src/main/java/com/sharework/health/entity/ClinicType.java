package com.sharework.health.entity;

public enum ClinicType {
    DEPOT, BRANCH
}
