package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "clinicstock")
//@IdClass(ClinicStock_PK.class)
public class ClinicStock implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "clinic_id")
    private Clinic clinic;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @Column(nullable = false)
    private Integer quantity;

    @Column(length = 255)
    private String unit;

    @OneToMany(mappedBy = "clinicStock")
    private List<ImportReceiptDetails> importReceiptDetails;

    @OneToMany(mappedBy = "clinicStock")
    private List<ExportReceiptDetails> exportReceiptDetails;

    @OneToMany(mappedBy = "clinicStock")
    private List<TransferReceiptDetails> transferReceiptDetails;

    @OneToMany(mappedBy = "clinicStock")
    private List<OthersWarehouseReceiptDetails> othersWarehouseReceiptDetails;
}
