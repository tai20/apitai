package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "transferreceiptdetails")
@IdClass(TransferReceiptDetails_PK.class)
public class TransferReceiptDetails implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "receipt_id")
    private TransferReceipt transferReceipt;

    @Id
    @ManyToOne
    @JoinColumn(name = "clinic_stock_id")
    private ClinicStock clinicStock;

    @Column
    private Integer number_product_transfer;

    @ManyToOne
    @JoinColumn(name = "productBatchId")
    private ProductBatch productBatchId;
}
