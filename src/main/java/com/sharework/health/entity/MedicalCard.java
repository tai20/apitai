package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class MedicalCard implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String diagnose;

    private String differentDiagnose;

    private String management;

    private LocalDate nextAppointment;

    private String note;

    private String advice;

    private String vulva;

    private String vagina;

    private String cervix;

    private String fornix;

    private String tuboovarian;

    private String perineal;

    private String uterus;

    private String cardiovascularrisk;

    private String breastrisk;

    private String popq;

    private String cough;

    private String bonney;

    private String knack;

    private String pelvicfloorusclestrength;

    private String oldsurgicalwound;

    private String contractions;

    private String shapeuterus;

    private String bctc;

    private String breast;

    private String heartfetus;

    private String amnioticfluidstatus;

    private String timeboken;

    private String color;

    private String smell;

    private String throne;

    private String penetration;

    private String pelvic;

    private String estimateddateofdelivery;

    private String shape;

    private String limitedbreastright;

    private String density;

    private String breastright;

    private String positionbreastright;

    private String densitybreastright;

    private String dimensionbreastright;

    private String secretionsbreastright;

    private String breastleft;

    private String positionbreastleft;

    private String densitybreastleft;

    private String dimensionbreastleft;

    private String secretionsbreastleft;

    private String axillarylymphnodes;

    private String supraclavicularlymphnodes;

    private String pulserate;

    private String bloodpressure;

    private Double height;

    private Double weight;

    private String bmi;

    private String allergy;

    private String para;

    private String obstetricsherself;

    private String internalherself;

    private String edema;

    private String heart;

    private String generalcondition;

    private String herfamily;

    private String lastmenstrualperiod;

    private String lung;

    private String mucosalskin;

    private String sign;

    private String stomach;

    private String nameMedicalCard;

    private String limitedBreastLeft;

    private String bodyTemperature;

    private String others;

    @ManyToOne
    @JoinColumn(name = "testservice_id")
    private TestService testService;

    @ManyToOne
    @JoinColumn(name = "slipusepackagetest_id")
    private SlipUsePackageTest slipUsePackageTest;

    @OneToOne
    @JoinColumn(name = "indicationcard_id")
    private IndicationCard indicationCard;

    @OneToOne
    @JoinColumn(name = "prescription_id")
    private Prescription prescription;

    @OneToOne
    @JoinColumn(name = "totalcost_id")
    private TotalCost totalCost;


}
