package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "transferreceipt")
public class TransferReceipt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name="sending_warehouse_id")
    private Clinic clinic_sending;

    @ManyToOne
    @JoinColumn(name="receiving_warehouse_id")
    private Clinic clinic_receiving;

    private LocalDateTime dateexport;

    private LocalDateTime dateimport;

    @Enumerated(EnumType.STRING)
    private TransferStatus status;

    @OneToMany(mappedBy = "transferReceipt")
    private List<TransferReceiptDetails> transferReceiptDetails;

}
