package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@IdClass(PrescriptionDetail_PK.class)
public class PrescriptionDetail implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "prescription_id")
    private Prescription prescription;

    @Id
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    private String tutorial;

    private int quantity;
}
