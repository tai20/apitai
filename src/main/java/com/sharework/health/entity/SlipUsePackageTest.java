package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;


@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@Entity
@Table(name = "slipusepackagetest")
public class SlipUsePackageTest implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "testservice_id")
    private TestService testService;

    private LocalDate startdate;

    private LocalDate enddate;

    @Enumerated(EnumType.STRING)
    private Status status;

    private BigDecimal price;

    private Integer numberofuse;

    @OneToMany(mappedBy = "slipusePackageTest", fetch = FetchType.LAZY)
    private List<SlipUsePackageTestDetail> slipusePackageTestDetails;

    @OneToMany(mappedBy = "slipUsePackageTest", fetch = FetchType.LAZY)
    private List<MedicalCard> medicalCards;

    private String name;

}
