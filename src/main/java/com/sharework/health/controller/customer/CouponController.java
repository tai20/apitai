package com.sharework.health.controller.customer;

import com.sharework.health.dto.CouponDto;
import com.sharework.health.service.CouponService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/coupons")
@AllArgsConstructor
public class CouponController {

    private CouponService couponService;

    @GetMapping("{customer_id}")
    public Object getCouponsByCustomer(@PathVariable("customer_id") Integer customerId){
        List<CouponDto> couponDtos = couponService.getCouponsByCustomer(customerId);
        if (couponDtos.isEmpty() || couponDtos == null){
            return new ResponseEntity<>("Khách hàng không có mã khuyến mã nào", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(couponDtos, HttpStatus.OK);
    }
}
