package com.sharework.health.controller.admin;

import com.sharework.health.dto.PackageTestServiceDetailDto;
import com.sharework.health.service.PackageTestServiceDetailService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/packagetestservicedetail")
@AllArgsConstructor
public class PackageTestServiceDetailController {

    private PackageTestServiceDetailService packageTestServiceDetailService;

    @GetMapping("")
    public Object getPackageTestServiceDetail() {
        List<PackageTestServiceDetailDto> packageTestServiceDetailDtos = packageTestServiceDetailService.findAll();
        if (packageTestServiceDetailDtos.isEmpty() || packageTestServiceDetailDtos == null) {
            return new ResponseEntity<>("Không có chi tiết gói dịch vụ nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(packageTestServiceDetailDtos, HttpStatus.OK);
    }

    @GetMapping("{packagetest_id}")
    public Object getAPackageTestServiceDetailByPackageId(@PathVariable("packagetest_id") Integer customerId) {
        List<PackageTestServiceDetailDto> packageTestServiceDetailDtos = packageTestServiceDetailService.getPackageTestDetalByPackageId(customerId);
        if (packageTestServiceDetailDtos.isEmpty() || packageTestServiceDetailDtos == null) {
            return new ResponseEntity<>("Không có gói chi tiết dịch vụ sử dụng nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(packageTestServiceDetailDtos, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addAPackageTestServiceDetail(@RequestBody PackageTestServiceDetailDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = packageTestServiceDetailService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm thành công", HttpStatus.CREATED);
    }


}
