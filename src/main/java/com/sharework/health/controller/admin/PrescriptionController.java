package com.sharework.health.controller.admin;

import com.sharework.health.dto.IndicationCardDto;
import com.sharework.health.dto.PrescriptionAfterInsertDto;
import com.sharework.health.dto.PrescriptionDetailDto;
import com.sharework.health.dto.PrescriptionDto;
import com.sharework.health.service.PrescriptionService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/prescriptions")
@AllArgsConstructor
public class PrescriptionController {

    private PrescriptionService prescriptionService;

    @GetMapping("")
    public Object getAllPrescription() {
        List<PrescriptionDto> dtos = prescriptionService.findAll();
        if (dtos.isEmpty() || dtos == null) {
            return new ResponseEntity<>("Không có đơn thuốc nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addPrescription(@RequestParam("medicalId") Integer id,@RequestBody PrescriptionDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = prescriptionService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm đơn thuốc thất bại", HttpStatus.BAD_REQUEST);
        }
        PrescriptionDto resultUpdatePrescriptionInMedicalCard = prescriptionService.updatePrescriptionInMedicalCard(id,prescriptionService.getPrescriptionAfterInsert());
        PrescriptionAfterInsertDto dto1 = new PrescriptionAfterInsertDto()
                .setMessage("Thêm đơn thuốc thành công")
                .setPrescriptionDto(resultUpdatePrescriptionInMedicalCard);
        return new ResponseEntity<>(dto1, HttpStatus.CREATED);
    }

    @PostMapping("addPrescriptionDetail")
    public Object addPrescriptionDetail(@RequestBody PrescriptionDetailDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = prescriptionService.addPrescriptionDetail(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm chi tiết đơn thuốc thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm chi tiết đơn thuốc thành công", HttpStatus.CREATED);
    }

    @DeleteMapping("deletePrescriptionDetail")
    public Object deletePrescriptionDetail(@RequestBody PrescriptionDetailDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = prescriptionService.deletePrescriptionDetail(dto);
        if (!result) {
            return new ResponseEntity<>("Xóa chi tiết đơn thuốc thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Xóa chi tiết đơn thuốc thành công", HttpStatus.CREATED);
    }

    @GetMapping("getPrescriptionDetailByMedicalCard/{medicalcard_id}")
    public Object getPrescriptionDetailByMedicalCard(@PathVariable("medicalcard_id") Integer medicalCardId) {
        List<PrescriptionDetailDto> dtos = prescriptionService.getPrescriptionDetailByMedicalCard(medicalCardId);
        if (dtos.isEmpty() || dtos == null) {
            return new ResponseEntity<>("Không có chi tiết đơn thuốc nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PutMapping("updatePrescriptionDetail")
    public Object updatePrescriptionDetail(@RequestParam("prescriptionId") Integer prescriptionId, @RequestParam("productId") Integer productId,
                                       @RequestBody PrescriptionDetailDto dto,  BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = prescriptionService.updatePrescriptionDetail(prescriptionId,productId,dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.OK);
    }


}
