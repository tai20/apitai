package com.sharework.health.controller.admin;

import com.sharework.health.Constants;
import com.sharework.health.dto.*;
import com.sharework.health.response.ClinicStockReponse;
import com.sharework.health.service.ClinicStockService;
import com.sharework.health.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/admin/clinicstocks")
@AllArgsConstructor
public class ClinicStockController {

    private ClinicStockService clinicStockService;

    private UserService userService;


//    @GetMapping("")
//    public Object getAllClinicStock() {
//        List<ClinicStockDto> clinicStockDtos = clinicStockService.findAll();
//        if (clinicStockDtos.isEmpty()) {
//            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.BAD_REQUEST);
//        }
//        return new ResponseEntity<>(clinicStockDtos, HttpStatus.OK);
//    }

    @GetMapping("clinic/{clinicStock_id}")
    public Object getClinicStockById(@PathVariable("clinicStock_id") Integer id) {
        ClinicStockDto dto = clinicStockService.findById(id);
        if (dto == null) {
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping("/{clinicStock_id}")
    public ResponseEntity<ClinicStockDto> updateClinicStock(@PathVariable("clinicStock_id") Integer id, @RequestBody ClinicStockDto dto,
                                                            BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        clinicStockService.update(id, dto);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    // ghi số lượng cần trừ vào (quantity)
    @PutMapping("/subtract")
    public ResponseEntity<Object> updateClinicSubtractStock(@RequestBody List<ClinicStockDto> dto,
                                                            BindingResult error) {
        for (ClinicStockDto model : dto) {
            ClinicStockDto dto2 = clinicStockService.findById(model.getId());
            dto2.setQuantity(dto2.getQuantity() - model.getQuantity());
            clinicStockService.update(model.getId(), dto2);
        }
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Đã trừ thành công vào kho", HttpStatus.OK);
    }


//    @GetMapping("clinic")
//    public ResponseEntity<ResponseObject> getClinicStockByClinicIDOfUsers(Principal principal) {
//        String email = principal.getName();
//        UserDto u = userService.findByEmail(email);
//        List<ClinicStockDto> dto = clinicStockService.getClinicStockByClinicId(u.getClinicDto().getId());
//        if (dto.isEmpty()) {
//            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
//                    new ResponseObject(404, "Không tìm thấy", "")
//            );
//        } else
//            return ResponseEntity.status(HttpStatus.OK).body(
//                    new ResponseObject(200, "Truy vấn thành công", dto)
//            );
//    }

    //tìm kiếm theo "Tên sản phẩm"
    @GetMapping("clinic/product_name/{product_name}")
    public ResponseEntity<ResponseObject> getClinicStockByNameClinic(Principal principal, @PathVariable String product_name) {
        try {
            List<ClinicStockDto> clinicStockDtos = clinicStockService.getClinicStockByProductName(principal, product_name);
            if (clinicStockDtos.isEmpty() || clinicStockDtos.size() == 0) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        new ResponseObject(404, "Không tìm thấy", ""));
            }
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200, "Truy vấn thành công", clinicStockDtos));

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Truy vấn không thành công", e.getMessage()));
        }

    }

    @GetMapping("clinic")
    public ResponseEntity<ResponseObject> getClinicStockByClinicIDOfUsers(Principal principal) {
        String email = principal.getName();
        UserDto u = userService.findByEmail(email);
        List<ClinicStockDto> dto = clinicStockService.getClinicStockByClinicIdOfUsers(u.getClinicDto().getId());
        if (dto.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không tìm thấy", "")
            );
        } else
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200, "Truy vấn thành công", dto)
            );
    }

//    @GetMapping("/clinic/{clinicStock_id}")
//    public ResponseEntity<ResponseObject> getClinicStockByClinicID(@PathVariable("clinicStock_id") Integer id) {
//        try {
//            List<ClinicStockDto> dto = clinicStockService.getClinicStockByClinicId(id);
//            if (dto.size()==0) {
//                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
//                        new ResponseObject(404, "Không tìm thấy", ""));
//            }
//            return ResponseEntity.status(HttpStatus.OK).body(
//                    new ResponseObject(200, "Truy vấn thành công", dto));
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
//                    new ResponseObject(400, "Truy vấn không thành công", e.getMessage()));
//        }
//    }

//    @GetMapping("page")
//    public ClinicStockReponse getAllClinicStock(
//            @RequestParam(value = "sortBy", defaultValue = Constants.DEFAULT_SORT_BY, required = false) String sortBy,
//            @RequestParam(value = "sortDir", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) String sortDir,
//            @RequestParam(value = "current", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) int pageNo ,
//            @RequestParam(value = "pageSize", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) int pageSize) {
//
//        ClinicStockReponse clinicStockReponse = clinicStockService.getAllClinicStock(pageNo, pageSize, sortBy, sortDir);
//        if (clinicStockReponse == null){
//            return null;
//        }
//        return clinicStockReponse;
//    }

    @GetMapping("page")
    public ClinicStockReponse getAllClinicStockClinic(Principal principal,
                                                      @RequestParam(value = "sortBy", defaultValue = Constants.DEFAULT_SORT_BY, required = false) String sortBy,
                                                      @RequestParam(value = "sortDir", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) String sortDir,
                                                      @RequestParam(value = "current", defaultValue = Constants.DEFAULT_PAGE_NUMBER, required = false) int pageNo ,
                                                      @RequestParam(value = "pageSize", defaultValue = Constants.DEFAULT_PAGE_SIZE, required = false) int pageSize,
                                                      @RequestParam(value = "productDto", required = false) String nameProduct,
                                                      @RequestParam(value = "productCategoryDto", required = false) String productCategoryDto) {

        String email = principal.getName();
        UserDto u = userService.findByEmail(email);

        if(nameProduct == null && productCategoryDto == null ) return clinicStockService.getAllClinicStockClinic(pageNo, pageSize, sortBy, sortDir, u.getClinicDto().getId());
        if(nameProduct != null && productCategoryDto == null ) return clinicStockService.getClinicStockByProductNamePage(principal,pageNo, pageSize, sortBy, sortDir, Constants.revertInput(nameProduct));
        if(nameProduct == null && productCategoryDto != null ) return clinicStockService.getClinicStockByCategoryNamePage(principal,pageNo, pageSize, sortBy, sortDir, Constants.revertInput(productCategoryDto));

        return null;
    }

    @GetMapping("clinic/product_name/find")
    public ClinicStockReponse getClinicStockByNameClinicPage(
            Principal principal, @RequestParam(value = "name",required = false) String product_name,
            @RequestParam(value = "sortBy", defaultValue = Constants.DEFAULT_SORT_BY, required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) String sortDir,
            @RequestParam(value = "current", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) int pageNo ,
            @RequestParam(value = "pageSize", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) int pageSize) {

//        ClinicStockReponse clinicStockReponse = clinicStockService.getClinicStockByProductNamePage(principal,product_name,pageNo,
//                pageSize, sortBy, sortDir);
//        if (clinicStockReponse.getData().isEmpty()){
//            return null;
//        }
        return null ;

    }
}



