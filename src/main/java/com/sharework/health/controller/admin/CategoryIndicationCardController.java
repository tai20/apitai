package com.sharework.health.controller.admin;

import com.sharework.health.dto.CategoryIndicationCardDto;
import com.sharework.health.service.CategoryIndicationCardService;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/categoryindicationcards")
@AllArgsConstructor
public class CategoryIndicationCardController {

    private CategoryIndicationCardService categoryIndicationCardService;

    @GetMapping("")
    public Object getAllCategoryIndicationCard() {
        List<CategoryIndicationCardDto> categoryIndicationCardDtos = categoryIndicationCardService.findAll();
        if (categoryIndicationCardDtos.isEmpty() || categoryIndicationCardDtos == null) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(categoryIndicationCardDtos, HttpStatus.OK);
    }

    @GetMapping("/{categoryIndication_id}")
    public Object getCategoryIndicationCardById(@PathVariable("categoryIndication_id") Integer id) {
        CategoryIndicationCardDto categoryIndicationCardDto = categoryIndicationCardService.findById(id);
        if (categoryIndicationCardDto == null) {
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(categoryIndicationCardDto, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addCategoryIndicationCard(@RequestBody CategoryIndicationCardDto dto,
                                            BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = categoryIndicationCardService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm yêu cầu thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm yêu cầu thành công", HttpStatus.CREATED);
    }

    @PutMapping("/{categoryIndication_id}")
    public Object updateCategoryIndicationCard(@PathVariable("categoryIndication_id") Integer id, @RequestBody CategoryIndicationCardDto dto,
                                               BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = categoryIndicationCardService.update(id, dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhật yêu cầu thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật yêu cầu thành công", HttpStatus.CREATED);
    }

    @DeleteMapping("/{categoryIndication_id}")
    public Object deleteCategoryIndicationCard(@PathVariable("categoryIndication_id") Integer categoryIndication_id) {

        boolean result = categoryIndicationCardService.delete(categoryIndication_id);
        if (!result) {
            return new ResponseEntity<>("Xóa thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Xóa thành công", HttpStatus.OK);
    }

    @PostMapping("findAllBySubCategoryIndicationCardName")
    public Object findAllBySubCategoryIndicationCardName(@RequestParam("subCategoryIndication_name") String subCategoryIndicationCard) {
        List<CategoryIndicationCardDto> categoryIndicationCardDtos = categoryIndicationCardService.findAllBySubCategoryIndicationCardName(subCategoryIndicationCard);
        try {
            if (categoryIndicationCardDtos == null) {
                return new ResponseEntity<>("Không tìm thấy chỉ định", HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(categoryIndicationCardDtos, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}