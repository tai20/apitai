package com.sharework.health.controller.admin;

import com.sharework.health.dto.ProductCategoryDto;
import com.sharework.health.service.ProductCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;


@RestController
@RequestMapping("/api/admin/productcategories")
@AllArgsConstructor
public class ProductCategoryController {

    private ProductCategoryService productCategoryService;

    @GetMapping("")
    public Object getAllProductCategory() {
        List<ProductCategoryDto> productCategoryDtos = productCategoryService.findAll();
        if (productCategoryDtos.isEmpty() || productCategoryDtos == null) {
            return new ResponseEntity<>("Không có dữ lieu", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(productCategoryDtos,HttpStatus.OK);
    }

    @GetMapping("/{productcategory_id}")
    public ResponseEntity<ProductCategoryDto> getProductCategoryById(@PathVariable("productcategory_id") Integer id) {
        ProductCategoryDto productCategoryDto = productCategoryService.findById(id);
        if (productCategoryDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(productCategoryDto,HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<ProductCategoryDto> addProductCategory(@RequestBody ProductCategoryDto dto,
                                           BindingResult error) throws IOException {
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        productCategoryService.insert(dto);
        return new ResponseEntity<>(dto,HttpStatus.CREATED);
    }

    @PutMapping("/{productcategory_id}")
    public ResponseEntity<ProductCategoryDto> updateProductCategory(@PathVariable("productcategory_id") Integer id , @RequestBody ProductCategoryDto dto,
                                                  BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        productCategoryService.update(id, dto);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @DeleteMapping("/{productcategory_id}")
    public ResponseEntity<ProductCategoryDto> deleteProductCategory(@PathVariable("productcategory_id") Integer id) {
        productCategoryService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("searchName")
    public Object searchCategoryByCategoryName(@RequestParam("productcategory_name") String name) {
        ProductCategoryDto dto = productCategoryService.searchCategoryByCategoryName(name);
        if (dto == null){
            return new ResponseEntity<>("Không tìm thấy loại sản phẩm cần tìm",HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }
}
