package com.sharework.health.controller.admin;

import com.sharework.health.Constants;
import com.sharework.health.dto.*;
import com.sharework.health.entity.Customer;
import com.sharework.health.entity.CustomerLevel;
import com.sharework.health.response.CustomerReponse;
import com.sharework.health.service.OrderService;
import com.sharework.health.service.UserService;
import org.springframework.mock.web.MockMultipartFile;
import com.sharework.health.repository.CustomerRepository;
import com.sharework.health.service.CustomerService;
import lombok.AllArgsConstructor;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/admin/customers")
@AllArgsConstructor
public class AdminCustomerController {

    private CustomerRepository customerRepository;

    private CustomerService customerService;

    private UserService userService;

    private OrderService orderService;

    @GetMapping("")
    public Object getAllUser() {
         String roleName = userService.getRoleName();
         List<CustomerLevelDto> customerlevelDtos = null;
        if (roleName.equalsIgnoreCase("ROLE_ADMIN")){
            customerlevelDtos = customerService.findAllByAdmin();
            if (customerlevelDtos.isEmpty()) {
                return new ResponseEntity<>("Không có dữ liệu",HttpStatus.BAD_REQUEST);
            }
        }else if(roleName.equalsIgnoreCase("ROLE_RECEPTIONIST")){
            customerlevelDtos = customerService.findAllByType("Spa");
            if (customerlevelDtos.isEmpty()) {
                return new ResponseEntity<>("Không có dữ liệu",HttpStatus.BAD_REQUEST);
            }
        }else if(roleName.equalsIgnoreCase("ROLE_RECEPTIONIST_OBSTETRICAL")){
            customerlevelDtos = customerService.findAllByType("Sản khoa");
            if (customerlevelDtos.isEmpty()) {
                return new ResponseEntity<>("Không có dữ liệu",HttpStatus.BAD_REQUEST);
            }
        }
        return new ResponseEntity<>(customerlevelDtos,HttpStatus.OK);
    }

    @PostMapping("")
    public Object addCustomer(@RequestParam(name="name") String name, @RequestParam(name = "email") String email,
                              @RequestParam(name = "birthDate") String birthDate, @RequestParam(name = "gender") String gender,
                              @RequestParam(name = "address") String address, @RequestParam(name = "cmnd") String cmnd,
                              @RequestParam(name = "phone") String phone, @RequestParam(name = "skinStatus") String skinStatus,
                              @RequestParam(name = "customerResource") String customerResource, @RequestParam(name = "avatar") MultipartFile avatar,
                              @RequestParam(name = "clinicId") Integer clinicId, @RequestParam(name = "typeCustomer") String typeCustomer
    ) throws IOException {
        boolean result = customerService.insertCustomer(name, email, birthDate, gender, address, cmnd, phone, skinStatus, customerResource,  avatar, clinicId,typeCustomer);

        if (!result){
            return new ResponseEntity<>("Thêm khách hàng thất bại",HttpStatus.BAD_REQUEST);
        }
        CustomerDto customerDto = customerService.findCustomerAfterInsert();
        CustomerAfterInsertDto customerAfterInsertDto = new CustomerAfterInsertDto()
                .setMessage("Thêm khách hàng thành công")
                .setCustomerDto(customerDto);
        return new ResponseEntity<>(customerDto,HttpStatus.CREATED);
    }

    @PostMapping("addCustomLevel")
    public Object addCustomLevel(@RequestBody CustomerLevelDto dto){
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = customerService.addCustomerLevel(dto);
        if (!result){
            return new ResponseEntity<>("Thêm dịch vụ thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm dịch vụ thành công", HttpStatus.OK);
    }

    @PutMapping("/{customer_id}")
    public Object updateCustomer(@PathVariable("customer_id") Integer id , @RequestBody CustomerDto dto,
                                       BindingResult error) {
        if (dto.getGender() == "" || dto.getCmnd() == "" || dto.getEmail() == "" || dto.getPhoneNumber() == ""){
            return new ResponseEntity<>("Thiếu thông tin quan trọng (gender, cmnd, email, phoneNumber)",HttpStatus.BAD_REQUEST);
        }
        if (error.hasErrors()) {
            return new ResponseEntity<>("Cập nhật khách hàng thất bại",HttpStatus.BAD_REQUEST);
        }
        boolean result = customerService.update(id, dto);
        if (!result){
            return new ResponseEntity<>("Cập nhật khách hàng thất bại",HttpStatus.OK);
        }
        return new ResponseEntity<>("Cập nhật khách hàng thành công",HttpStatus.OK);
    }

    @PutMapping("updateCustomlevel")
    public Object updateCustomer(@RequestBody CustomerLevelDto dto) {
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = customerService.updateCustomerLevel(dto);
        if (!result){
            return new ResponseEntity<>("Thêm dịch vụ thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm dịch vụ thành công", HttpStatus.OK);
    }



    @PutMapping("/changeAvatar/{customer_id}")
    public Object updateAvatar(@PathVariable("customer_id") Integer id, @RequestParam(name = "avatar") MultipartFile avatar) throws IOException {
        boolean result = customerService.updateAvatar(id, avatar);
        if (!result){
            return new ResponseEntity<>("Sửa avatar không thành công",HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Sửa avatar thành công",HttpStatus.CREATED);
    }

    @DeleteMapping("customerLevel/{customer_id}")
    public ResponseEntity<CustomerLevelDto> deleteCustomerLevel(@PathVariable("customer_id") Integer id) {
        customerService.deleteCustomerLevel(id);
        return new ResponseEntity<>( HttpStatus.OK);
    }

    @DeleteMapping("/{customer_id}")
    public ResponseEntity<CustomerDto> deleteCustomer(@PathVariable("customer_id") Integer id) {
        customerService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{phoneNumber}")
    public Object findCustomerByPhoneNumber(@PathVariable("phoneNumber") String phoneNumber) {
        CustomerDto dto = customerService.findByPhoneNumber(phoneNumber);
        if (dto == null){
            return new ResponseEntity<>("Không tìm thấy khách hàng cần tìm", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @GetMapping("getCustomerById/{customer_id}")
    public Object getCustomerById(@PathVariable("customer_id") Integer customerId) {
        List<CustomerLevelDto> dto = customerService.findByUID(customerId);
        if (dto == null){
            return new ResponseEntity<>("Không tìm thấy khách hàng cần tìm", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @GetMapping("findAllByCustomerCategory/{customercategory_id}")
    public Object findAllByCustomerCategory(@PathVariable("customercategory_id") Integer customerCategoryId) {
        List<CustomerDto> customerDtos = customerService.findAllByCustomerCategory(customerCategoryId);
        if (customerDtos == null || customerDtos.isEmpty()){
            return new ResponseEntity<>("Danh mục này không có khách hàng", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(customerDtos,HttpStatus.OK);
    }

    @GetMapping("notification")
    public Object  getListNotification() {
        List<String> listNotification = customerService.getListNotification();
        if (listNotification.size()==0){
            return new ResponseEntity<>("Hôm nay không có thông báo", HttpStatus.OK);
        }
        return new ResponseEntity<>(listNotification,HttpStatus.OK);
    }

    @GetMapping("page")
    public CustomerReponse getAll(
            @RequestParam(value = "sortBy", defaultValue = Constants.CUSTOMER_DEFAULT_SORT_BY, required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) String sortDir,
            @RequestParam(value = "current", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) int pageNo ,
            @RequestParam(value = "pageSize", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) int pageSize,
            @RequestParam(value = "customer_id", required = false) String name) { // find theo ant design dung sua param truyen ve ;)))

        String roleName = userService.getRoleName();
        if (name == null) {
            if (roleName.equalsIgnoreCase("ROLE_ADMIN"))
                return customerService.getAllCustomer(pageNo, pageSize, sortBy, sortDir);
            if (roleName.equalsIgnoreCase("ROLE_RECEPTIONIST"))
                return customerService.findAllByType(pageNo, pageSize, sortBy, sortDir, "Spa");
            if (roleName.equalsIgnoreCase("ROLE_RECEPTIONIST_OBSTETRICAL"))
                return customerService.findAllByType(pageNo, pageSize, sortBy, sortDir, "Sản khoa");
        }
        if (name != null) {
            String result = name.replaceAll("[\"-+.^:,}{]", "")
                    .replaceAll(Constants.REMOVE_NAME, "")
                    .replaceAll(Constants.REMOVE_PHONE, "");
            if (roleName.equalsIgnoreCase("ROLE_ADMIN"))
                return customerService.findByTypeOfAdmin(pageNo, pageSize, sortBy, sortDir,result);
            if (roleName.equalsIgnoreCase("ROLE_RECEPTIONIST"))
                return customerService.findByType(pageNo, pageSize, sortBy, sortDir, "Spa", result);
            if (roleName.equalsIgnoreCase("ROLE_RECEPTIONIST_OBSTETRICAL"))
                return customerService.findByType(pageNo, pageSize, sortBy, sortDir, "Sản khoa", result);
        }
        return null;
    }

}
