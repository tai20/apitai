package com.sharework.health.controller.admin;

import com.sharework.health.dto.CustomerDto;
import com.sharework.health.dto.CustomerLevelDto;
import com.sharework.health.dto.RankCustomerDto;
import com.sharework.health.service.CustomerLevelService;
import com.sharework.health.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/admin/customerlevels")
@AllArgsConstructor
public class CustomerLevelController {
    private OrderService orderService;
    private CustomerLevelService customerLevelService;
    @GetMapping("/addlevel")
    public Object addLevel(){
        orderService.createCustomerLevel();

        return new ResponseEntity<>("test",HttpStatus.OK);
    }
    @GetMapping("")
    public Object getAllCustemLevel() {

        List<CustomerLevelDto> customerlevelDtos = customerLevelService.findAll();

        if (customerlevelDtos.isEmpty()) {
            return new ResponseEntity<>("Không có dữ liệu",HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(customerlevelDtos,HttpStatus.OK);
    }


}
