package com.sharework.health.controller.admin;

import com.sharework.health.Constants;
import com.sharework.health.dto.*;
import com.sharework.health.repository.ImportReceiptRepository;
import com.sharework.health.response.ImportRecepitReponse;
import com.sharework.health.response.OrderReponse;
import com.sharework.health.service.ImportReceiptService;
import com.sharework.health.service.UserService;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/admin/importreceipts")
@AllArgsConstructor
public class ImportReceiptController {

    private ImportReceiptService importReceiptService;
    private ImportReceiptRepository repository;
    private UserService userService;

//    @GetMapping("")
//    public ResponseEntity<ResponseObject> getAllImportReceipt(){
//        List<ImportReceiptDto> importReceiptDtos = importReceiptService.findAll();
//        if(importReceiptDtos.isEmpty()){
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
//                    new ResponseObject(400, "Không có dữ liệu", "")
//            );
//        }
//        return ResponseEntity.status(HttpStatus.OK).body(
//                new ResponseObject(200,  "Truy vấn thành công", importReceiptDtos)
//        );
//    }

//controller lay tat ca phieu nhap theo kho
    @GetMapping("")
    public ResponseEntity<ResponseObject> getAllImportWareHouseReceipt(Principal principal){
        List<ImportReceiptDto> importReceiptDtos = null;
        String email= principal.getName();
        UserDto u = userService.findByEmail(email);
        int clinic_id = u.getClinicDto().getId();// lay clinic theo tai khoan
        String role = u.getClinicDto().getType().toString();//lay the loai cua kho
        //kiem tra xem la kho tong hay kho chi nhanh
        if(role == "DEPOT"){
            importReceiptDtos = importReceiptService.findAll();
            if(importReceiptDtos.isEmpty()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ResponseObject(400, "Không có dữ liệu", "")
                );
            }
        }
        //kiem tra xem kho phai la kho chi nhanh khong
        if(role == "BRANCH"){
            importReceiptDtos = importReceiptService.getAllImportReceiptClinicDetail(clinic_id);
            if(importReceiptDtos.isEmpty()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ResponseObject(400, "Không có dữ liệu", "")
                );
            }

        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Truy vấn thành công", importReceiptDtos)
        );
    }

    @GetMapping("/{importReceipt_id}")
    public ResponseEntity<ResponseObject> getImportReceiptById(@PathVariable("importReceipt_id") Integer id){
       ImportReceiptDto dto = importReceiptService.findById(id);
        if (dto!=null)
        {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );

        } else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không tìm thấy", "")
            );
    }

    @PostMapping("")
    public ResponseEntity<ResponseObject> addImportReceipt(@RequestBody ImportReceiptDto dto ){
        if (dto == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không có dữ liệu", "")
            );
        }
        ImportReceiptDto result = importReceiptService.insertImportReceipt(dto);
        if(result == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Thêm không thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Thêm phiếu thành công", result)
        );
    }

    @GetMapping("/detail/{receipt_id}")
    public ResponseEntity<ResponseObject> getImportReceiptDetails(@PathVariable("receipt_id") Integer id){
        List<ImportReceiptDetailsQueryDto> dto = importReceiptService.getAllImportReceiptDetailByReceiptid(id);
        if (dto.isEmpty())
        {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không tìm thấy", "")
            );
        } else
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );

    }


    @PostMapping("/detail")
    public ResponseEntity<ResponseObject> addImportReceiptDetail(@RequestBody ImportReceiptDetailsDto dto){
        if (dto == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không có dữ liệu", "")
            );
        }
        ImportReceiptDetailsDto result = importReceiptService.insertImportReceiptDetail(dto);
        if(result == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Thêm không thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Thêm phiếu thành công", result)
        );
    }

    @DeleteMapping("{receipt_id}")
    public ResponseEntity<ResponseObject> deleteImportReceipt(@PathVariable("receipt_id") Integer id){
        boolean result = importReceiptService.deleteImportReceipt(id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã xóa thành công", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không được xóa", "")
            );
        }
    }

    @DeleteMapping("/detail/{receipt_id}/{clinic_stock_id}")
    public ResponseEntity<ResponseObject> deleteImportReceiptDetail(@PathVariable("receipt_id") Integer receipt_id, @PathVariable("clinic_stock_id") Integer clinic_stock_id){
        boolean result = importReceiptService.deleteImportReceiptDetail(receipt_id,clinic_stock_id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã xóa thành công", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không được xóa", "")
            );
        }
    }


    @PutMapping("/detail")
    public ResponseEntity<ResponseObject> updateImportReceiptDetail(@RequestBody ImportReceiptDetailsQueryDto dto){
        ImportReceiptDetailsQueryDto result = importReceiptService.updateImportReceiptDetail(dto);
        if(result == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Sửa không thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Sửa thành công", result)
        );

    }

    @PutMapping("/restore/{receipt_id}")
    public ResponseEntity<ResponseObject> restoreImportReceipt(@PathVariable("receipt_id") Integer id){
        boolean result = importReceiptService.restoreImportReceipt(id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã khôi phục thành công", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không khôi phục được. Hoặc đã có sẵn", "")
            );
        }
    }

    @PutMapping("/lock/{receipt_id}")
    public ResponseEntity<ResponseObject> lockImportReceipt(@PathVariable("receipt_id") Integer id){
        boolean result = importReceiptService.lockImportReceipt(id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã khóa phiếu", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Phiếu đã được khóa hoặc đã xóa", "")
            );
        }
    }

    //Use to Pageable improve performance
    @GetMapping("page")
    public ImportRecepitReponse getAllImportRecepit(
            Principal principal,
            @RequestParam(value = "sortBy", defaultValue = Constants.DEFAULT_SORT_BY, required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) String sortDir,
            @RequestParam(value = "current", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) int pageNo ,
            @RequestParam(value = "pageSize", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) int pageSize) {

        ImportRecepitReponse importRecepitReponse = new ImportRecepitReponse();
        String email= principal.getName();
        UserDto u = userService.findByEmail(email);
        int clinic_id = u.getClinicDto().getId();// lay clinic theo tai khoan
        String role = u.getClinicDto().getType().toString();//lay the loai cua kho
        //kiem tra xem la kho tong hay kho chi nhanh
        if(role == "DEPOT"){
            importRecepitReponse = importReceiptService.getAllImportRecepit(pageNo, pageSize, sortBy, sortDir);
        } else {
            importRecepitReponse = importReceiptService.getAllImportRecepitClinic(pageNo, pageSize, sortBy, sortDir, clinic_id);
        }
        if (importRecepitReponse.getData().isEmpty()){
            return null;
        }
        return importRecepitReponse;
    }

}


