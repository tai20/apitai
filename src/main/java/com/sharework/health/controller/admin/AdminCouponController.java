package com.sharework.health.controller.admin;

import com.sharework.health.dto.CouponDto;
import com.sharework.health.dto.CustomerCouponDto;
import com.sharework.health.service.CouponService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/admin/coupons")
@AllArgsConstructor
public class AdminCouponController {

    private CouponService couponService;

    @GetMapping("")
    public Object getAllCoupon() {
        List<CouponDto> couponDtos = couponService.findAll();
        if (couponDtos.isEmpty()) {
            return new ResponseEntity<>("Không có khuyến mãi nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(couponDtos, HttpStatus.OK);
    }

    @GetMapping("{coupon_id}")
    public Object getCouponById(@PathVariable("coupon_id") Integer couponId) {
        CouponDto dto = couponService.findById(couponId);
        if (dto == null) {
            return new ResponseEntity<>("Không tìm thấy khuyến mãi nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addCoupon(@RequestBody CouponDto dto, BindingResult error) throws IOException {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thêm khuyến mãi mới thất bại", HttpStatus.BAD_REQUEST);
        }
        boolean result = couponService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thiếu thông tin hoặc chưa đặt thời gian cho khuyến mãi", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm khuyến mãi mới thành công", HttpStatus.CREATED);
    }

    @PutMapping("/{coupon_id}")
    public Object updateCoupon(@PathVariable("coupon_id") Integer id, @RequestBody CouponDto dto,
                               BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Cập nhật khuyến mãi thất bại", HttpStatus.BAD_REQUEST);
        }
        couponService.update(id, dto);
        return new ResponseEntity<>("Cập nhât khuyến mãi thành công", HttpStatus.OK);
    }

    @DeleteMapping("/{coupon_id}")
    public ResponseEntity<CouponDto> deleteCoupon(@PathVariable("coupon_id") Integer id) {
        couponService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("addCouponForCustomer")
    public Object addCouponForCustomer(@RequestBody CustomerCouponDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = couponService.addCouponForCustomer(dto.getCouponDto().getId(), dto.getCustomerDto().getId());
        if (!result) {
            return new ResponseEntity<>("Thêm khuyến mãi cho khách hàng thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm khuyến mãi cho khách hàng thành công", HttpStatus.OK);
    }

    /**
     * NOTE : Chức năng thừa
      */

    @PostMapping("addCouponForServiceCategory")
    public Object addCouponForServiceCategory(@RequestParam("coupon_id") String couponId, @RequestParam("servicecategory_id") String serviceCategoryId) {
        boolean result = couponService.addCouponForServiceCategory(Integer.parseInt(couponId), Integer.parseInt(serviceCategoryId));
        if (!result) {
            return new ResponseEntity<>("Thêm khuyến mãi cho loại dịch vụ thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm khuyến mãi cho loại dịch vụ thành công", HttpStatus.OK);
    }

    @GetMapping("getCustomerCouponByCustomer/{customer_id}")
    public Object getCustomerCouponByCustomer(@PathVariable("customer_id") Integer customerId) {
        List<CustomerCouponDto> customerCouponDtos = couponService.findAllByCustomer(customerId);
        if (customerCouponDtos.isEmpty() || customerCouponDtos == null) {
            return new ResponseEntity<>("Khách hàng này không có khuyến mãi", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(customerCouponDtos, HttpStatus.OK);
    }

    @GetMapping("getCustomerCouponByCoupon/{coupon_id}")
    public Object getCustomerCouponByCoupon(@PathVariable("coupon_id") Integer couponId) {
        List<CustomerCouponDto> customerCouponDtos = couponService.findAllByCoupon(couponId);
        if (customerCouponDtos.isEmpty() || customerCouponDtos == null) {
            return new ResponseEntity<>("Khuyễn mãi chưa được tặng cho khách hàng", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(customerCouponDtos, HttpStatus.OK);
    }

    @PostMapping("checkCouponCode")
    public Object checkCouponCode(@RequestParam("couponCode") String couponCode) {
        CouponDto dto = couponService.checkCouponByCouponCode(couponCode);
        if (dto == null) {
            return new ResponseEntity<>("Mã khuyến mãi không hợp lệ", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
