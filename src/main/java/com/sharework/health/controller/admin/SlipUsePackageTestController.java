package com.sharework.health.controller.admin;

import com.sharework.health.dto.IndicationCardDto;
import com.sharework.health.dto.SlipUsePackageTestDto;
import com.sharework.health.service.SlipUsePackageTestService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/slipusepackagetest")
@AllArgsConstructor
public class SlipUsePackageTestController {

    private SlipUsePackageTestService slipUsePackageTestService;

    @GetMapping("")
    public Object getAllSlipUsePackageTest() {
        List<SlipUsePackageTestDto> slipusePackageTests = slipUsePackageTestService.findAll();
        if (slipusePackageTests.isEmpty() || slipusePackageTests == null) {
            return new ResponseEntity<>("Không có tư vấn nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(slipusePackageTests, HttpStatus.OK);
    }

    @GetMapping("{customer_id}")
    public Object getAllSlipUsePackagetestByCustomer(@PathVariable("customer_id") Integer customerId) {
        List<SlipUsePackageTestDto> slipUsePackageTestDtos = slipUsePackageTestService.findAllByCustomer(customerId);
        if (slipUsePackageTestDtos.isEmpty() || slipUsePackageTestDtos == null) {
            return new ResponseEntity<>("Khách hàng này không có gói sử dụng nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(slipUsePackageTestDtos, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addSlipUsePackageTest(@RequestBody SlipUsePackageTestDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = slipUsePackageTestService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm thành công", HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public Object updateSlipUsePackageTest(@PathVariable("id") Integer slipusepackagetestId, @RequestBody SlipUsePackageTestDto dto) {
        if (dto.getCustomerDto() == null || dto.getTestServiceDto() == null) {
            return new ResponseEntity<>("Thiếu thông tin gói dịch vụ khám hoặc khách hàng", HttpStatus.BAD_REQUEST);
        }
        boolean result = slipUsePackageTestService.update(slipusepackagetestId, dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public Object deleteSlipUsePackageTest(@PathVariable("id") Integer slipusepackagetestId) {

        boolean result = slipUsePackageTestService.delete(slipusepackagetestId);
        if (!result) {
            return new ResponseEntity<>("Xóa thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Xóa thành công", HttpStatus.OK);
    }

    @PutMapping("activeSlipUsePackageTest/{id}")
    public Object activeSlipUsePackageTest(@PathVariable("id") Integer slipusepackagetestId) {
        boolean result = slipUsePackageTestService.activeSlipUsePackageTest(slipusepackagetestId);
        if (!result) {
            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.OK);
    }

    @PutMapping("updateExtensionTime/{id}")
    public Object updateExtensionTime(@PathVariable("id") Integer slipusepackagetestId, @RequestBody SlipUsePackageTestDto dto,
                                      BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = slipUsePackageTestService.updateExtensionTime(slipusepackagetestId, dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.OK);
    }

    @GetMapping("getSlipUsePackageTestByMedicalCardId/{medicalCardId}")
    public Object getSlipUsePackageTestByMedicalCardId(@PathVariable("medicalCardId") Integer medicalCardId) {
        SlipUsePackageTestDto dto = slipUsePackageTestService.getSlipUsePackageTestByMedicalCardId(medicalCardId);
        if (dto == null) {
            return new ResponseEntity<>("Không có phiếu chỉ định", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping("updateSlipUseAfterAddTotalCost/{medical_id}")
    public Object updateSlipUseAfterAddTotalCost(@PathVariable("medical_id") Integer medicalCardId, @RequestBody SlipUsePackageTestDto dto
                        ,BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = slipUsePackageTestService.updateSlipUseAfterAddTotalCost(medicalCardId, dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.CREATED);
    }
}
