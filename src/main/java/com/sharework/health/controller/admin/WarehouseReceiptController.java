package com.sharework.health.controller.admin;

import com.sharework.health.dto.*;
import com.sharework.health.service.ProductService;
import com.sharework.health.service.WarehouseReceiptService;
import com.sharework.health.service.impl.WarehouseReceiptdetailServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/admin/warehousereceipts")
@AllArgsConstructor
public class WarehouseReceiptController {

    private WarehouseReceiptService warehouseReceiptService;
    private WarehouseReceiptdetailServiceImpl warehouseReceiptdetailService;

    private ProductService productService;

    @GetMapping("")
    public Object getAllWarehouseReceipt() {
        List<WarehouseReceiptDto> warehouseReceiptDtos = warehouseReceiptService.findAll();
        if (warehouseReceiptDtos.isEmpty()) {
            return new ResponseEntity<>("Không có phiếu nhập kho nào", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(warehouseReceiptDtos, HttpStatus.OK);
    }

    @GetMapping("/{warehousereceipt_id}")
    public Object getWarehouseReceiptById(@PathVariable("warehousereceipt_id") Integer id) {
        WarehouseReceiptDto warehouseReceiptDto = warehouseReceiptService.findById(id);
        if (warehouseReceiptDto == null) {
            return new ResponseEntity<>("Không có phiếu nhập kho nào", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(warehouseReceiptDto, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addWarehouseReceipt(@RequestBody WarehouseReceiptDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Không có dữ liệu truyền", HttpStatus.BAD_REQUEST);
        }
        boolean result = warehouseReceiptService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm không thành công", HttpStatus.BAD_REQUEST);
        }
        WarehouseReceiptAfterInsertDto warehouseReceiptAfterInsertDto = new WarehouseReceiptAfterInsertDto()
                .setMessage("Thêm phiếu kho thành công")
                .setWarehouseReceiptDto(warehouseReceiptService.findWarehouseReceiptAfterInsert());
        return new ResponseEntity<>(warehouseReceiptAfterInsertDto, HttpStatus.CREATED);
    }

    @PutMapping("/{warehousereceipt_id}")
    public Object updateWarehouseReceipt(@PathVariable("warehousereceipt_id") Integer id, @RequestBody WarehouseReceiptDto dto) {

        if (dto == null) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.BAD_REQUEST);
        }
        boolean result = warehouseReceiptService.update(id, dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhật phiếu kho thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật phiếu kho thành công", HttpStatus.OK);
    }

    @DeleteMapping("/{warehousereceipt_id}")
    public Object deleteWarehouseReceipt(@PathVariable("warehousereceipt_id") Integer id) {

        boolean result = warehouseReceiptService.delete(id);
        if (!result) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

//    @PostMapping("")
//    public Object searchWarehouseReceiptByClinic(@RequestBody WarehouseReceiptDto warehouseReceiptDto) {
//        if (warehouseReceiptDto == null) {
//            return new ResponseEntity<>("Không có dữ liệu truyền vào", HttpStatus.BAD_REQUEST);
//        }
//        warehouseReceiptService.insert(warehouseReceiptDto);
//        return new ResponseEntity<>(warehouseReceiptDto,HttpStatus.CREATED);
//    }

    @PostMapping("searchWarehouseReceiptByDateImport")
    public Object searchWarehouseReceiptByDateImport(@RequestParam("dateImport") LocalDate dateImport) {
        List<WarehouseReceiptDto> warehouseReceiptDtos = warehouseReceiptService.searchWarehouseReceiptByDateImport(dateImport);
        if (warehouseReceiptDtos.isEmpty() || warehouseReceiptDtos == null) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(warehouseReceiptDtos, HttpStatus.OK);
    }

    @PostMapping("searchWarehouseReceiptByReceiptType")
    public Object searchWarehouseReceiptByReceiptType(@RequestParam("receiptType") String receiptType) {
        List<WarehouseReceiptDto> warehouseReceiptDtos = warehouseReceiptService.searchWarehouseReceiptByReceiptType(receiptType);
        if (warehouseReceiptDtos.isEmpty() || warehouseReceiptDtos == null) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(warehouseReceiptDtos, HttpStatus.OK);
    }

    @PostMapping("searchWarehouseReceiptByClinic")
    public Object searchWarehouseReceiptByClinic(@RequestParam("clinic_id") Integer clinicId) {
        List<WarehouseReceiptDto> warehouseReceiptDtos = warehouseReceiptService.searchWarehouseReceiptByClinic(clinicId);
        if (warehouseReceiptDtos.isEmpty() || warehouseReceiptDtos == null) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(warehouseReceiptDtos, HttpStatus.OK);
    }

    @PostMapping("addWarehouseReceiptDetail/{warehousereceipt_id}")
    public Object addWarehouseReceiptDetail(@PathVariable("warehousereceipt_id") Integer warehousereceiptId, @RequestBody WarehouseReceiptDetailDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu dữ liệu", HttpStatus.BAD_REQUEST);
        }
        boolean result = warehouseReceiptService.addWarehouseReceiptDetail(warehousereceiptId, dto);
        if (!result) {
            return new ResponseEntity<>("Thêm thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm thành công", HttpStatus.OK);
    }

    @GetMapping("getWarehouseReceiptDetailByWarehouseReceipt/{warehousereceipt_id}")
    public Object getWarehouseReceiptDetailByWarehouseReceipt(@PathVariable("warehousereceipt_id") Integer warehousereceiptId) {

        List<WarehouseReceiptDetailDto> dtos = warehouseReceiptService.findAllByWarehouseReceipt(warehousereceiptId);
        if (dtos.isEmpty() || dtos == null) {
            return new ResponseEntity<>("Không có chi tiết phiếu kho", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    // sửa số số lượng sản phẩm (nhập xuất khác)
    @PutMapping("/warehouseReceiptdetail")
    public ResponseEntity updateWarehouseReceiptdetail(@RequestBody WarehouseReceiptDetailDto2 dto) {

        if (dto == null) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.BAD_REQUEST);
        }
        boolean result = warehouseReceiptdetailService.update(dto.getNumberProductImport(), dto.getProduct_id(), dto.getReceipt_id());
        if (!result) {
            return new ResponseEntity<>("Cập nhật kho thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật kho thành công", HttpStatus.OK);
    }

    // sửa số lượng và giá (xuất hàng)
    @PutMapping("/product")
    public ResponseEntity<ProductDto> updateProduct(@RequestBody ProductDto dto, BindingResult error) {
        ProductDto productDto = productService.findById(dto.getId());
        productDto.setPrice(dto.getPrice());
        productDto.setQuantity(dto.getQuantity());
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        productService.update(dto.getId(), productDto);
        return new ResponseEntity<>(productDto, HttpStatus.OK);
    }

}
