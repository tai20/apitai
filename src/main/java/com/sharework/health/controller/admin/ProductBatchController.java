package com.sharework.health.controller.admin;

import com.sharework.health.Constants;
import com.sharework.health.dto.ProductBatchDto;
import com.sharework.health.dto.ProductDto;
import com.sharework.health.dto.QueryProductBatch;
import com.sharework.health.dto.ResponseObject;
import com.sharework.health.response.CustomerReponse;
import com.sharework.health.response.ProductBatchReponse;
import com.sharework.health.service.ProductBatchService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/admin/productbatch")
@AllArgsConstructor
public class ProductBatchController {

    private ProductBatchService productBatchService;

    @GetMapping("")
    public ResponseEntity<List<ProductBatchDto>> getAll(){
        List<ProductBatchDto> dtos = productBatchService.findAll();
        if (dtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    @GetMapping("findPBById/{id}")
    public ResponseEntity<List<ProductBatchDto>> findProductBatchIdByIdProduct (@PathVariable("id") Integer id){
        List<ProductBatchDto> dtos = productBatchService.findProductBatchIdByIdProduct(id);
        if (dtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("findPBByExpiry")
    public ResponseEntity<List<ProductBatchDto>> findProductByExpiry (@RequestParam("expiryStart") String expiryStart, @RequestParam("expiryEnd") String expiryEnd){
        List<ProductBatchDto> dtos = productBatchService.findProductByExpiry(LocalDate.parse(expiryStart), LocalDate.parse(expiryEnd));

        if (dtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("findPBByName/{name}")
    public ResponseEntity<List<ProductBatchDto>> findByLikeName (@PathVariable("name") String name){
        List<ProductBatchDto> dtos = productBatchService.findByLikeName(name);
        if (dtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("findByLikeNameAndExpiry")
    public ResponseEntity<List<ProductBatchDto>> findByLikeNameAndExpiry (@RequestParam("name") String name, @RequestParam("expiryStart") String expiryStart, @RequestParam("expiryEnd") String expiryEnd){
        List<ProductBatchDto> dtos = productBatchService.findByLikeNameAndExpiry(name,LocalDate.parse(expiryStart),LocalDate.parse(expiryEnd));
        if (dtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    @GetMapping("/{productBatch_id}")
    public ResponseEntity<ProductBatchDto> getProductById(@PathVariable("productBatch_id") Integer id) {
        ProductBatchDto dto = productBatchService.findById(id);
        if (dto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<ProductBatchDto> addProductBatch(@RequestBody ProductBatchDto dto, BindingResult error) {

        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        productBatchService.insert(dto);

        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

    @PutMapping("/{productBatch_id}")
    public ResponseEntity<ProductBatchDto> updateProductBatch(@PathVariable("productBatch_id") Integer id, @RequestBody ProductBatchDto dto,
                                                    BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        productBatchService.update(id, dto);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping("/{productBatch_id}")
    public ResponseEntity<ResponseObject> deleteProductBatch(@PathVariable("productBatch_id") Integer id) {
        boolean result = productBatchService.delete(id);
        if (!result) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không được xóa, sản phẩm còn ở các phiếu. Vui lòng xóa sản phẩm ở các phiếu trước khi xóa sản phẩm", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200, "Xóa sản phẩm thành công", "")
        );
    }

    @GetMapping("page")
    public ProductBatchReponse getAllProductBatch(
            @RequestParam(value = "sortBy", defaultValue = Constants.DEFAULT_SORT_BY, required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) String sortDir,
            @RequestParam(value = "current", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) int pageNo ,
            @RequestParam(value = "pageSize", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) int pageSize,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "expiry", required = false) String expiry) {
        String start = String.valueOf(LocalDate.from(LocalDateTime.now()));
        if(name == null && expiry == null ) return productBatchService.getAllProductBatch(pageNo, pageSize, sortBy, sortDir);
        if(name != null && expiry == null ) return productBatchService.getAllProductBatchByName(pageNo, pageSize, sortBy, sortDir,name);
        if(name == null && expiry != null ) return productBatchService.getAllProductBatchByExpiry(pageNo, pageSize, sortBy, sortDir,start,expiry);

        return null;
    }
}
