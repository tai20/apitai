package com.sharework.health.convert;

import com.sharework.health.dto.CustomerDto;
import com.sharework.health.dto.SlipReturnProductDto;
import com.sharework.health.entity.SlipReturnProduct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class SlipReturnProductConvert implements BaseConvert<SlipReturnProduct, SlipReturnProductDto> {

    @Override
    public SlipReturnProductDto entityToDto(SlipReturnProduct entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, SlipReturnProductDto.class)
                .setCustomerDto(modelMapper.map(entity.getCustomer(), CustomerDto.class));
    }

    @Override
    public SlipReturnProduct dtoToEntity(SlipReturnProductDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, SlipReturnProduct.class);
    }
}
