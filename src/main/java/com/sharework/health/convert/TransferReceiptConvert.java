package com.sharework.health.convert;

import com.sharework.health.dto.ClinicDto;
import com.sharework.health.dto.TransferReceiptDto;
import com.sharework.health.dto.UserDto;
import com.sharework.health.entity.TransferReceipt;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class TransferReceiptConvert implements BaseConvert<TransferReceipt, TransferReceiptDto>{


    @Override
    public TransferReceiptDto entityToDto(TransferReceipt entity){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, TransferReceiptDto.class)
                .setUserDto(modelMapper.map(entity.getUser(), UserDto.class))
                .setClinicSendingDto(modelMapper.map(entity.getClinic_sending(), ClinicDto.class))
                .setClinicRecevingDto(modelMapper.map(entity.getClinic_receiving(),ClinicDto.class));

    }

    @Override
    public TransferReceipt dtoToEntity(TransferReceiptDto dto){
        ModelMapper modelMapper = new ModelMapper();
        return  modelMapper.map(dto, TransferReceipt.class);
    }

}
