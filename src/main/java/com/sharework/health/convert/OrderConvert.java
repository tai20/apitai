package com.sharework.health.convert;

import com.sharework.health.dto.CouponDto;
import com.sharework.health.dto.CustomerDto;
import com.sharework.health.dto.OrderDto;
import com.sharework.health.dto.UserDto;
import com.sharework.health.entity.Order;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class OrderConvert implements BaseConvert<Order, OrderDto> {
    @Override
    public OrderDto entityToDto(Order entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, OrderDto.class)
        		.setCustomerDto(modelMapper.map(entity.getCustomer(), CustomerDto.class))
        		.setUserDto(modelMapper.map(entity.getUser(), UserDto.class));
    }

    @Override
    public Order dtoToEntity(OrderDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, Order.class);
    }
}
