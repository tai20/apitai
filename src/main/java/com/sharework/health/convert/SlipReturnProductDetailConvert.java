package com.sharework.health.convert;

import com.sharework.health.dto.ProductDto;
import com.sharework.health.dto.SlipReturnProductDetailDto;
import com.sharework.health.dto.SlipReturnProductDto;
import com.sharework.health.entity.SlipReturnProductDetail;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class SlipReturnProductDetailConvert implements BaseConvert<SlipReturnProductDetail, SlipReturnProductDetailDto> {

    @Override
    public SlipReturnProductDetailDto entityToDto(SlipReturnProductDetail entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, SlipReturnProductDetailDto.class)
                .setProductDto(modelMapper.map(entity.getProduct(), ProductDto.class))
                .setSlipReturnProductDto(modelMapper.map(entity.getSlipReturnProduct(), SlipReturnProductDto.class));
    }

    @Override
    public SlipReturnProductDetail dtoToEntity(SlipReturnProductDetailDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, SlipReturnProductDetail.class);
    }
}
