package com.sharework.health.convert;

import com.sharework.health.dto.CustomerCategoryDto;
import com.sharework.health.entity.CustomerCategory;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CustomerCategoryConvert implements BaseConvert<CustomerCategory, CustomerCategoryDto> {

    @Override
    public CustomerCategoryDto entityToDto(CustomerCategory entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, CustomerCategoryDto.class);
    }

    @Override
    public CustomerCategory dtoToEntity(CustomerCategoryDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, CustomerCategory.class);
    }
}
