package com.sharework.health.convert;

import com.sharework.health.dto.ProductDto;
import com.sharework.health.dto.WarehouseReceiptDetailDto;
import com.sharework.health.dto.WarehouseReceiptDto;
import com.sharework.health.entity.WarehouseReceiptDetail;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class WarehouseReceiptDetailConvert implements BaseConvert<WarehouseReceiptDetail, WarehouseReceiptDetailDto> {

    @Override
    public WarehouseReceiptDetailDto entityToDto(WarehouseReceiptDetail entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, WarehouseReceiptDetailDto.class)
                .setWarehouseReceiptDto(modelMapper.map(entity.getWarehouseReceipt(), WarehouseReceiptDto.class))
                .setProductDto(modelMapper.map(entity.getProduct(), ProductDto.class));
    }

    @Override
    public WarehouseReceiptDetail dtoToEntity(WarehouseReceiptDetailDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, WarehouseReceiptDetail.class);
    }
}
