package com.sharework.health.convert;

import com.sharework.health.dto.CustomerDto;
import com.sharework.health.dto.CustomerLevelDto;
import com.sharework.health.dto.LevelDto;
import com.sharework.health.entity.CustomerLevel;
import com.sharework.health.entity.Level;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CustomerLevelConvert implements BaseConvert<CustomerLevel, CustomerLevelDto>{
    @Override
    public CustomerLevelDto entityToDto(CustomerLevel entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, CustomerLevelDto.class)
                .setCustomer_id(modelMapper.map(entity.getCustomer(), CustomerDto.class))
                .setLevel_id((modelMapper.map(entity.getLevel(), LevelDto.class)));
    }

    @Override
    public CustomerLevel dtoToEntity(CustomerLevelDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, CustomerLevel.class);
    }
}
