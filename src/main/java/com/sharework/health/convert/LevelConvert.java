package com.sharework.health.convert;


import com.sharework.health.dto.CustomerLevelDto;
import com.sharework.health.dto.LevelDto;
import com.sharework.health.entity.Level;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class LevelConvert implements BaseConvert<Level, LevelDto>{
    @Override
    public LevelDto entityToDto(Level entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, LevelDto.class);
    }

    @Override
    public Level dtoToEntity(LevelDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, Level.class);
    }
}
