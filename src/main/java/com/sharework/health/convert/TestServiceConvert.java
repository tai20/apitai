package com.sharework.health.convert;

import com.sharework.health.dto.TestServiceDto;
import com.sharework.health.entity.TestService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class TestServiceConvert implements BaseConvert<TestService, TestServiceDto> {

    @Override
    public TestServiceDto entityToDto(TestService entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, TestServiceDto.class);
    }

    @Override
    public TestService dtoToEntity(TestServiceDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, TestService.class);
    }
}
