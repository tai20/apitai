package com.sharework.health.convert;

import com.sharework.health.dto.CouponDto;
import com.sharework.health.dto.CustomerCouponDto;
import com.sharework.health.dto.CustomerDto;
import com.sharework.health.entity.CustomerCoupon;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CustomerCouponConvert implements BaseConvert<CustomerCoupon, CustomerCouponDto> {

    @Override
    public CustomerCouponDto entityToDto(CustomerCoupon entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, CustomerCouponDto.class)
                .setCouponDto(modelMapper.map(entity.getCoupon(), CouponDto.class))
                .setCustomerDto(modelMapper.map(entity.getCustomer(), CustomerDto.class));
    }

    @Override
    public CustomerCoupon dtoToEntity(CustomerCouponDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, CustomerCoupon.class);
    }
}
