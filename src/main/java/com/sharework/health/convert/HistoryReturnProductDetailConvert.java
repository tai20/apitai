package com.sharework.health.convert;

import com.sharework.health.dto.HistoryReturnProductDetailDto;
import com.sharework.health.dto.HistoryReturnProductDto;
import com.sharework.health.dto.ProductDto;
import com.sharework.health.entity.HistoryReturnProductDetail;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class HistoryReturnProductDetailConvert implements BaseConvert<HistoryReturnProductDetail, HistoryReturnProductDetailDto> {

    @Override
    public HistoryReturnProductDetailDto entityToDto(HistoryReturnProductDetail entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, HistoryReturnProductDetailDto.class)
                .setHistoryReturnProductDto(modelMapper.map(entity.getHistoryReturnProduct(), HistoryReturnProductDto.class))
                .setProductDto(modelMapper.map(entity.getProduct(), ProductDto.class));
    }

    @Override
    public HistoryReturnProductDetail dtoToEntity(HistoryReturnProductDetailDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, HistoryReturnProductDetail.class);
    }
}
