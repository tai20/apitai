package com.sharework.health.convert;

import com.sharework.health.dto.CategoryIndicationCardDto;
import com.sharework.health.dto.IndicationCardDetailDto;
import com.sharework.health.dto.IndicationCardDto;
import com.sharework.health.entity.IndicationCardDetail;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class IndicationCardDetailConvert implements BaseConvert<IndicationCardDetail, IndicationCardDetailDto> {

    @Override
    public IndicationCardDetailDto entityToDto(IndicationCardDetail entity) {
        IndicationCardConvert indicationCardConvert = new IndicationCardConvert();

        CategoryIndicationCardConvert categoryIndicationCardConvert = new CategoryIndicationCardConvert();

        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, IndicationCardDetailDto.class)
                .setIndicationCardDto(indicationCardConvert.entityToDto(entity.getIndicationCard()))
                .setCategoryIndicationCardDto(categoryIndicationCardConvert.entityToDto(entity.getCategoryIndicationCard()));
    }

    @Override
    public IndicationCardDetail dtoToEntity(IndicationCardDetailDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, IndicationCardDetail.class);
    }
}
