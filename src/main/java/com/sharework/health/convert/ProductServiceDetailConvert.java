package com.sharework.health.convert;

import com.sharework.health.dto.ProductDto;
import com.sharework.health.dto.ProductServiceDetailDto;
import com.sharework.health.dto.ServiceDto;
import com.sharework.health.entity.ProductServiceDetail;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ProductServiceDetailConvert implements BaseConvert<ProductServiceDetail, ProductServiceDetailDto> {

    @Override
    public ProductServiceDetailDto entityToDto(ProductServiceDetail entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, ProductServiceDetailDto.class)
                .setProductDto(modelMapper.map(entity.getProduct(), ProductDto.class))
                .setServiceDto(modelMapper.map(entity.getService(), ServiceDto.class));
    }

    @Override
    public ProductServiceDetail dtoToEntity(ProductServiceDetailDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, ProductServiceDetail.class);
    }
}
