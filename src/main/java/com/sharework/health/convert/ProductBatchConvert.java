package com.sharework.health.convert;

import com.sharework.health.dto.ProductBatchDto;
import com.sharework.health.dto.ProductDto;
import com.sharework.health.entity.ProductBatch;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ProductBatchConvert implements BaseConvert<ProductBatch, ProductBatchDto>{
    @Override
    public ProductBatchDto entityToDto(ProductBatch entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, ProductBatchDto.class)
                .setProductDto(modelMapper.map(entity.getProductId(), ProductDto.class));
    }

    @Override
    public ProductBatch dtoToEntity(ProductBatchDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, ProductBatch.class);
    }
}
