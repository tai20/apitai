package com.sharework.health.convert;

import com.sharework.health.dto.BillDto;
import com.sharework.health.dto.OrderDto;
import com.sharework.health.entity.Bill;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class BillConvert implements BaseConvert<Bill, BillDto>{
    @Override
    public BillDto entityToDto(Bill entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, BillDto.class)
                .setOrderDto(modelMapper.map(entity.getOrder(), OrderDto.class));
    }

    @Override
    public Bill dtoToEntity(BillDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, Bill.class);
    }
}
