package com.sharework.health.convert;

import com.sharework.health.dto.GiftDto;
import com.sharework.health.dto.GiftListDto;
import com.sharework.health.dto.ProductDto;
import com.sharework.health.entity.GiftList;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class GiftListConvert implements BaseConvert<GiftList, GiftListDto>{

    @Override
    public GiftListDto entityToDto(GiftList entity) {
        ModelMapper modelMapper = new ModelMapper();
        GiftListDto giftListDto= modelMapper.map(entity,GiftListDto.class)
                .setGiftDto(modelMapper.map(entity.getGift(), GiftDto.class))
                .setProductDto(modelMapper.map(entity.getProduct(), ProductDto.class));
        return giftListDto;
    }

    @Override
    public GiftList dtoToEntity(GiftListDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, GiftList.class);
    }
}
