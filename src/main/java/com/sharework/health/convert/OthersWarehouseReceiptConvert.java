package com.sharework.health.convert;

import com.sharework.health.dto.ClinicDto;
import com.sharework.health.dto.OthersWarehouseReceiptDto;
import com.sharework.health.dto.UserDto;
import com.sharework.health.entity.OthersWarehouseReceipt;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class OthersWarehouseReceiptConvert implements BaseConvert<OthersWarehouseReceipt, OthersWarehouseReceiptDto>{

    @Override
    public OthersWarehouseReceiptDto entityToDto(OthersWarehouseReceipt entity){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, OthersWarehouseReceiptDto.class)
                .setUserDto(modelMapper.map(entity.getUser(), UserDto.class))
                .setClinicDto(modelMapper.map(entity.getClinic(), ClinicDto.class));
    }


    @Override
    public OthersWarehouseReceipt dtoToEntity(OthersWarehouseReceiptDto dto){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, OthersWarehouseReceipt.class);
    }
}
